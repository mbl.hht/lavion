//
//  HomeTitleHeaderView.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/16/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class HomeTitleHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var hotImage: UIImageView!
    
    @IBOutlet weak var seeMoreButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    var typeHeaderHome: TypeHomeTitleHeader = .None
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: 2, opacity: 0.35)
        mainView.initGradient(colorStart: "#F09819", colorEnd: "#FF512F",defautRadius: 0)
    }
    @IBAction func seeMoreButton(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        switch typeHeaderHome {
        case .DanhMucNganhHang:
            let vc = sb.instantiateViewController(withIdentifier: "catelogyProductViewController")
            self.parentContainerViewController()?.navigationController?.pushViewController(vc, animated: true)
            
            break
        case .DanhSachSanPham:
            let vc = sb.instantiateViewController(withIdentifier: "productListViewController")
            self.parentContainerViewController()?.navigationController?.pushViewController(vc, animated: true)
            
            break
        

        default:
            break
        }
    }
    
}
