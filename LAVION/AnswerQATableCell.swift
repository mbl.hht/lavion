//
//  AnswerQATableCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/7/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class AnswerQATableCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentAnwserLabel: UILabel!
    @IBOutlet weak var anwserLabel: UILabel!
    
    var item: AnswerAgencyModel? {
        didSet{
            dateLabel.text = item?.createdAt
            contentAnwserLabel.text = item?.coiDungTraLoi
            anwserLabel.text = item?.congTacVienHoVaTen
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.initShadowForCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
