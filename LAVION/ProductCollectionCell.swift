//
//  ProductCollectionCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/21/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import Cosmos
import AlamofireImage


class ProductCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var rightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftContraint: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameProductLabel: UILabel!
    
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var costLabel: UILabel!
    
    @IBOutlet weak var costDownLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    var item: ProductModel? { 
        didSet {
            let placeholderImage = UIImage(named: "lavion.jpg")
            if let url = URL(string: (item?.san_pham_hinh_dai_dien) ?? address) {
                imageView.af_setImage(withURL: url, placeholderImage: placeholderImage)
            } else {
                imageView.image = placeholderImage
            }
            nameProductLabel.text = item?.san_pham_ten
            ratingView.rating = 5
            costLabel.text = "\(item?.san_pham_gia_ban_thuc_te.formatNumber() ?? "0") VND"
            costDownLabel.text = ""
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.initShadowForCell()
    }

}
