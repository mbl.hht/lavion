//
//  HistoryTakeCashCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/8/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class HistoryTakeCashCell: UITableViewCell {

    @IBOutlet weak var cashLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var nameBankLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
