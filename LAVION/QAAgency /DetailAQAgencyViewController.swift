//
//  DetailAQAgencyViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/7/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class DetailAQAgencyViewController: UIViewController {


    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var answerTextView: UITextView!
    
    var qAAgency: QAAgencyModel?
    
    
    var answerListArray = [AnswerAgencyModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavigation(text: "Chi tiết câu hỏi",hidden: true)
        answerTextView.delegate = self
        answerTextView.text = "Nhập câu trả lời"
        answerTextView.textColor = UIColor.lightGray
        self.answerTextView.layer.cornerRadius = 10
        self.answerTextView.backgroundColor = UIColor(white: 0.9, alpha: 1)
        self.answerTextView.toolbarPlaceholder = "Nhập câu trả lời "
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib.init(nibName: "AnswerQATableCell", bundle: nil), forCellReuseIdentifier: "answerQATableCell")
        mainTableView.register(UINib.init(nibName: "AnswerQAHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "AnswerQAHeaderView")
        mainTableView.sectionHeaderHeight = UITableView.automaticDimension
        mainTableView.estimatedSectionHeaderHeight =  mainTableView.sectionHeaderHeight
        
        mainTableView.tableFooterView = UIView()
        
        //init Data
        loadData()
        
    }
    
    
    func sizeHeaderToFit(tableView: UITableView) {
        if let headerView = tableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var frame = headerView.frame
            frame.size.height = height
            headerView.frame = frame
            tableView.tableHeaderView = headerView
            headerView.setNeedsLayout()
            headerView.layoutIfNeeded()
        }
    }
    
    override func viewDidLayoutSubviews() {
        // viewDidLayoutSubviews is called when labels change.
        super.viewDidLayoutSubviews()
        sizeHeaderToFit(tableView: self.mainTableView)
    }
    
    @IBAction func sendAction(_ sender: Any) {
        if answerTextView.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            return
        }
        answerTextView.resignFirstResponder()
        
        UIHelper.showHUD()
        SyncHttpConnect.postAQAgency(id: (qAAgency?.id ?? -1), cau_tra_loi: answerTextView.text!) { (data, scuccess) in
            UIHelper.dismisHUD()
            if scuccess {
                if data["status"] as! Int == 200 {
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: "Đăng câu trả lời thành công.", complete: {
                        DispatchQueue.main.async {
                            self.answerListArray.removeAll()
                            self.loadData()
                            self.answerTextView.text = ""
                            self.mainTableView.scrollToNearestSelectedRow(at: .bottom, animated: true)
                        }
                    })
                    
                    
                } else if data["status"] as! Int == 400 {
                    let messageError = "Lỗi không xác định."
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: messageError, complete: nil)
                    
                }
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
            
        }
    }
    
    func loadData() {
        UIHelper.showHUD()
        SyncHttpConnect.getDetailQAgency(id: qAAgency?.id ?? -1) { (data, success) in
            UIHelper.dismisHUD()
            DispatchQueue.main.async {
                if success {
                    let statusCode = data["status"] as? Int ?? 0
                    if statusCode == TypeStatusCode.success.rawValue {
                        let parseData = (data["data"] as! Dictionary<String, Any>)["traLoi"] as! Array<Dictionary<String, Any>>
                        if parseData.count > 0 {
                            for item in parseData {
                                var model = AnswerAgencyModel()
                                model.congTacVienHoVaTen        = item["cong_tac_vien_ho_va_ten"] as? String ?? ""
                                model.coiDungTraLoi             = item["noi_dung_tra_loi"] as? String ?? ""
                                model.createdAt                 = item["created_at"] as? String ?? ""
                                self.answerListArray.append(model)
                            }
                            self.mainTableView.reloadData()
                            
                        }
                        
                    } else if statusCode == TypeStatusCode.failed.rawValue {
                        ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: data["errors"] as? String ?? "Không tìm thấy câu hỏi", complete: {
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                }
                
            }
            
        }
        
    }
    
    
}

extension DetailAQAgencyViewController: UITableViewDelegate {
    
}

extension DetailAQAgencyViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answerListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "answerQATableCell") as? AnswerQATableCell else {
            return UITableViewCell()
        }
        cell.item = answerListArray[indexPath.row]
        cell.contentAnwserLabel.text = answerListArray[indexPath.row].coiDungTraLoi
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "AnswerQAHeaderView") as? AnswerQAHeaderView else {
            return UITableViewHeaderFooterView()
        }
        cell.nameQuestionLabel.text = qAAgency?.ho_va_ten
        cell.dateQuestionLabel.text = qAAgency?.created_at
        cell.contentQuestionLabel.text = qAAgency?.noi_dung_cau_hoi
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}

extension DetailAQAgencyViewController:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        if mainTableView.numberOfRows(inSection: 0) > 0 {
            mainTableView.scrollToRow(at: IndexPath.init(row: mainTableView.numberOfRows(inSection: 0) - 1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
        }
        

        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Nhập câu trả lời"
            textView.textColor = UIColor.lightGray
        }
        
    }
    
}
