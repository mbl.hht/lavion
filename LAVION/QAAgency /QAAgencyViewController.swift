//
//  QAAgencyViewController.swift
//  LAVION
//
//  Created by iOS on 12/1/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class QAAgencyViewController: UIViewController {
    @IBOutlet weak var mainTableView: UITableView!
    var notificationToken: NotificationToken? = nil
    
    let searchController = UISearchController(searchResultsController: nil)
    var arrayList = (try! Realm()).objects(QAAgencyModel.self).sorted(byKeyPath: "created_at", ascending: false)
    var filteredQAAgencyArray = [QAAgencyModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        if #available(iOS 9.1, *) {
            searchController.obscuresBackgroundDuringPresentation = false
        }
        definesPresentationContext = true
        searchController.searchBar.placeholder = "Tìm kiếm câu hỏi"
        let scb = searchController.searchBar
        scb.tintColor = UIColor.white
        scb.barTintColor = UIColor.white
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                backgroundview.backgroundColor = UIColor.white
                backgroundview.layer.cornerRadius = 10;
            }
        }
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Hủy"
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        }
     
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Tạo câu hỏi", style: .plain, target: self, action: #selector(addQAAction))
        // Setup the Scope Bar
        searchController.searchBar.scopeButtonTitles = ["Tất cả", "Chủ đề", "Tên"]
        searchController.searchBar.delegate = self
        
        
        //SetupTableView
        mainTableView.dataSource = self
        mainTableView.delegate = self
        mainTableView.register(UINib.init(nibName: "GeneralInformationTableCell", bundle: nil), forCellReuseIdentifier: "generalInformationTableCell")
        
        notificationToken = arrayList.observe({ (data) in
            self.filterContentForSearchText(searchText: self.searchController.searchBar.text ?? "",scope: self.searchController.searchBar.scopeButtonTitles![self.searchController.searchBar.selectedScopeButtonIndex])
        })
        


    }
    
    @objc func addQAAction() {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "createQAViewController") as! CreateQAViewController
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initNavigation(text: "Hỏi đáp đại lý")
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    func filterContentForSearchText(searchText: String, scope: String = "Tất cả") {
        var tam: Int = 0
        filteredQAAgencyArray = arrayList.filter({ (qAAgency: QAAgencyModel) -> Bool in
            tam += 1
            let doesCategoryMatch = (scope == "Tất cả") || (scope == "Chủ đề") || (scope == "Tên")
            if searchBarIsEmpty() {
                return doesCategoryMatch
            } else {
                if scope == "Chủ đề" {
                    return doesCategoryMatch && qAAgency.noi_dung_cau_hoi.lowercased().contains(searchText.lowercased())
                } else if scope == "Tên" {
                    return doesCategoryMatch && qAAgency.ho_va_ten.lowercased().contains(searchText.lowercased())
                }
                
                return doesCategoryMatch && (doesCategoryMatch && qAAgency.noi_dung_cau_hoi.lowercased().contains(searchText.lowercased()) || qAAgency.ho_va_ten.lowercased().contains(searchText.lowercased()) )
                
            }
        })
        mainTableView.reloadData()
        
    }
    
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }

}
extension QAAgencyViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        let  scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        print("LBMJoker %@",searchBar.text ?? "")
        filterContentForSearchText(searchText: searchBar.text!, scope: scope)
        
    }
    
}

extension QAAgencyViewController:UISearchControllerDelegate {
    
}

extension QAAgencyViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let  scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        print("LBMJoker %@",searchBar.text ?? "")
        filterContentForSearchText(searchText: searchBar.text!, scope: scope)
        
    }
}


extension QAAgencyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "detailAQAgencyViewController") as! DetailAQAgencyViewController
        if isFiltering() {
            vc.qAAgency = filteredQAAgencyArray[indexPath.row]
        } else {
            vc.qAAgency = arrayList[indexPath.row]
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
}

extension QAAgencyViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredQAAgencyArray.count
        }
        return arrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "generalInformationTableCell") as! GeneralInformationTableCell
        
        let qAAgency: QAAgencyModel
        
        if isFiltering() {
            qAAgency = filteredQAAgencyArray[indexPath.row]
        } else {
            qAAgency = arrayList[indexPath.row]
        }
        
        cell.item = qAAgency
        cell.contentView.backgroundColor = UIColor.clear
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    
}
