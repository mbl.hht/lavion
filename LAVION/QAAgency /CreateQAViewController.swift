//
//  CreateQAViewController.swift
//  LAVION
//
//  Created by iOS on 12/3/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class CreateQAViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var createQAButton: UIButton!
    @IBOutlet weak var QATextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavigation(text: "Tạo câu hỏi", hidden: true)
        QATextView.initShadowUITextView()
        createQAButton.initRadius()
        createQAButton.setShadow(color: UIColor.mainColor!)
        QATextView.delegate = self

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func createQAAction(_ sender: Any) {
        var messageError = ""
        if QATextView.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Bạn chưa nhập câu hỏi."
        }
        
        if messageError.count > 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Thông Báo", errorString: messageError, complete: nil)
            return
        }
        
        UIHelper.showHUD()
        SyncHttpConnect.postQAgency(cau_hoi: QATextView.text!) { (data, success) in
            UIHelper.dismisHUD()
            if success {
                if data["status"] as! Int == 200 {
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: "Tạo câu hỏi thành công.", complete: {
                        self.navigationController?.popViewController(animated: true)
                    })
    
                    SyncHttpConnect.getQAgency(pageMax: 1, LIMITPAGEMAX: 10) { (data, success) in
                        if success == true && QAAgencyAccessor.set(data: data) == true {
                        } else {
                        }
                    }
                    
                    
                    
                } else if data["status"] as! Int == 400 {
                    let messageError = "Lỗi không xác định"
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: messageError, complete: nil)
                    
                }
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
        }

    }


}
