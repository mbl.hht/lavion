//
//  UserDefault.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/21/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class UserDefault: NSObject {
    static func getToken() -> String {
        return UserDefaults.standard.string(forKey: "access_token") ?? ""
    }
    
    static func setToken(_ token : String ) {
        UserDefaults.standard.set(token, forKey: "access_token")
        UserDefaults.standard.synchronize()
    }
    
    
    static func setEmail(_ email : String ) {
        UserDefaults.standard.set(email, forKey: "email")
        UserDefaults.standard.synchronize()
    }
    
    static func getEmail() -> String {
        return UserDefaults.standard.string(forKey: "email") ?? ""
    }
    
    static func setPassword(_ password : String ) {
        UserDefaults.standard.set(password, forKey: "password")
        UserDefaults.standard.synchronize()
    }
    
    static func getPassword() -> String {
        return UserDefaults.standard.string(forKey: "password") ?? ""
    }
    
    

}
