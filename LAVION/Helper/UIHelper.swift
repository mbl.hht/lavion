//
//  UIHelper.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/22/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import SVProgressHUD

class UIHelper: NSObject {
    static func showHUD() {
        DispatchQueue.main.async {
            SVProgressHUD.setDefaultMaskType(.black)
            SVProgressHUD.show();
        }
    }
    
    static func dismisHUD() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    static func heightForView(text:String, width:CGFloat, font:UIFont) -> CGFloat{
       
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.setLineSpacing(lineSpacing: 1.25)
        label.sizeToFit()
        return label.frame.height
    }
    
    static func convertStringToDate(date: String,format: String = "yyyy-MM-dd HH:mm:ss +zzzz", returnFormat: String = "dd/MM/yyyy") ->String {
        let simpleDateFormat = DateFormatter()
        simpleDateFormat.dateFormat =  format//format our date String
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = returnFormat //format return
        let dateAfter = simpleDateFormat.date(from: date)
        return dateFormat.string(from: dateAfter ?? Date.init())
    }
    

}


extension UIColor {
    static let mainColorGreen = UIColor(red: 138/255.0, green: 184/255.0, blue: 75/255.0, alpha: 1.0)
    static let mainColor = UIColor("#fe8c00")
    convenience init(colorCode: String, alpha: Float = 1.0){
        let scanner = Scanner(string:colorCode)
        var color:UInt32 = 0;
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = CGFloat(Float(Int(color >> 16) & mask)/255.0)
        let g = CGFloat(Float(Int(color >> 8) & mask)/255.0)
        let b = CGFloat(Float(Int(color) & mask)/255.0)
        
        self.init(red: r, green: g, blue: b, alpha: CGFloat(alpha))
    }
    
}


extension UIViewController {
    public func initNavigation(text:String?, hidden: Bool = false) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.topItem?.title = text
        self.navigationController?.navigationBar.barTintColor = UIColor.mainColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 20)!]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.tabBarController?.tabBar.isHidden = hidden
    }
}

extension UIButton {
    public func initRadius() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.clear.cgColor
        self.backgroundColor = UIColor.mainColor
    }
    
    public func setShadow(color: UIColor = UIColor.black, cornerRadius: CGFloat = 5) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.5
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.clear.cgColor
        self.clipsToBounds = false
        
    }
}


extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhones_4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_X_XS = "iPhone X or iPhone XS"
        case iPhone_XR = "iPhone XR"
        case iPhone_XSMax = "iPhone XS Max"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhones_4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1792:
            return .iPhone_XR
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhones_X_XS
        case 2688:
            return .iPhone_XSMax
        default:
            return .unknown
        }
    }
}
