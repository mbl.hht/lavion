//
//  ErrorHelper.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/21/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class ErrorHelper: NSObject {
    static func showErrorWithSelf(_ viewController : UIViewController , titleString : String, errorString : String,complete :Enum.Complete?) {
        let alertController = UIAlertController(title: titleString, message:
            errorString, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
            if complete != nil {
                complete!()
            }
        }))
        viewController.present(alertController, animated: true, completion: nil)
        
    }
    
    static func showDisconnectInternet(viewController : UIViewController) {
        showErrorWithSelf(viewController, titleString: "Lỗi kết nối", errorString: "Vui lòng xem lại kết nối Internet", complete: nil)
    }
}
