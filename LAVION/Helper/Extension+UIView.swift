//
//  extension+UIView.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/15/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import Foundation
import Swift
import UIKit
import HexColors

extension UIView {
    func initGradient(colorStart: String!, colorEnd: String!, defautRadius: Int = 5) {
        var widthTypeIphone:CGFloat = 0.0
        switch UIDevice.current.screenType {
        case .iPhone_XSMax,.iPhone_XR,.iPhones_X_XS:
            widthTypeIphone = 13
            break;
        default :
            widthTypeIphone = 0.0
        }
        
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y, width: self.bounds.width + widthTypeIphone, height: self.bounds.height)
        layer.colors = [UIColor(colorStart)!.cgColor ,UIColor(colorEnd)!.cgColor]
        layer.startPoint = CGPoint(x: 1, y: 0.5)
        layer.endPoint = CGPoint(x: 0, y: 0.5)
        if defautRadius > 0 {
            self.layer.cornerRadius = 5
            self.layer.borderWidth = 1
            self.layer.borderColor = UIColor.clear.cgColor
            layer.cornerRadius = 5
            layer.borderWidth = 1
            layer.borderColor = UIColor.clear.cgColor
            
        }
        self.layer.insertSublayer(layer, at: 0)
    }
    
    
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    
    func addShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func addShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func initShadowForCell() {
        self.backgroundColor = .white // need background
        let layer = self.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.2
        layer.cornerRadius = 5

    }
    
    func initShadowForCellType2() {
//        // corner radius
//        self.layer.cornerRadius = 10
//
//        // border
//        self.layer.borderWidth = 1.0
//        self.layer.borderColor = UIColor.clear.cgColor
//
//        // shadow
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.shadowOffset = CGSize(width: 0, height: 0)
//        self.layer.shadowOpacity = 0.7
//        self.layer.shadowRadius = 4.0
        self.backgroundColor = .white // need background
        let layer = self.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.2
        layer.cornerRadius = 5
    }
}

