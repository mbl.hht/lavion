//
//  Utilty.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/2/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
class Utilty: NSObject {
}

extension Double {
    func formatNumber() -> String {
        let formater = NumberFormatter()
        formater.groupingSeparator = ","
        formater.numberStyle = .decimal
        return formater.string(from: NSNumber(value: self))!
    }
}
