//
//  SyncHttpConnect.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/18/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import Alamofire
//typealias SyncHttpCompletion = (_ result: NSDictionary, _ error: Bool) -> Void

let LIMIT_PAGE_MAX  = 1000000
let PAGE_MAX        = 1


let address : String                = "https://lavion.vn" //"https://msm.lienminh.club" //

let loginUrl: String                        = address + "/api/cong-tac-vien/login"
let forgetUrl: String                       = address + "/api/cong-tac-vien/forgot"
let registerUrl: String                     = address + "/api/cong-tac-vien/register"
let profileUrl: String                      = address + "/api/cong-tac-vien/ho-so"
let maGiamGiaUrl: String                    = address + "/api/cong-tac-vien/ma-giam-gia/list"
let bankListUrl: String                     = address + "/api/cong-tac-vien/ngan-hang/list"
let changePasswordUrl: String               = address + "/api/cong-tac-vien/doi-mat-khau"
let uploadImageUrl: String                  = address + "/api/cong-tac-vien/ho-so/update-img"
let changeInfoAccountUrl: String            = address + "/api/cong-tac-vien/ho-so/update"
let changeInfoBankUrl: String               = address + "/api/cong-tac-vien/ngan-hang/update"
let statisticUrl: String                    = address + "/api/cong-tac-vien/thong-ke"
let transactionHistoryUrl:String            = address + "/api/cong-tac-vien/lich-su-giao-dich"
let vNDWithdrawalHistoryUrl: String         = address + "/api/cong-tac-vien/rut-tien"
let vNDWithdrawalUrl: String                = address + "/api/cong-tac-vien/rut-tien"
let historyTransferMILKToVNDUrl: String     = address + "/api/cong-tac-vien/chuyen-milk-thanh-vnd"
let generalSettingUrl                       = address + "/api/cong-tac-vien/cai-dat-chung/list"
let transferMILKToVNDUrl                    = address + "/api/cong-tac-vien/chuyen-milk-thanh-vnd"
let historyTranferMILKToLocalUrl            = address + "/api/cong-tac-vien/chuyen-milk-cho-noi-bo"
let checkEmailUrl                           = address + "/api/cong-tac-vien/chuyen-milk-cho-noi-bo/search?email="
let tranferMILKToLocalDUrl                  = address + "/api/cong-tac-vien/chuyen-milk-cho-noi-bo"
let mILKLoadUrl                             = address + "/api/cong-tac-vien/nap-milk/nap"
let historyMILKLoadUrl                      = address + "/api/cong-tac-vien"
let qAgencyUrl                              = address + "/api/cong-tac-vien/hoi-dap-dai-ly"
let productsUrl                             = address + "/api/cong-tac-vien/mua-san-pham"






class SyncHttpConnect: NSObject {
    
    static func postLogin(Email email: String, Password password: String, Complete complete: @escaping Enum.SyncHttpCompletion) {
        let user : [String : String] = ["email"     : email,
                                        "password"  : password];
        let urlString = loginUrl;
        Alamofire.request(urlString, method: .post, parameters: user,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(NSDictionary.init(), false)
            }
        }
        
    }
    
    static func postForget(email : String, complete : @escaping Enum.SyncHttpCompletion) {
        let user : [String : String] = ["email" : email];
        let urlString = forgetUrl;
        Alamofire.request(urlString, method: .post, parameters: user,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(NSDictionary.init(), false)
            }
        }
        
    }
    
    static func postRegister( email : String, password : String, ma_dk :String,  tinh_thanh : String, cmnd : String, ho_va_ten : String, so_dien_thoai : String, ma_gt : String, Complete complete: Enum.SyncHttpCompletion?) {
        let user : [String : String] = ["email"             : email ,
                                        "password"          : password,
                                        "ma_dang_ky"        : ma_dk,
                                        "tinh_thanh"        : tinh_thanh,
                                        "cmnd"              : cmnd,
                                        "ho_va_ten"         : ho_va_ten,
                                        "so_dien_thoai"     : so_dien_thoai,
                                        "ma_gioi_thieu"     : ma_gt];
        
        let urlString = registerUrl;
        Alamofire.request(urlString, method: .post, parameters: user,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                if (complete != nil) {
                    complete!(value as? NSDictionary ,true)
                }
                break
            case .failure:
                if (complete != nil) {
                    complete!(nil, false)
                }
                //complete(NSDictionary.init(), false)
            }
        }
        
    }
    
    
    static func putChangePassword(oldPassWord:String = "", newPassword:String = "", Complete complete: @escaping Enum.SyncHttpCompletion) {
        let urlString = changePasswordUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let body = [
            "current_password" : oldPassWord,
            "new_password"      : newPassword
        ]
        
        Alamofire.request(urlString, method: .put, parameters: body,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
        
    }
    
    
    static func upLoadImage(listImage: Dictionary<String, UIImage>, Complete complete: @escaping Enum.SyncHttpCompletion) {
        
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let parameters = ["-method": "PUT"] //Optional for extra parameter
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            for (key,value) in listImage {
                print("key:\(key) value:\(value)")
                let imgData = value.jpegData(compressionQuality: 0.2)!
                multipartFormData.append(imgData, withName: key, mimeType: "image/jpg")
            }
        
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
        usingThreshold:UInt64.init(),
        to:uploadImageUrl,
        method:.post,
        headers: headerDictionary,
        encodingCompletion:
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print(response.result.value!)
                    complete(response.result.value as? NSDictionary , true)
                }
                
            case .failure(let encodingError):
                complete(NSDictionary.init() , false)
                print(encodingError)
            }
        })
    }
    
    static func changeInformationMember(numberPhone: String, facebook: String,addressReal:String, addressCMND: String, gender: String, birthday: Int64,  Complete complete: @escaping Enum.SyncHttpCompletion) {
        
        let urlString = changeInfoAccountUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let body = [
            "so_dien_thoai": numberPhone,
            "facebook": facebook,
            "dia_chi_giao_hang": addressReal,
            "dia_chi_theo_chung_minh": addressCMND,
            "gioi_tinh": gender,
            "ngay_sinh": birthday
            ] as [String : Any]
        
        Alamofire.request(urlString, method: .put, parameters: body,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
    }
    
    
    static func changeInformationBank(numberAccount: String, nameAccount: String, nameBank: String, idNganHang: Int,  Complete complete: @escaping Enum.SyncHttpCompletion) {
        
        let urlString = changeInfoBankUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let body = [
            "so_tai_khoan": numberAccount,
            "ten_chu_tai_khoan": nameAccount,
            "ten_chi_nhanh": nameAccount,
            "id_ngan_hang": idNganHang
            ] as [String : Any]
        
        Alamofire.request(urlString, method: .put, parameters: body,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
    }
    
    static func postWithdrawalVND(moneyVND: String, complete: @escaping SyncHttpCompletion) {
        let urlString = vNDWithdrawalUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let body = [
            "so_tien": moneyVND
            ] as [String : Any]
        
        Alamofire.request(urlString, method: .post, parameters: body,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    
    
    
    static func putTransferMILKToVND(MILK: String, complete: @escaping SyncHttpCompletion) {
        let urlString = transferMILKToVNDUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let body = [
            "so_milk": MILK
            ] as [String : Any]
        
        Alamofire.request(urlString, method: .put, parameters: body,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    static func putTranferMILKToLocal(email: String, MILK: String, complete: @escaping SyncHttpCompletion) {
        let urlString = tranferMILKToLocalDUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let body = [
            "email_nguoi_nhan"  : email,
            "so_milk"           : MILK
            ] as [String : Any]
        
        Alamofire.request(urlString, method: .put, parameters: body,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    
    static func postMILKLoad(ma_nap_diem: String, seri: String, complete: @escaping SyncHttpCompletion) {
        let urlString = mILKLoadUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let body = [
            "ma_nap_diem"  : ma_nap_diem,
            "seri"           : seri
            ] as [String : Any]
        
        Alamofire.request(urlString, method: .post, parameters: body,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    static func postQAgency(cau_hoi: String, complete: @escaping SyncHttpCompletion) {
        let urlString = qAgencyUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let body = [
            "cau_hoi"  : cau_hoi
            ] as [String : Any]
        
        Alamofire.request(urlString, method: .post, parameters: body,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    static func postAQAgency(id: Int,cau_tra_loi: String, complete: @escaping SyncHttpCompletion) {
        let urlString = qAgencyUrl + "/\(id)/tra-loi"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        let body = [
            "tra_loi"  : cau_tra_loi
            ] as [String : Any]
        
        Alamofire.request(urlString, method: .post, parameters: body,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }

    
    
    
    
    // GET
    static func getProfile(Complete complete: @escaping Enum.SyncHttpCompletion) {
        let urlString = profileUrl;
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
        
    }
    
    static func getMaGiamGia(Complete complete: @escaping Enum.SyncHttpCompletion) {
        let urlString = maGiamGiaUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
        
    }
    
    static func getBankList(Complete complete: @escaping Enum.SyncHttpCompletion) {
        let urlString = bankListUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
        
    }
    
    
    static func getStatistic(Complete complete: @escaping Enum.SyncHttpCompletion) {
        let urlString = statisticUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
        
    }
    
    static func getVNDTransactionHistory(Complete complete: @escaping Enum.SyncHttpCompletion) {
        let urlString = transactionHistoryUrl + "/vnd?page=\(PAGE_MAX)&limit=\(LIMIT_PAGE_MAX)"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
        
    }
    
    
    static func getVNDWithdrawalHistory(Complete complete: @escaping Enum.SyncHttpCompletion) {
        let urlString = vNDWithdrawalHistoryUrl + "/list?page=\(PAGE_MAX)&limit=\(LIMIT_PAGE_MAX)"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
        
    }
    
    
    
    static func getMILKTransactionHistory(Complete complete: @escaping Enum.SyncHttpCompletion) {
        let urlString = transactionHistoryUrl + "/milk?page=\(PAGE_MAX)&limit=\(LIMIT_PAGE_MAX)"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? NSDictionary ,true)
                break
            case .failure:
                complete(nil, false)
            }
        }
        
    }
    
    
    static func getHistoryTransferMILKToVND(Complete complete: @escaping SyncHttpCompletion) {
        let urlString = historyTransferMILKToVNDUrl + "/list?page=\(PAGE_MAX)&limit=\(LIMIT_PAGE_MAX)"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    
    static func getGeneralSetting(Complete complete: @escaping SyncHttpCompletion) {
        let urlString = generalSettingUrl
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    static func getHistoryTranferMILKToLocal(Complete complete: @escaping SyncHttpCompletion) {
        let urlString = historyTranferMILKToLocalUrl + "/list?page=\(PAGE_MAX)&limit=\(LIMIT_PAGE_MAX)"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    static func getCheckEmail(email: String, complete: @escaping SyncHttpCompletion) {
        let urlString = checkEmailUrl + email
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    static func getHistoryMILKLoad(complete: @escaping SyncHttpCompletion) {
        let urlString = historyMILKLoadUrl + "/nap-milk?page=\(PAGE_MAX)&limit=\(LIMIT_PAGE_MAX)"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    
    
    static func getQAgency(pageMax: Int = PAGE_MAX,LIMITPAGEMAX: Int = LIMIT_PAGE_MAX, complete: @escaping SyncHttpCompletion) {
        let urlString = qAgencyUrl + "/list?page=\(pageMax)&limit=\(LIMITPAGEMAX)"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    static func getDetailQAgency(id: Int ,complete: @escaping SyncHttpCompletion) {
        let urlString = qAgencyUrl + "/\(id)/show"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    
    static func getProducts(page: Int = PAGE_MAX,limit: Int = LIMIT_PAGE_MAX, complete: @escaping SyncHttpCompletion) {
        let urlString = productsUrl + "/list?page=\(page)&limit=\(limit)"
        let headerDictionary = [
            "Authorization" : "Bearer \(UserDefault.getToken())",
            "Content-Type"  : "application/json",
            "Accept"        : "application/json"
        ]
        
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headerDictionary).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                complete(value as? Dictionary<String,Any> ?? Dictionary<String, Any>.init(),true)
                break
            case .failure:
                complete(Dictionary<String, Any>.init(), false)
            }
        }
    }
    
    
    
    
    
    
    
}

