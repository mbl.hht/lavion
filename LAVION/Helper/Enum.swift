//
//  Enum.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/22/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import Foundation
enum TypeHomeTitleHeader: Int {
    case None = -1
    case SanPhamHot = 0
    case TimkiemPhoBien = 1
    case DanhMucNganhHang = 2
    case DanhSachSanPham = 3
}

enum TypeAPICallLogin: Int {
    case TypeProfile = 1
    case TypeMaGiamGia
    case TypeBankList
    case TypeStatistic
    case TypeVNDTransactionHistory
    case TypeVNDWithdrawalHistory
    case TypeQAAgency
    case TypeProducts
}

enum TypeStatusCode:Int {
    case success = 200
    case failed  = 400
}

enum TypeLogin {
    case None
    case AutoLogin
    case EnterUser
}

enum TypeLogout {
    case None
    case Button
    case FailAPIWhenLogining
    case FailAPIWhenLogined
}

typealias SyncHttpCompletion = (_ result: Dictionary<String ,Any >, _ error: Bool) -> Void


class Enum: NSObject {
    typealias SyncHttpCompletion = (_ result: NSDictionary?, _ error: Bool) -> Void
    typealias Complete = () -> Void
    enum TypeHeaderMember: String {
        case None               = ""
        case MaGiamGia          = "Mã giới thiệu của bạn"
        case NguoiGioiThieu     = "Người giới thiệu bạn"
        case HeThongCuaToi      = "Hệ thống của tôi"
        case CapNhatCMND        = "Cập nhật CMND"
        case ThongTinCaNhan     = "Thông tin cá nhân"
        case TaiKhoanNganHang   = "Tài khoản ngân hàng"
    }
    
    enum TypeColor:String {
        case mainColor = "5DAC3D"
    }
    
    
    enum TypeImageUpload {
        case None
        case frontPerson
        case backSidePerson
        case avatarPerson
    }
    
}
