//
//  LoginHelper.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/27/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

let appDelegate = UIApplication.shared.delegate as! AppDelegate


class LoginHelper: NSObject {
    static var isLogining = true
    static var typeLogin : TypeLogin = .None
    static let arrayLogin = [TypeAPICallLogin.TypeProfile, .TypeMaGiamGia, .TypeBankList, .TypeStatistic, .TypeVNDTransactionHistory, .TypeVNDWithdrawalHistory, .TypeQAAgency, .TypeProducts]
    static var arrayList  = [TypeAPICallLogin]()
    
    static func loadData() {
        arrayList.removeAll()
        
        SyncHttpConnect.getProducts { (data, success) in
            if success == true && ProductAccessor.set(data: data) == true {
                callLoadMenu(addType: .TypeProducts)
            } else {
                logout(type: .FailAPIWhenLogining)
            }
        }
        
        SyncHttpConnect.getProfile { (data: NSDictionary?, success: Bool) in
            if success == true && ProfileAccessor.set(data?.object(forKey: "data") as? NSDictionary) == true {
                callLoadMenu(addType: .TypeProfile)
                SyncHttpConnect.getMaGiamGia { (data: NSDictionary?, success: Bool) in
                    if success == true && ProfileAccessor.setMaGiamGia(maGiamGia: data?.object(forKey: "data") as? Int ?? 0) == true {
                        callLoadMenu(addType: .TypeMaGiamGia)
                    } else {
                        logout(type: .FailAPIWhenLogining)
                    }
                    
                }

            } else {
                 logout(type: .FailAPIWhenLogining)
            }
            
        }
        
        SyncHttpConnect.getBankList { (data: NSDictionary?, success: Bool) in
            if success == true && BankAccessor.set(data?.object(forKey: "data") as? NSArray) == true {
                callLoadMenu(addType: .TypeBankList)
            } else {
                 logout(type: .FailAPIWhenLogining)
            }
            
        }
        
        
        SyncHttpConnect.getStatistic { (data: NSDictionary?, success: Bool) in
            if success == true && StatisticAccessor.set(data: data as! Dictionary<String, Any>) == true {
                callLoadMenu(addType: .TypeStatistic)
            } else {
                 logout(type: .FailAPIWhenLogining)
            }
        }
        
        
        SyncHttpConnect.getVNDTransactionHistory { (data: NSDictionary?, success: Bool) in
            if success == true && VNDTransactionHistoryAccessor.set(data: data as! Dictionary<String, Any>) == true {
                callLoadMenu(addType: .TypeVNDTransactionHistory)
            } else {
                 logout(type: .FailAPIWhenLogining)
            }
        }
        
        
        SyncHttpConnect.getVNDWithdrawalHistory { (data: NSDictionary?, success: Bool) in
            if success == true && VNDWithdrawalAccessor.set(data: data as! Dictionary<String, Any>) == true {
                callLoadMenu(addType: .TypeVNDWithdrawalHistory)
            } else {
                logout(type: .FailAPIWhenLogining)
            }
        }
        
        SyncHttpConnect.getQAgency { (data, success) in
            if success == true && QAAgencyAccessor.set(data: data) == true {
                callLoadMenu(addType: .TypeQAAgency)
            } else {
                logout(type: .FailAPIWhenLogining)
            }
        }
        

        
        
        
        
        // MARK: Lấy những Api không quan trọng
 
        SyncHttpConnect.getMILKTransactionHistory { (data: NSDictionary?, success: Bool) in
            if success == true && MILKTransactionHistoryAccessor.set(data: data as! Dictionary<String, Any>) == true {
                //callLoadMenu(addType: .TypeVNDTransactionHistory)
            } else {
                //logout(type: .FailAPIWhenLogining)
            }
        }
        
        SyncHttpConnect.getHistoryTransferMILKToVND { (data, success) in
            if success == true && HistoryTransferMILKToVNDAccessor.set(data: data) == true {
                //callLoadMenu(addType: .TypeVNDTransactionHistory)
            } else {
                //logout(type: .FailAPIWhenLogining)
            }
        }
        
        SyncHttpConnect.getGeneralSetting { (data, success) in
            if success == true && GeneralSettingAccessor.set(data: data) == true {
                //callLoadMenu(addType: .TypeVNDTransactionHistory)
            } else {
                //logout(type: .FailAPIWhenLogining)
            }
        }
        
        SyncHttpConnect.getHistoryTranferMILKToLocal { (data, success) in
            if success == true && HistoryTranferMILKToLocalAccessor.set(data: data) == true {
                //callLoadMenu(addType: .TypeVNDTransactionHistory)
            } else {
                //logout(type: .FailAPIWhenLogining)
            }
        }
        
        SyncHttpConnect.getHistoryMILKLoad { (data, success) in
            if success == true && HistoryMILKLoadAccessor.set(data: data) == true {
                //callLoadMenu(addType: .TypeVNDTransactionHistory)
            } else {
                //logout(type: .FailAPIWhenLogining)
            }
        }
        
        
    }
    
    static func callLoadMenu(addType: TypeAPICallLogin) {
        DispatchQueue.main.async {
            arrayList.append(addType)
            print("\(arrayList.count) \(arrayLogin.count)")
            print("\(addType)")
            if arrayLogin.count == arrayList.count {
                appDelegate.loadMenu()
                isLogining = false
            }
        }

    }
    
    static func logout(type: TypeLogout = .None) {
        if !isLogining {
            return
        }
        
        DispatchQueue.main.async {
            let realm = try! Realm()
            try! realm.write {
                realm.deleteAll()
            }
            UserDefault.setToken("")
            UserDefault.setEmail("")
            UserDefault.setPassword("")
            arrayList.removeAll()
            isLogining = true
//            switch type {
                
//            case .FailAPIWhenLogined, .Button:
//                appDelegate.loadLogin()
//
//            case .FailAPIWhenLogining:
//                if typeLogin == .AutoLogin {
//                     appDelegate.loadLogin()
//                }
//            default:
//                break
//            }
            
            appDelegate.loadLogin()
            
        }
    }
    
    
    
    
}


