//
//  Extension+UITextView.swift
//  LAVION
//
//  Created by iOS on 12/3/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    func initShadowUITextView() {
        //self.clipsToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 5
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.5
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.clear.cgColor
    }
}
