//
//  GeneralInformationTableCell.swift
//  LAVION
//
//  Created by iOS on 12/2/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class GeneralInformationTableCell: UITableViewCell {
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateAndQuetsionLabel: UILabel!
    @IBOutlet weak var persionLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    var item: QAAgencyModel? {
        didSet {
            contentLabel.text = item?.noi_dung_cau_hoi
            dateAndQuetsionLabel.text = "( " + (item?.created_at ?? "") + " ) \(item?.so_luong_cau_tra_loi ?? 0) câu trả lời"
            
            persionLabel.text = "Người hỏi : \(item?.ho_va_ten ?? "") "
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.initShadowForCellType2()
    }
    
}
