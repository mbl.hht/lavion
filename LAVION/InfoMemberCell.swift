//
//  InfoMemberCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/26/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class InfoMemberCell: UITableViewCell {

    @IBOutlet weak var fieldLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func intCell(textField: String?,textValue: String? ,isOpen: Bool?) {
        fieldLabel?.text = textField ?? ""
        valueLabel?.text = textValue ?? ""
        if isOpen! {
            self.accessoryType = .disclosureIndicator
        } else {
            self.accessoryType = .none
        }
    }
}
