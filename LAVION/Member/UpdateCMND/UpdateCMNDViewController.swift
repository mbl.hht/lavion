//
//  UpdateCMNDViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/27/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class UpdateCMNDViewController: UIViewController {
    
    @IBOutlet weak var frontCMNDView: UIView!
    @IBOutlet weak var frontCMNDImageView: UIImageView!
    @IBOutlet weak var frontCMNDButton: UIButton!
    
    @IBOutlet weak var backsideCMNDView: UIView!
    @IBOutlet weak var backsideCMNDImageView: UIImageView!
    @IBOutlet weak var backsedeCMNDButton: UIButton!
    
    @IBOutlet weak var avatarView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var avatarButton: UIButton!
    
    @IBOutlet weak var summitAccountButton: UIButton!
    

    
    
    var imagePicker = UIImagePickerController()
    var typeUploadImage : Enum.TypeImageUpload = .None

    var profile:ProfileModel!
    var flashUpload = Set<String>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationController
        initNavigation(text: "Thông tin CMND")
        
        
        //summitAccountButton
        summitAccountButton.layer.cornerRadius = 5
        summitAccountButton.layer.borderWidth = 1
        summitAccountButton.layer.borderColor = UIColor.clear.cgColor
        summitAccountButton.addTarget(self, action: #selector(self.summitAccountAction(sender:)), for: .touchUpInside)
        
        frontCMNDButton.addTarget(self, action: #selector(self.uploadFrontCMNDAction(sender:)), for: .touchUpInside)
        backsedeCMNDButton.addTarget(self, action: #selector(self.uploadBacksedeCMNDAction(sender:)), for: .touchUpInside)
        avatarButton.addTarget(self, action: #selector(self.uploadAvatarCMNDAction(sender:)), for: .touchUpInside)
        
       //Image Picker
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        
        
        let realm = try! Realm()
        profile = realm.objects(ProfileModel.self).first
        frontCMNDImageView.af_setImage(withURL: URL.init(string: address + profile.img_01)!)
        backsideCMNDImageView.af_setImage(withURL: URL.init(string: address + profile.img_02)!)
        avatarImageView.af_setImage(withURL: URL.init(string: address + profile.avatar)!)
        
        var current = 0
        if profile.img_02.count > 0 {
            frontCMNDButton.isEnabled = false
            current += 1;
        }
        if profile.img_01.count > 0  {
            backsedeCMNDButton.isEnabled = false
            current += 1;
        }
        if profile.avatar.count > 0 {
            avatarButton.isEnabled  = false
            current += 1
        }
        
        if current == 3 {
            lockSummit()
        }
        
        
        
    }

    @objc func summitAccountAction(sender: UIButton) {
        flashUpload.removeAll()
        var paramater = Dictionary<String, UIImage>()
        if(profile.img_01.count == 0) {
            
            paramater["img_01"] = frontCMNDImageView.image
            flashUpload.insert("img_01")
            
        }
        if(profile.img_02.count == 0) {
            
            paramater["img_02"] = frontCMNDImageView.image
            flashUpload.insert("img_02")
        }
        if(profile.avatar.count == 0) {
            
            paramater["avatar"] = frontCMNDImageView.image
            flashUpload.insert("avatar")
        }
        UIHelper.showHUD()
        SyncHttpConnect.upLoadImage(listImage: paramater) { (returnData: NSDictionary?, sccess: Bool) in
            UIHelper.dismisHUD()
            if sccess {
                if returnData?.object(forKey: "data") is String {
                    
                    let data = returnData?.object(forKey: "data") as! String
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: data, complete: nil)
                    
                } else if returnData?.object(forKey: "data") is NSDictionary {
                    
                    let data = returnData?.object(forKey: "data") as! NSDictionary
                    let img_02 = data.value(forKey: "img_02") as! String
                    let img_01 = data.value(forKey: "img_02") as! String
                    let avatar = data.value(forKey: "avatar") as! String
                    var count = 0
                    autoreleasepool {
                        let realm = try! Realm()
                        try! realm.write {
                            if img_02.count > 0 {
                                self.profile.img_02 = img_02
                                count += 1
                            }
                            if img_01.count > 0 {
                                self.profile.img_01 = img_01
                                count += 1
                            }
                            if avatar.count > 0 {
                                self.profile.avatar = avatar
                                count += 1
                            }
                            
                        }
                    }
                    
                    if count == 3 {
                        self.lockSummit()
                    }
                    
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông Báo", errorString: "Cập nhập thành công.", complete: nil)
                }
                
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
        }
        
    }
    
    @objc func uploadFrontCMNDAction(sender: UIButton) {
        typeUploadImage = .frontPerson
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    @objc func uploadBacksedeCMNDAction(sender: UIButton) {
        typeUploadImage = .backSidePerson
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    @objc func uploadAvatarCMNDAction(sender: UIButton) {
        typeUploadImage = .avatarPerson
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    func lockSummit() {
        summitAccountButton.isEnabled = false
        summitAccountButton.backgroundColor = UIColor.gray
        
    }
}

extension UpdateCMNDViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            switch typeUploadImage {
            case .avatarPerson:
                avatarImageView.image = image
            case .frontPerson:
                frontCMNDImageView.image = image
            case .backSidePerson:
                backsideCMNDImageView.image = image
            default :
                break
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    
}

