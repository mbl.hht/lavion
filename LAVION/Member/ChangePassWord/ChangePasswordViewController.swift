//
//  ChangePasswordViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/27/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
//import HexColors

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var passwordOldTextField: UITextField!
    @IBOutlet weak var passwordNewTextField: UITextField!
    @IBOutlet weak var rePasswordNewTextField: UITextField!
    @IBOutlet weak var changePasswordButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Settup navigationController
        initNavigation(text: "Đổi mật khẩu")

        
        //TextField
        passwordNewTextField.isSecureTextEntry = true
        
        passwordOldTextField.isSecureTextEntry = true
        rePasswordNewTextField.isSecureTextEntry = true
        
        // ChangePassword Button
        changePasswordButton.layer.cornerRadius = 5
        changePasswordButton.layer.borderWidth = 1
        changePasswordButton.layer.borderColor = UIColor.clear.cgColor
        changePasswordButton.addTarget(self, action: #selector(self.changePasswordAction(_:)), for: .touchUpInside)

    }
    
    @objc func changePasswordAction(_ sender: UIButton) {
        if passwordOldTextField.text?.count == 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Mật khẩu hiện tại không được để trống.", errorString: "",complete: nil)
            
            return;
        }
        if passwordNewTextField.text?.count == 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Mật khẩu mới không được để trống.", errorString: "",complete: nil)
            
            return;
        }
        if rePasswordNewTextField.text?.count == 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Nhập lại mật khẩu không được để trống.", errorString: "",complete: nil)
            
            return;
        }
        
        if rePasswordNewTextField.text != passwordNewTextField.text {
            ErrorHelper.showErrorWithSelf(self, titleString: "Nhập lại mật khẩu không khớp.", errorString: "",complete: nil)
            
            return;
        }
        
        UIHelper.showHUD()
        SyncHttpConnect.putChangePassword(oldPassWord: passwordOldTextField.text ?? "", newPassword: passwordNewTextField.text ?? "") { (data:NSDictionary?, sucess:Bool) in
            UIHelper.dismisHUD()
            var mess:String = ""
            if sucess {
                if !(data?.value(forKey: "errors") is NSNull) {
                    mess = ((data?.value(forKey: "errors") as! NSDictionary).value(forKey: "message") as! NSArray).firstObject as! String
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: mess,complete: nil)
                }
                else if let chidData = data?.value(forKey: "message") {
                    mess = chidData as! String
                    UserDefault.setPassword(self.passwordNewTextField.text!)
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: mess,complete: {
                            self.navigationController?.popViewController(animated: true)
                    } )
                }
                
                
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
        }
        
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

}


