//
//  InfomationMemberViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/28/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class InfomationMemberViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var numberCMNDTextField: UITextField!
    
    @IBOutlet weak var brithdayTextField: UITextField!
    
    @IBOutlet weak var numberPhoneTextField: UITextField!
    
    @IBOutlet weak var facebookTextField: UITextField!
    
    @IBOutlet weak var AddressTextField: UITextField!
    
    @IBOutlet weak var AddressCMNDTextField: UITextField!
    
    @IBOutlet weak var genderTextField: UITextField!
    
    @IBOutlet weak var summitButton: UIButton!
    
    var brithdayText: String = ""
    var brithdayInt: Int64 = 0
    var genderText: String = ""
    var pickOption = ["NAM","NỮ"]
    
    var profile: ProfileModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //Settup navigationController
        initNavigation(text: "Thông tin khách hàng")
        
        //Setting TextField
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        brithdayTextField.inputView = datePicker
        brithdayTextField.text = "01/10/2018"
        datePicker.addTarget(self, action: #selector(self.datePickerValueChanged(sender:)), for: UIControl.Event.valueChanged)
        brithdayTextField.delegate = self
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        genderTextField.inputView = pickerView
        //genderTextField.text = "Nữ"
        genderTextField.delegate = self
        
        numberCMNDTextField.keyboardType = .numberPad
        
        numberPhoneTextField.keyboardType = .namePhonePad
        
        summitButton.layer.cornerRadius = 5
        summitButton.layer.borderWidth = 1
        summitButton.layer.borderColor = UIColor.clear.cgColor
        
        let realm = try! Realm()
        profile = realm.objects(ProfileModel.self).first
        
        loadData()
        
        
        

    }
    
    @IBAction func summitAction(_ sender: UIButton) {
        var messageError = ""
        if brithdayTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Ngày sinh không được để trống"
        } else if numberPhoneTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Số điện thoại không được bỏ trống"
        } else if facebookTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Facebook không được bỏ trống"
        } else if AddressCMNDTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Địa chỉ chứng minh không được bỏ trống"
        } else if AddressTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Địa chỉ hiện tại không được bỏ trống"
        } else if genderTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Giới tính không được bỏ trống"
        } 
        
        if messageError.count > 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Thông Báo", errorString: messageError, complete: nil)
            return
        }
        
        UIHelper.showHUD()
        SyncHttpConnect.changeInformationMember(numberPhone: numberPhoneTextField.text!, facebook: facebookTextField.text!, addressReal: AddressTextField.text!, addressCMND: AddressCMNDTextField.text!, gender: genderTextField.text!, birthday: brithdayInt) { (data1: NSDictionary?, success: Bool) in
            UIHelper.dismisHUD()
            if success {
                if data1?.value(forKey: "status") as! Int == 200 {
                    
                    
                    autoreleasepool {
                        let realm = try! Realm()
                        try! realm.write {
                            self.profile.ngaySinh       = self.brithdayTextField.text!
                            self.profile.soDienThoai    = self.numberPhoneTextField.text!
                            self.profile.facebook       = self.facebookTextField.text!
                            self.profile.diaChiCmnd     = self.AddressCMNDTextField.text!
                            self.profile.diaChiHienTai  = self.AddressTextField.text!
                            self.profile.gioiTinh       = self.genderTextField.text!
                            
                        }
                    }
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: "Cập nhật thành công.", complete: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    
                    
                } else if data1?.value(forKey: "status") as! Int == 400 {
                    messageError = ""
                    let errorDic = (data1?.value(forKey: "errors") as! Dictionary<String, Array<String>>)
                    errorDic.values.forEach({ (value: Array<String>) in
                        messageError += (value.first ?? "") + "\n"
                    })
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: messageError, complete: nil)
    
                }
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
            
            
        }
        
        
    }
    
    
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        brithdayInt = Int64(sender.date.timeIntervalSince1970)
        brithdayText = dateFormatter.string(from: sender.date)
        
    }
    
    
    func setValueTextField(textField: UITextField, value: String?, enabled: Bool = false) {
        if (value?.count ?? 0) > 0 {
            textField.text = value
            textField.isEnabled = enabled
        }
    }
    
    
    func loadData() {
        setValueTextField(textField: nameTextField,         value: profile.hoVaTen)
        setValueTextField(textField: emailTextField,        value: profile.email)
        setValueTextField(textField: numberCMNDTextField,   value: profile.cmnd)
        setValueTextField(textField: numberPhoneTextField,  value: profile.soDienThoai, enabled: true)
        setValueTextField(textField: facebookTextField,     value: profile.facebook, enabled: true)
        setValueTextField(textField: AddressTextField,      value: profile.diaChiHienTai, enabled: true)
        setValueTextField(textField: AddressCMNDTextField,  value: profile.diaChiCmnd)
        setValueTextField(textField: genderTextField,       value: profile.gioiTinh)
        setValueTextField(textField: brithdayTextField, value:profile.ngaySinh == "Chưa có" ? "" :UIHelper.convertStringToDate(date: profile.ngaySinh), enabled: profile.ngaySinh == "Chưa có" ? true : false)
        
    }

}


extension InfomationMemberViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
            
        case brithdayTextField:
            brithdayTextField.text = brithdayText
            
        case genderTextField:
            genderTextField.text = genderText
            
        default:
            break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case brithdayTextField:
            if brithdayTextField.text!.count > 0 {
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = DateFormatter.Style.medium
                dateFormatter.timeStyle = DateFormatter.Style.none
                dateFormatter.dateFormat = "dd/MM/yyyy"
                (brithdayTextField.inputView as! UIDatePicker).setDate(dateFormatter.date(from: brithdayTextField.text!) ?? Date(), animated: true)
            }
        case genderTextField:
            if(genderTextField.text!.count > 0) {
                let row = pickOption.firstIndex(of: genderTextField.text!)!
                if row >= 0 {
                    (genderTextField.inputView as! UIPickerView).selectRow(row, inComponent: 0, animated: true)
                }
                
            }
            break
        default:
            break
        }
        
        if brithdayText.count > 0 {
            brithdayTextField.text = brithdayText
        }
        
        if genderText.count > 0 {
            brithdayTextField.text = brithdayText
        }
    }
    
}


extension InfomationMemberViewController:UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderText = pickOption[row]
    }
}
