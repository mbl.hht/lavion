//
//  AccountBankViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/27/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift
class AccountBankViewController: UIViewController {
    @IBOutlet weak var nameAccountTextField: UITextField!
    @IBOutlet weak var numberAccountTextField: UITextField!
    @IBOutlet weak var nameBankTextField: UITextField!
    @IBOutlet weak var addressBankTextField: UITextField!
    @IBOutlet weak var UpdateButton: UIButton!
    
    var profile: ProfileModel?
    
    var pickOption = [String]()
        //["Vietcombank - NH TMCP NT VN","Techcombank - NH TMCP KT VN","VIBank - NH TMCP QT","Agribank - NH NNV PTNT VN","BIDV - NH TMCP ĐTVPT VN","VPbank - NH TMCP VN TV","Vietinbank - NH TMCP CT VN","Sacombank - NH TMCP SG TT","ACB - NgH TMCP AC VN","MB Bank - NH TMCP QĐ"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationController
        initNavigation(text: "Tài Khoản Ngân Hàng")
        
        //Name Bank TextField
        let pickerView = UIPickerView()
        pickerView.delegate = self
        nameBankTextField.inputView = pickerView
        nameBankTextField.clearButtonMode = .whileEditing
        
        
        //Number Account TextField
        numberAccountTextField.keyboardType = .numberPad
        numberAccountTextField.clearButtonMode = .whileEditing
        
        //Name Account TextField
        nameAccountTextField.keyboardType = .asciiCapable
        nameAccountTextField.clearButtonMode = .whileEditing
        
        addressBankTextField.clearButtonMode = .whileEditing
        
        //Update Button
        UpdateButton.layer.cornerRadius = 5
        UpdateButton.layer.borderWidth = 1
        UpdateButton.layer.borderColor = UIColor.clear.cgColor
        
        //init Data
        profile = (try! Realm()).objects(ProfileModel.self).first
        
        let bankList = (try! Realm()).objects(BankModel.self)
        for item in bankList {
            pickOption.append(item.tenNganHang)
        }
        
        loadData()

    }
    
    func setValueTextField(textField: UITextField, value: String?, enabled: Bool = false) {
        if (value?.count ?? 0) > 0 {
            textField.text = value
            textField.isEnabled = enabled
        }
    }
    
    func loadData() {
        setValueTextField(textField: nameAccountTextField, value: profile?.tenChuTaiKhoan)
        setValueTextField(textField: numberAccountTextField, value: profile?.soTaiKhoan)
        setValueTextField(textField: addressBankTextField, value: profile?.ten_chi_nhanh)
        setValueTextField(textField: nameBankTextField, value: profile?.idNganHang != 0  ? pickOption[(profile?.idNganHang)! - 1]: "")
        
        if((profile?.soTaiKhoan.count)! > 0 && (profile?.ten_chi_nhanh.count)! > 0 && (profile?.id)! > 0) {
            UpdateButton.isEnabled = false
            UpdateButton.backgroundColor = .gray
        }
    }
    
    @IBAction func updateAction(_ sender: UIButton) {
        var messageError = ""
        if numberAccountTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Số tài khoản không được để trống"
        } else if nameBankTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Tên ngân hàng không được bỏ trống"
        } else if addressBankTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Địa chỉ ngân hàng không được bỏ trống"
        }
        
        if messageError.count > 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Thông Báo", errorString: messageError, complete: nil)
            return
        }
        
        UIHelper.showHUD()
        SyncHttpConnect.changeInformationBank(numberAccount: numberAccountTextField.text!, nameAccount: nameAccountTextField.text!, nameBank: addressBankTextField.text!, idNganHang: (pickOption.firstIndex(of: nameBankTextField.text!) ?? -2) + 1) { (data: NSDictionary?, success: Bool) in
            UIHelper.dismisHUD()
            if success {
                if data?.value(forKey: "status") as! Int == 200 {
                    
                    
                    autoreleasepool {
                        let realm = try! Realm()
                        try! realm.write {
                            self.profile?.soTaiKhoan       = self.numberAccountTextField.text!
                            self.profile?.ten_chi_nhanh    = self.addressBankTextField.text!
                            self.profile?.idNganHang       = (self.pickOption.firstIndex(of: self.nameBankTextField.text!) ?? -2) + 1
                        }
                    }
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: "Cập nhật thành công.", complete: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                    
                    
                } else if data?.value(forKey: "status") as! Int == 400 {
                    messageError = ""
                    let errorDic = (data?.value(forKey: "errors") as! Dictionary<String, Array<String>>)
                    errorDic.values.forEach({ (value: Array<String>) in
                        messageError += (value.first ?? "") + "\n"
                    })
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: messageError, complete: nil)
                    
                }
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
            
        }

    }
    
}


extension AccountBankViewController:UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        nameBankTextField.text = pickOption[row]
    }
}
