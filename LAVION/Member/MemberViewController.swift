//
//  MemberViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/25/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RKNotificationHub
import AlamofireImage
import RealmSwift

class MemberViewController: BaseViewController {

    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var changePassBtton: UIButton!
    
    @IBOutlet weak var navigationView: UIView!
    
    var hub:RKNotificationHub!
    var profile:ProfileModel!
    var nameBank:String!
    let realm = try! Realm()
    var notificationToken: NotificationToken? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup Table view
        navigationView.backgroundColor = UIColor.mainColor

        //DispatchQueue.main.async {
            self.mainTableView.delegate = self
            self.mainTableView.dataSource = self
            self.mainTableView.register(UINib.init(nibName: "InfoMemberCell", bundle: nil), forCellReuseIdentifier: "infoMemberCell")
            self.mainTableView.register(UINib.init(nibName: "InforMemberTableViewHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "InforMemberTableViewHeaderFooterView")
            self.mainTableView.register(UINib.init(nibName: "TypeAccountHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "TypeAccountHeaderFooterView")
            self.mainTableView.register(UINib.init(nibName: "TitleHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "TitleHeaderFooterView")
            self.mainTableView.register(UINib.init(nibName: "SpaceFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "SpaceFooterView")
        //}

        
        
        //Settup ChangePasswordButton
        changePassBtton.addTarget(self, action: #selector(self.changePasswordAction(sennder:)), for: .touchUpInside)
        hub = RKNotificationHub.init(view: notificationImageView)
        hub.moveCircleBy(x: 0, y: 3)
        hub.scaleCircleSize(by: 0.5)
        profile  = realm.objects(ProfileModel.self).first
        nameBank = realm.objects(BankModel.self).filter("id = \(profile.idNganHang)").first?.tenNganHang
        
        notificationToken = profile.observe({ (ObjectChange) in
            self.mainTableView.reloadData()
        })
        
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }
    
    @objc func changePasswordAction(sennder: UIButton) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "changePasswordViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    
    @IBAction func notificationAction(_ sender: Any) {
        hub.increment()
        hub.pop()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }

}

extension MemberViewController:UITableViewDelegate,UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var row:Int = 0
        switch section {
        case 0:
            row = 0
        case 1:
            row = 0
        case 2:
            row = 0
        case 3 :
            row = 3
        case 4 :
            row = 4
        case 5 :
            row = 0
        case 6 :
            row = 6
        case 7 :
            row = 4
        default:
            row = 0
        }
        return row
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:InfoMemberCell = tableView.dequeueReusableCell(withIdentifier: "infoMemberCell") as! InfoMemberCell
        
        switch indexPath.section {
            
        case 0: break
         
        case 1: break
           
        case 2: break
           
        case 3 : // Người giới thiệu Bạn
            do {
                switch indexPath.row {
                case 0:
                    cell.intCell(textField: "Tên hiệu", textValue: profile.tenNguoiGioiThieu, isOpen: false)
                case 1:
                    cell.intCell(textField: "Người đại diện", textValue: "", isOpen: false)
                case 2:
                    cell.intCell(textField: "Số điện thoại", textValue: profile.soDienThoaiGioiThieu, isOpen: false)
                default: break
                }
            }
            
        case 4 : //Hệ thống của tôi
            do {
                switch indexPath.row {
                case 0:
                    cell.intCell(textField: "Tổng thành viên", textValue: "\(profile.soThanhVienDaGioiThieu)", isOpen: true)
                case 1:
                    cell.intCell(textField: "Tổng cộng tác viên", textValue: "0", isOpen: true)
                case 2:
                    cell.intCell(textField: "Tổng đơn hàng mua", textValue: "0", isOpen: true)
                case 3:
                    cell.intCell(textField: "Tổng đơn hàng bán", textValue: "0", isOpen: true)
                default: break
                }
            }
        case 5 : break
            
        case 6 : // Thông tin cá nhân
            do {
                switch indexPath.row {
                case 0:
                    cell.intCell(textField: "Họ và tên", textValue: profile.hoVaTen, isOpen: false)
                case 1:
                    cell.intCell(textField: "Số điện thoại", textValue: profile.soDienThoai, isOpen: false)
                case 2:
                    cell.intCell(textField: "Email", textValue: profile.email, isOpen: false)
                case 3:
                    cell.intCell(textField: "Ngày sinh", textValue: profile.ngaySinh != "Chưa có" ? UIHelper.convertStringToDate(date: profile.ngaySinh) : "" , isOpen: false)
                case 4:
                    cell.intCell(textField: "Giới tính", textValue: profile.gioiTinh, isOpen: false)
                case 5:
                    cell.intCell(textField: "Địa chỉ", textValue: profile.diaChiHienTai, isOpen: false)
                default: break
                }
            }
            
        case 7 : // Tài khoản ngân hàng
            do {
                switch indexPath.row {
                case 0:
                    cell.intCell(textField: "Chủ tài khoản", textValue: profile.tenChuTaiKhoan, isOpen: false)
                case 1:
                    cell.intCell(textField: "Số tài khoản", textValue: profile.soTaiKhoan, isOpen: false)
                case 2:
                    cell.intCell(textField: "Tên ngân hàng", textValue: nameBank, isOpen: false)
                case 3:
                    cell.intCell(textField: "Chi nhánh", textValue: profile.ten_chi_nhanh, isOpen: false)
                default: break
                }
            }
            
        default: break
            
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var cell:Any!
        switch section {
        case 0:
            do {
                let cellInfo = tableView.dequeueReusableHeaderFooterView(withIdentifier: "InforMemberTableViewHeaderFooterView") as! InforMemberTableViewHeaderFooterView
//                let UrlImage:String? = "Nam"
//                switch UrlImage {
//                case "Nam":
//                    cellInfo.avatarImageView.image = UIImage(named: "male_img.png")
//                case "Nu":
//                    cellInfo.avatarImageView.image = UIImage(named: "female.png")
//                default :
//                    cellInfo.avatarImageView.image = UIImage(named: UrlImage!)
//                    break
//                }
                cellInfo.avatarImageView.af_setImage(withURL:URL.init(string:address + profile.avatar)!)
                cellInfo.nameLabel.text         = profile.hoVaTen
                cellInfo.phoneNumberLabel.text  = profile.soDienThoai
                cellInfo.avatarButton.isEnabled = false
                cell = cellInfo
            }
        case 1:
            do {
                let cellType = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TypeAccountHeaderFooterView") as! TypeAccountHeaderFooterView
                var currentType:Int
                switch (profile.isThanhVien) {
                case "DAI LY ONLINE" :
                    currentType = 0
                case "THANH VIEN" :
                    currentType = 1
                default:
                    currentType = 2
                }
                cellType.intHeader(typeAccount: currentType)
                cell = cellType
            }
        case 2:
            do {
                let cellType = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TitleHeaderFooterView") as! TitleHeaderFooterView
                cellType.typeHeader = .MaGiamGia
                cellType.initHeader(urlImage: "share_img.png", setTitleLabel: cellType.typeHeader.rawValue, isBold: false, isShowPoint: true, setPointLabel: "\(profile.magiamgia)",isShowButton: true, setTextButton: "Chia Sẻ")
                cellType.typeHeader = .MaGiamGia
                cell = cellType
            }
        case 3:
            do {
                let cellType = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TitleHeaderFooterView") as! TitleHeaderFooterView
                cellType.typeHeader = .NguoiGioiThieu
                cellType.initHeader(urlImage: "reseller_img.png", setTitleLabel: cellType.typeHeader.rawValue, isBold: true, isShowPoint: false, setPointLabel: "",isShowButton: false, setTextButton: "Cập nhật")
                cell = cellType
            }
        case 4:
            do {
                let cellType = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TitleHeaderFooterView") as! TitleHeaderFooterView
                cellType.typeHeader = .HeThongCuaToi
                cellType.initHeader(urlImage: "usergroups_img", setTitleLabel: cellType.typeHeader.rawValue, isBold: true, isShowPoint: false, setPointLabel: "",isShowButton: false, setTextButton: "")
                cell = cellType
            }
        case 5:
            do {
                let cellType = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TitleHeaderFooterView") as! TitleHeaderFooterView
                cellType.typeHeader = .CapNhatCMND
                cellType.initHeader(urlImage: "card_img.png", setTitleLabel: cellType.typeHeader.rawValue, isBold: true, isShowPoint: false, setPointLabel: "",isShowButton: true, setTextButton: "Cập nhật")
                cell = cellType
            }
        case 6:
            do {
                let cellType = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TitleHeaderFooterView") as! TitleHeaderFooterView
                cellType.typeHeader = .ThongTinCaNhan
                cellType.initHeader(urlImage: "card_img.png", setTitleLabel: cellType.typeHeader.rawValue, isBold: true, isShowPoint: false, setPointLabel: "",isShowButton: true, setTextButton: "Cập nhật")
                cell = cellType
            }
        case 7:
            do {
                
                let cellType = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TitleHeaderFooterView") as! TitleHeaderFooterView
                cellType.typeHeader = .TaiKhoanNganHang
                    cellType.initHeader(urlImage: "card_img.png", setTitleLabel: cellType.typeHeader.rawValue, isBold: true, isShowPoint: false, setPointLabel: "",isShowButton: true, setTextButton: "Cập nhập")
                cell = cellType
            }
        default:
            print("22222")
        }
        return cell as? UIView
        
      
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height:CGFloat = 0
        switch section {
        case 0:
            height = 60
        case 1:
            height = 40
        case 2:
            height = 40
        case 3 :
            height = 40
        case 4 :
            height = 40
        case 5 :
            height = 40
        case 6 :
            height = 40
        case 7 :
            height = 40
        default:
            height = 40
        }
        return height
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SpaceFooterView") as! SpaceFooterView
        if section == 0 {
            cell.View.backgroundColor = UIColor.orange
            cell.heightViewConstraint.constant = 2
            cell.logoutButton.isHidden = true
        } else if (section == 7){
            cell.heightViewConstraint.constant = 130
            cell.logoutButton.isHidden = false
            cell.View.backgroundColor = UIColor.white
        } else {
            cell.View.backgroundColor = .groupTableViewBackground
            cell.heightViewConstraint.constant = 3
            cell.logoutButton.isHidden = true
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var height:CGFloat = 0
        switch section {
        case 0:
            height = 2
        case 1...6:
            height = 3
        case 7:
            height = 130
        default:
            break
        }
        return height
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 4:
            do {
                switch indexPath.row {
                case 0:
                    do {
                        let sb = UIStoryboard.init(name: "Main", bundle: nil)
                        let vc:MemberOfMeViewController = sb.instantiateViewController(withIdentifier: "memberOfMeViewController") as! MemberOfMeViewController
                        vc.titleNavigation = "Tổng thành viên"
                        vc.postionView = "Thành viên"
                        vc.postionText = "thành viên"
                        vc.urlText = "medal_thanhvien.png"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                case 1:
                    do {
                        let sb = UIStoryboard.init(name: "Main", bundle: nil)
                        let vc:MemberOfMeViewController = sb.instantiateViewController(withIdentifier: "memberOfMeViewController") as! MemberOfMeViewController
                        vc.titleNavigation = "Tổng cộng tác viên"
                        vc.postionView = "Cộng tác viên"
                        vc.postionText = "cộng tác viên"
                        vc.urlText = "medal.png"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                case 2:
                    do {
                        print("Open tổng đơn hàng mua")
                    }
                case 3:
                    do {
                        print("Open tổng đơn hàng bán")
                    }
                
                default :
                    break
                }
            }
            
        default:
            break
        }
    }
    
    
}

