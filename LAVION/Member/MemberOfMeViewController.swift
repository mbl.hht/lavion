//
//  MemberOfMeViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/30/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit


struct MemberDetail {
    var name: String
    var email: String
    var dateRegister: String
    var phoneNumber: String
    var postion: String
}

class MemberOfMeViewController: UIViewController {
    
    @IBOutlet weak var totalMemberLabel: UILabel!
    @IBOutlet weak var mainTableView: UITableView!
    var titleNavigation: String?
    var postionView: String?
    var postionText: String?
    var urlText: String?
    
    var memberOfMeArray = [MemberDetail]()
    override func viewDidLoad() {
        super.viewDidLoad()
 
        //setupTable
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib.init(nibName: "DetailMemberOfMeCell", bundle: nil), forCellReuseIdentifier: "detailMemberOfMeCell")
        mainTableView.register(UINib.init(nibName: "DetailMemberOfMeHeaderCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "DetailMemberOfMeHeaderCell")
        
        
        //add Data
        memberOfMeArray.append(MemberDetail(name: "Mai Bao Loc", email: "mbl.hht@gmail.com", dateRegister: "12/12/2017", phoneNumber: "01264535373",postion: postionView!))
        memberOfMeArray.append(MemberDetail(name: "Mai Bao Loc 1", email: "mbl.hht@gmail.com", dateRegister: "12/12/2019", phoneNumber: "01264535373",postion: postionView!))
        memberOfMeArray.append(MemberDetail(name: "Mai Bao Loc 2", email: "mbl.hht@gmail.com", dateRegister: "12/12/2021", phoneNumber: "01264535373",postion: postionView!))
        
        
        self.totalMemberLabel.text = "Danh sách có "+String(memberOfMeArray.count) + " " + postionText!
        
        //navigationController
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.topItem?.title = titleNavigation
        self.navigationController?.navigationBar.barTintColor = UIColor(Enum.TypeColor.mainColor.rawValue)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 20)!]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MemberOfMeViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DetailMemberOfMeCell = tableView.dequeueReusableCell(withIdentifier: "detailMemberOfMeCell") as! DetailMemberOfMeCell
        cell.setValueCell(urlImage: urlText!, name: memberOfMeArray[indexPath.row].name, level: memberOfMeArray[indexPath.row].postion, phoneNumber: memberOfMeArray[indexPath.row].phoneNumber, dateRegister: memberOfMeArray[indexPath.row].dateRegister)
        return cell
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return memberOfMeArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 61
    }
    
    
    
}
