//
//  ProductListViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/22/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class ProductListViewController: UIViewController {
    @IBOutlet weak var mainCollectionView: UICollectionView!
    var listProductArray = (try! Realm()).objects(ProductModel.self)
    let searchController = UISearchController(searchResultsController: nil)
    var filteredProductArray = [ProductModel]()
    var isOpenSearch = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavigation(text: "Danh sách sản phẩm",hidden: true)

        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        
        
        mainCollectionView.register(UINib.init(nibName: "ProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "productCollectionCell")
        if let flowLayout = mainCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let inset:CGFloat = 0
            let numberOfColumns:CGFloat = 2
            let spacing:CGFloat = 0.01//1.5 * 0.5
            flowLayout.minimumInteritemSpacing = spacing
            flowLayout.minimumLineSpacing = spacing
            flowLayout.sectionInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            //set scroll to Vertical
            flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.width - numberOfColumns * 2 * spacing - inset * numberOfColumns) / numberOfColumns, height: 300)
            flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        }
        
        
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        if #available(iOS 9.1, *) {
            searchController.obscuresBackgroundDuringPresentation = false
        }
        definesPresentationContext = true
        searchController.searchBar.placeholder = "Tìm kiếm sản phẩm"
        let scb = searchController.searchBar
        scb.tintColor = UIColor.white
        scb.barTintColor = UIColor.white
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                backgroundview.backgroundColor = UIColor.white
                backgroundview.layer.cornerRadius = 10;
            }
        }
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Hủy"
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        }
        
        
        //navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Tạo câu hỏi", style: .plain, target: self, action: #selector(addQAAction))
        // Setup the Scope Bar
        searchController.searchBar.scopeButtonTitles = ["Tất cả", "Tên", "Mã sản phẩm"]
        searchController.searchBar.delegate = self
        
    }
    
    
    
    func filterContentForSearchText(searchText: String, scope: String = "Tất cả") {
        var tam: Int = 0
        filteredProductArray = listProductArray.filter({ (product: ProductModel) -> Bool in
            tam += 1
            let doesCategoryMatch = (scope == "Tất cả") || (scope == "Mã sản phẩm") || (scope == "Tên")
            if searchBarIsEmpty() {
                return doesCategoryMatch
            } else {
                if scope == "Mã sản phẩm" {
                    return doesCategoryMatch && product.san_pham_ma.lowercased().contains(searchText.lowercased())
                } else if scope == "Tên" {
                    return doesCategoryMatch && product.san_pham_ten.lowercased().contains(searchText.lowercased())
                }
                
                return doesCategoryMatch && (doesCategoryMatch && product.san_pham_ma.lowercased().contains(searchText.lowercased()) || product.san_pham_ten.lowercased().contains(searchText.lowercased()) )
            }
        })
        mainCollectionView.reloadData()
        
    }
    
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !searchController.isActive && isOpenSearch {
              searchController.isActive = true
            isOpenSearch = false
        }
      
    }

}

extension ProductListViewController:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "detailProductViewController") as! DetailProductViewController
        if isFiltering() {
            vc.detailProduct = filteredProductArray[indexPath.row]
        } else {
            vc.detailProduct = listProductArray[indexPath.row]
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ProductListViewController:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredProductArray.count
        }
        return listProductArray.count
   
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCollectionCell", for: indexPath) as! ProductCollectionCell
        if indexPath.row % 2 == 0 {
            cell.leftContraint.constant = 15
            cell.rightContraint.constant = 7.5
        } else {
            cell.leftContraint.constant = 7.5
            cell.rightContraint.constant = 15
        }
        
        if indexPath.row == 0 || indexPath.row == 1 {
            cell.topConstraint.constant = 15
        } else {
            cell.topConstraint.constant = 7.5
        }
        
        let product: ProductModel
        
        if isFiltering() {
            product = filteredProductArray[indexPath.row]
        } else {
            product = listProductArray[indexPath.row]
        }
        
        cell.item = product
        return cell
        
    }

    
    
}


extension ProductListViewController:UISearchControllerDelegate {
    
}

extension ProductListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let  scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        print("LBMJoker %@",searchBar.text ?? "")
        filterContentForSearchText(searchText: searchBar.text!, scope: scope)
        
        
    }
}


extension ProductListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        let  scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        print("LBMJoker %@",searchBar.text ?? "")
        filterContentForSearchText(searchText: searchBar.text!, scope: scope)
        
    }
    
}


