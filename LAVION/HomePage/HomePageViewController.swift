//
//  HomePageViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/14/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class HomePageViewController: BaseViewController {

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var MilkView: UIView!
    @IBOutlet weak var VNDView: UIView!
    @IBOutlet weak var donHangView: UIView!
    @IBOutlet weak var vNDKiemDuocView: UIView!
    @IBOutlet weak var mILKKiemDuocView: UIView!
    
    @IBOutlet weak var gioiThieuView: UIView!
    
    @IBOutlet weak var vNDLabel: UILabel!
    @IBOutlet weak var mILKLabel: UILabel!
    @IBOutlet weak var donHangLabel: UILabel!
    @IBOutlet weak var vNDkiemDuocLabel: UILabel!
    
    @IBOutlet weak var mILKKiemDuocLabel: UILabel!
    @IBOutlet weak var gioiThieuLabel: UILabel!
    
    
    
    
    @IBOutlet weak var pageControlView: FSPageControl! {
        didSet {
            pageControlView.numberOfPages = self.imageNames.count
            pageControlView.contentHorizontalAlignment = .center
            pageControlView.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            pageControlView.hidesForSinglePage = true
        }
    }
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.automaticSlidingInterval = 3.0 - self.pagerView.automaticSlidingInterval//Au to
        }
    }
    @IBOutlet weak var mainTableView: UITableView!
    
    fileprivate let imageNames = ["1.jpg","2.jpg","3.jpg"]
    let titleHeaderArray:Array = ["Sản phẩm HOT","Tìm kiếm phổ biến","Danh mục ngành hàng","Danh mục sản phẩm"]
    
    let statitic = (try! Realm()).objects(StatisticModel.self).first
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        pagerView.dataSource = self
//        pagerView.delegate = self
//        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
//        pagerView.isHidden = false
        pageControlView.isHidden = true
    
        
        // init Seach TextField
        searchTextField.leftViewMode = UITextField.ViewMode.always
        searchTextField.leftViewMode = .always
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 20 ))
        let leftImage = UIImageView(image: UIImage(named: "Search_img"))
        leftImage.frame = CGRect(x: 5, y: 0, width: 20, height: 20)
        leftView.addSubview(leftImage)
        searchTextField.leftView = leftView
        searchTextField.leftView?.contentMode = UIView.ContentMode.scaleAspectFit
        searchTextField.placeholder = "Tìm kiếm"
        searchTextField.delegate = self

        //Header view
        MilkView.initGradient(colorStart:"#a8e063", colorEnd:"#56ab2f")
        VNDView.initGradient(colorStart: "#00c6ff", colorEnd: "#0072ff")
        donHangView.initGradient(colorStart: "#f85032", colorEnd: "#e73827")
        mILKKiemDuocView.initGradient(colorStart: "#1e3c72", colorEnd: "#2a5298")
        vNDKiemDuocView.initGradient(colorStart: "#e52d27", colorEnd: "#b31217")
        gioiThieuView.initGradient(colorStart: "#ADD100", colorEnd: "#7B920A")
        
        MilkView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: MilkView.layer.cornerRadius, opacity: 0.35)
        VNDView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: MilkView.layer.cornerRadius, opacity: 0.35)
        gioiThieuView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: MilkView.layer.cornerRadius, opacity: 0.35)
        vNDKiemDuocView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: MilkView.layer.cornerRadius, opacity: 0.35)
        mILKKiemDuocView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: MilkView.layer.cornerRadius, opacity: 0.35)
        donHangView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: MilkView.layer.cornerRadius, opacity: 0.35)

        
        
        // mainTableView
        mainTableView.dataSource = self
        mainTableView.delegate = self

        mainTableView.register(UINib.init(nibName: "HomeTitleHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HomeTitleHeaderView")
        mainTableView.register(UINib.init(nibName: "HotProduceCell", bundle: nil), forCellReuseIdentifier: "hotProduceCell")
        mainTableView.register(UINib.init(nibName: "PopularSearchTableCell", bundle: nil), forCellReuseIdentifier: "popularSearchTableCell")
        mainTableView.register(UINib.init(nibName: "ProductTableCell", bundle: nil), forCellReuseIdentifier: "productTableCell")
        mainTableView.register(UINib.init(nibName: "CatelogyProduceTableCell", bundle: nil), forCellReuseIdentifier: "catelogyProduceTableCell")
        mainTableView.tableFooterView = UIView()
        mainTableView.tableHeaderView = headerView
       
        //init data
        vNDLabel.text =  "\(statitic?.tong_tien_vnd.formatNumber() ?? "0") VND"
        mILKLabel.text = "\(statitic?.the_milk.formatNumber() ?? "0") MILK"
        gioiThieuLabel.text = "\(statitic?.soCongTacVienGioiThieuDaKichHoat ?? 0) / \(statitic?.soCongTacVienGioiThieu ?? 0)"
        vNDkiemDuocLabel.text =  "\(statitic?.tongTienVNDKiemDuoc.formatNumber() ?? "0") VND"
        mILKKiemDuocLabel.text = "\(statitic?.tongMilkKiemDuoc.formatNumber() ?? "0") MILK"
        donHangLabel.text = "\(statitic?.soDonHangDaBanThanhCong ?? 0) / \(statitic?.soDonHangDaBan ?? 0)"

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    

}


extension HomePageViewController:FSPagerViewDelegate {
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        pageControlView.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        pageControlView.currentPage = pagerView.currentIndex
    }
    
}

extension HomePageViewController:FSPagerViewDataSource {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return imageNames.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.image = UIImage(named: self.imageNames[index])
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        //cell.textLabel?.text = index.description+index.description
        return cell
    }
    
    
}

extension HomePageViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        switch section {
        case 0:
            number = 1
        case 1:
            number = 1
        case 2:
            number = 1
        case 3:
            number = 1
        default:
            number = 0
        }
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell!
        switch indexPath.section {
        case 0:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "hotProduceCell",for:indexPath) as! HotProduceCell
            cell = cell1
        case 1:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "popularSearchTableCell",for:indexPath) as! PopularSearchTableCell
            
            cell = cell1
        case 2:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "catelogyProduceTableCell",for:indexPath) as! CatelogyProduceTableCell
            cell = cell1
        case 3:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "productTableCell",for:indexPath) as! ProductTableCell
            cell = cell1
    
            break
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "")
            break
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return titleHeaderArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HomeTitleHeaderView") as! HomeTitleHeaderView
        cell.titleLabel.text = titleHeaderArray[section]
        switch section {
        case 0:
            cell.typeHeaderHome = .SanPhamHot
            cell.seeMoreButton.isHidden = true
            cell.isHidden = false
        case 1:
            cell.typeHeaderHome = .TimkiemPhoBien
            cell.seeMoreButton.isHidden = true
            cell.isHidden = true
            
        case 2:
            cell.typeHeaderHome = .DanhMucNganhHang
            cell.seeMoreButton.isHidden = false
            cell.isHidden = true
        case 3:
            cell.typeHeaderHome = .DanhSachSanPham
            cell.seeMoreButton.isHidden = false
            cell.isHidden = false
        default:
            cell.typeHeaderHome = .None
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 || section == 2 {
            return 0.0001
        }
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 200
        case 1:
            return 0.001
        case 2:
            return 0.001//200
        case 3:
            return 300*4
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }

}

extension HomePageViewController: UITableViewDelegate {
    
}

extension HomePageViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "productListViewController") as! ProductListViewController
        vc.isOpenSearch = true
        self.navigationController?.pushViewController(vc, animated: true)
        return false
    }
}

