//
//  CatelogyProductViewController.swift
//  LAVION
//
//  Created by iOS on 11/25/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import AlamofireImage

class CatelogyProductViewController: UIViewController {
    @IBOutlet weak var mainCollectionView: UICollectionView!
    var listProductArray = [CatelogyProduceModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationController
        initNavigation(text: "Danh Mục Sản Phẩm")
        
        
        
        
        //setup Collection View
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        mainCollectionView.register(UINib.init(nibName: "CatelogyProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "catelogyProductCollectionCell")
        if let flowLayout = mainCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            
            let inset:CGFloat = 0
            let numberOfColumns:CGFloat = 3
            let spacing:CGFloat = 1.5 * 0.5
            flowLayout.minimumInteritemSpacing = spacing
            flowLayout.minimumLineSpacing = spacing
            flowLayout.sectionInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            //set scroll to Vertical
            flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.width - numberOfColumns * 2 * spacing - inset * numberOfColumns) / numberOfColumns, height: 300)
            flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        }

        //init Data
        listProductArray.append(CatelogyProduceModel(image: "canci.png", title: "Thực phẩm bổ sung cho mọi độ tuổi"))
        listProductArray.append(CatelogyProduceModel(image: "hop_vani2", title: "Thực phẩm chức năng dành cho người lớn"))
        listProductArray.append(CatelogyProduceModel(image: "canci.png", title: "Mỹ phẩm làm đẹp cho phái đẹp"))
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension CatelogyProductViewController:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listProductArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "catelogyProductCollectionCell", for: indexPath) as! CatelogyProductCollectionCell
        cell.item = listProductArray[indexPath.row]
        return cell
        
    }
    
    
}

extension CatelogyProductViewController:UICollectionViewDelegate {
    
}
