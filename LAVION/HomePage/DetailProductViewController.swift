//
//  DetailProductViewController.swift
//  LAVION
//
//  Created by iOS on 11/26/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import EFMarkdown
import WebKit
import RealmSwift

class DetailProductViewController: UIViewController {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var VNDlabel: UILabel!
    @IBOutlet weak var MILKLabel: UILabel!
    @IBOutlet weak var nameProductLabel: UILabel!
    @IBOutlet weak var addProductButton: UIButton!
    
    var realm = try! Realm()
    
    var detailProduct:ProductModel?
    
    var infoString = "" //= "Sữa viên ăn liền Mixmilk\nlà thực phẩmBổ sung Betaglucan và các dưỡng chất từ sữa giúp nâng cao sức khỏe,\ntăng cường sức đề kháng của cơ thể, hỗ trợ làm giảm tình trạng mệt mỏi, hay ốm\nvặt do sức đề kháng yếu. Bổ sung canxi và vitamin D3 giúp phát triển răng xương\nở trẻ, giúp giảm nguy cơ loãng xương ở người cao tuổi"
//
//    var content1 = ""//= "<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:xx24px\"><span style=\"color:#000000\"><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"background-color:white\">Sữa vi&ecirc;n ăn liền Mixmilk l&agrave; thực phẩm bổ sung Betaglucan v&agrave; c&aacute;c dưỡng chất từ sữa gi&uacute;p n&acirc;ng cao sức khỏe, tăng cường sức đề kh&aacute;ng của cơ thể, hỗ trợ l&agrave;m giảm t&igrave;nh trạng mệt mỏi, hay ốm vặt do sức đề kh&aacute;ng yếu. Bổ sung canxi v&agrave; vitamin D3 gi&uacute;p ph&aacute;t triển răng xương ở trẻ, gi&uacute;p giảm nguy cơ lo&atilde;ng xương ở người cao tuổi</span></span></span></span></p>\n\n<p style=\"text-align:justify\">&nbsp;</p>\n\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:xx16xpx\"><span style=\"font-family:Arial,Helvetica,sans-serif\">I.&nbsp;Th&ocirc;ng tin về sản phẩm<br />\n1.&nbsp;&nbsp; &nbsp;T&ecirc;n sản phẩm đầy đủ: Thực phẩm bổ sung MIXMILK B<br />\n2.&nbsp;&nbsp; &nbsp;Th&agrave;nh phần:&nbsp;<br />\nMỗi vi&ecirc;n 1500 mg chứa:<br />\nSữa bột nguy&ecirc;n kem&nbsp; 67% (1000 mg)<br />\nVitalarmor Ca D&nbsp;(bao gồm hỗn hợp chất kho&aacute;ng từ sữa, đạm, lactose)&nbsp;&nbsp; &nbsp;1,3 % (20 mg)<br />\nVitamin D3&nbsp;&nbsp; &nbsp;20 UI<br />\nBetaglucan&nbsp;&nbsp; &nbsp;0,3% (5 mg)<br />\nColostrum (sữa non)&nbsp;&nbsp; &nbsp;0,3% (5 mg)<br />\nPhụ liệu: Glucose, sorbitol, hương vani vừa đủ 1 vi&ecirc;n</span></span></p>\n\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:xx16xpx\">3.&nbsp;Thời hạn sử dụng sản phẩm:&nbsp;<br />\n-&nbsp;&nbsp; &nbsp;24 th&aacute;ng kể từ ng&agrave;y sản xuất.&nbsp;<br />\n-&nbsp;&nbsp; &nbsp;Ng&agrave;y sản xuất v&agrave; hạn sử dụng ghi tr&ecirc;n bao b&igrave; của sản phẩm<br />\n4.&nbsp;Quy c&aacute;ch đ&oacute;ng g&oacute;i v&agrave; chất liệu bao b&igrave;:<br />\n-&nbsp;&nbsp; &nbsp;Chất liệu bao b&igrave;:&nbsp;<br />\nĐ&oacute;ng trong g&oacute;i gh&eacute;p Alu/giấy/PVC hoặc lọ thủy tinh, lọ nhựa PVC, đựng trong hộp giấy.<br />\nChất lượng bao b&igrave; đảm bảo an to&agrave;n vệ sinh thực phẩm theo quy định của Bộ Y tế v&agrave; ph&aacute;p luật Việt Nam, c&oacute; TCCS ri&ecirc;ng, kh&ocirc;ng g&acirc;y &ocirc; nhiễm cho sản phẩm<br />\n-&nbsp;&nbsp; &nbsp;Quy c&aacute;ch đ&oacute;ng g&oacute;i:&nbsp;G&oacute;i 10 vi&ecirc;n, 1 hộp 10 g&oacute;i</span></span><br />\n&nbsp;</p>"
//    var content2 = ""//= "<p>5. Phiếu kết quả thử nghiệm TPBVSK Mixmilk hương Vani đạt chuẩn do Tổng cục ti&ecirc;u chuẩn đo lường chất lượng cấp ng&agrave;y 3 th&aacute;ng 5 năm 2018.</p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"/ckfinder/userfiles/images/Đối tác/long vuong/ảnh long vuong 2/long vuong 2/Phieu-ket-qua-thu-nghiem_huong_vani.png\" style=\"height:967px; width:680px\" /></p>\n\n<p>&nbsp;</p>\n\n<p style=\"text-align:justify\">6. Chứng nhận C&ocirc;ng ty Cổ phần dược Vesta đủ điều kiện an to&agrave;n thực phẩm</p>\n\n<p style=\"text-align:center\"><img alt=\"\" src=\"/ckfinder/userfiles/images/Đối tác/long vuong/ảnh long vuong 2/long vuong 2/CN-đủ đk sx-Vesta.jpg\" style=\"height:962px; width:680px\" /></p>\n\n<p style=\"text-align:justify\">&nbsp;</p>"
//
//    var content3 = ""//= "<p style=\"margin-left:0in; margin-right:0in\"><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:xx16xpx\">7.&nbsp;&nbsp;T&ecirc;n v&agrave; địa chỉ cơ sở sản xuất sản phẩm:<br />\nT&ecirc;n c&ocirc;ng ty:&nbsp;&nbsp; &nbsp;C&Ocirc;NG TY CỔ PHẦN PH&Aacute;T TRIỂN DƯỢC VESTA<br />\nĐịa chỉ:&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Mỹ Giang &ndash; Tam Hiệp &ndash;Ph&uacute;c Thọ &minus; H&agrave; Nội<br />\nĐiện thoại:&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;02432979188</span></span></p>\n\n<p style=\"margin-left:0in; margin-right:0in\"><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:xx16xpx\">8.&nbsp; Ph&acirc;n phối v&agrave; chịu tr&aacute;ch nhiệm về chuất lượng sản phẩm</span></span></p>\n\n<p style=\"margin-left:0in; margin-right:0in\"><span style=\"font-family:Arial,Helvetica,sans-serif\"><span style=\"font-size:xx16xpx\">T&ecirc;n c&ocirc;ng ty:&nbsp;&nbsp; &nbsp;C&Ocirc;NG TY CỔ PHẦN ĐẦU TƯ PH&Aacute;T TRIỂN QUỐC TẾ MIXMILK<br />\nĐịa chỉ:&nbsp; &nbsp; &nbsp; &nbsp; Số 391 đường Nguyễn Xiển, Phường Kim Giang, Quận Thanh Xu&acirc;n, TP&nbsp;H&agrave; Nội<br />\nĐiện thoại:&nbsp; &nbsp; &nbsp;0983836969</span></span></p>"
//    var content4 = ""//"<p>Vận chuyển to&agrave;n quốc trong v&ograve;ng 2 -&nbsp;5 ng&agrave;y</p>"
//    var content5 = ""

    var heighContent1ProductTableCell: CGFloat = 0

    let contentWebView = EFMarkdownView()
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.tableFooterView = UIView()
        mainTableView.tableHeaderView = headerView
        //mainTableView.register(UINib.init(nibName: "InforProductTableCell", bundle: nil), forCellReuseIdentifier: "inforProductTableCell")
        //infoString = infoString.replacingOccurrences(of: "\n", with: " ")
        //heighInforProductTableCell = 10 + 20 + 10 + UIHelper.heightForView(text: infoString, width: mainTableView.frame.width - 40, font:  UIFont(name: "HelveticaNeue", size: 14)!) + 10
        
        // init WebView Content
        var content = "",content1 = "",content2 = "",content3 = "",content4 = "",content5 = ""
        
        content1 = detailProduct?.san_pham_noi_dung_tab_1 ?? ""
        content  += content1.replacingOccurrences(of: "font-size:", with: "")
        
        content2 = detailProduct?.san_pham_noi_dung_tab_2 ?? ""
        content2 = content2.replacingOccurrences(of: "font-size:", with: "")
        content  += content2.replacingOccurrences(of: "src=\"/ckfinder", with: "src=\"\(address)/ckfinder")
        
        content3 = detailProduct?.san_pham_noi_dung_tab_3 ?? ""
        content  += content3.replacingOccurrences(of: "font-size:", with: "")
        
        content4 = detailProduct?.san_pham_noi_dung_tab_4 ?? ""
        content  += content4.replacingOccurrences(of: "font-size:", with: "")
        
        content5 = detailProduct?.san_pham_noi_dung_tab_5 ?? ""
        content  += content5.replacingOccurrences(of: "font-size:", with: "")
        
        
        contentWebView.frame = CGRect(x: 0, y: 0, width: mainTableView.bounds.width, height: 200)
        contentWebView.isScrollEnabled = false
        contentWebView.onRendered = {
            [weak self] (height) in
            if let _ = self {
                self?.heighContent1ProductTableCell = height ?? 0.0
                self?.contentWebView.frame = CGRect(x: 0, y: 0, width: (self?.mainTableView.bounds.width)!, height: height!)
                let indexPath = IndexPath.init(row: 1, section: 0)
                self?.mainTableView.beginUpdates()
                self?.mainTableView.reloadRows(at: [indexPath], with: .automatic)
                self?.mainTableView.endUpdates()
                print("onRendered height: \(height ?? 0)")
            }
        }
        contentWebView.load(markdown: content, options: [.default]) {
            [weak self] (_, _) in
            if let _ = self {
                // Optional: you can change font-size with a value of percent here
                self!.contentWebView.setFontSize(percent: 120)
                printLog("load finish!")
            }
        }
        
        //init Button
        addProductButton.backgroundColor = UIColor.mainColor
        
        // settup Value
        nameProductLabel.text = detailProduct?.san_pham_ten
        VNDlabel.text = (detailProduct?.san_pham_gia_ban_thuc_te.formatNumber() ?? "0") + " VND"
        MILKLabel.text = ((detailProduct?.san_pham_gia_ban_thuc_te ?? 0) / 1000).formatNumber() + " MILK"
        let placeholderImage = UIImage(named: "lavion.jpg")
        if let url = URL(string: (detailProduct?.san_pham_hinh_dai_dien) ?? address) {
            productImageView.af_setImage(withURL: url, placeholderImage: placeholderImage)
        } else {
            productImageView.image = placeholderImage
        }

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initNavigation(text: "Chi tiết sản phẩm")
        self.tabBarController?.tabBar.isHidden = true
    }

    @IBAction func addProductAction(_ sender: Any) {
        var product:ProductInCartModel? = realm.objects(ProductInCartModel.self).filter("id = \(detailProduct?.id ?? -1)").first
        if (product != nil) {
            try! realm.write {
                product!.soLuong = product!.soLuong + 1
            }
        } else {
            product = ProductInCartModel()
            product!.id = detailProduct!.id
            product!.product = detailProduct!
            product!.soLuong = 1
            product?.date = Date()
            
            try! realm.write {
                realm.add(product!, update: true)
            }
        }
    }
    
    
}

extension DetailProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailProductContentTableCell")!
            contentWebView.isUserInteractionEnabled = false
            cell.contentView.addSubview(contentWebView)
            return cell
        
        default:
            return UITableViewCell()
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return heighContent1ProductTableCell
        default:
            return 0
        }
    }
    
    
}

