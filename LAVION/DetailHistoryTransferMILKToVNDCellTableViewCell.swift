//
//  DetailHistoryTransferMILKToVNDCellTableViewCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/11/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class DetailHistoryTransferMILKToVNDCellTableViewCell: UITableViewCell {
    @IBOutlet weak var codeGDlabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var numberMILKLabel: UILabel!
    
    @IBOutlet weak var numberVNDLabel: UILabel!
    
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    var item: HistoryMILKLoadModel? {
        didSet {
            codeGDlabel.text = "Mã GD : \(item?.id ?? 0)"
            numberVNDLabel.text = item?.trang_thai
            numberMILKLabel.text = "Số điểm : \(item?.so_diem.formatNumber() ?? "0")"
            feeLabel.text = item?.updated_at
            statusLabel.text = ""
            dateLabel.text = ""
        }
    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell(codeGD: String?,status: String?,numberMilk: String?,numberVND: String?,fee: String?,date: String?) {
        codeGDlabel.text = "Mã GD :" + (codeGD ?? "")
        statusLabel.text = status
        numberMILKLabel.text = "Số MILK : " + (numberMilk ?? "") + " MILK"
        numberVNDLabel.text = "Thành VND : " + (numberVND ?? "") + " VND"
        feeLabel.text = "Phí : " + (fee ?? "") + " MILK"
        dateLabel.text = date
        
    }
    
}
