//
//  PayCartHeaderView.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/17/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class PayCartHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var checkButton: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    
}
