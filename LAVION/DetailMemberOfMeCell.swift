//
//  DetailMemberOfMeCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/30/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class DetailMemberOfMeCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var dateRegisterLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValueCell(urlImage: String, name: String, level: String,phoneNumber: String, dateRegister: String) {
        iconImageView?.image    = UIImage(named: urlImage)
        nameLabel.text          = name
        phoneNumberLabel.text   = phoneNumber
        levelLabel.text         = level
        dateRegisterLabel.text  = dateRegister
        
    }
    
}
