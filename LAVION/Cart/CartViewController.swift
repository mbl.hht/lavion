//
//  CartViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/11/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var segmentView: UIView!
    var shoppingCart = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "shoppingCartViewController") as! ShoppingCartViewController
    var bought = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "boughtViewController") as! BoughtViewController
    var sold = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "soldViewController") as! SoldViewController
    lazy var subController:[UIViewController] = {
        return [shoppingCart,bought,sold]
    }()
    
    var pageViewController: UIPageViewController!
    var segmentedControl: TwicketSegmentedControl!
    
    override func viewDidLoad() {

        super.viewDidLoad()
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.delegate = self
        pageViewController.dataSource = self
        pageViewController.setViewControllers([subController[0]], direction: .forward, animated: true, completion: nil)
        addChild(pageViewController)
        view.addSubview(pageViewController.view)
        var hieghtTypeIphone:CGFloat = 0.0
        switch UIDevice.current.screenType {
        case .iPhone_XSMax,.iPhone_XR,.iPhones_X_XS:
            hieghtTypeIphone = 25
            break;
        default :
            hieghtTypeIphone = 0.0
        }
        let currentFrame = CGRect(x:  0.0, y: (self.titleView.frame.origin.y + self.titleView.bounds.height + hieghtTypeIphone ), width: self.view.bounds.width, height: self.view.bounds.height - 40 - (self.tabBarController?.tabBar.frame.height ?? 0))
        
        pageViewController!.view.frame = currentFrame
        
        pageViewController.didMove(toParent: self)
        view.gestureRecognizers = pageViewController.gestureRecognizers
        
        let titles = ["Giỏ hàng", "Đã mua", "Đã bán"]
        segmentedControl = TwicketSegmentedControl(frame: self.segmentView.bounds)
        segmentedControl.setSegmentItems(titles)
        segmentedControl.delegate = self
        self.segmentView.addSubview(segmentedControl)
        self.segmentedControl.move(to: 0)
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    

}

extension CartViewController:UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subController.firstIndex(of: viewController) ?? 0
        if currentIndex <= 0 {
           
            return subController[subController.count - 1]
        }
        
        return subController[currentIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subController.firstIndex(of: viewController) ?? 0
        if currentIndex >= subController.count - 1 {
           
            return subController[0]
        }
       
        return subController[currentIndex + 1]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return subController.count
    }
    
    
}

extension CartViewController:UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (!completed)
        {
            return
        }
        let currentIndex:Int = subController.firstIndex(of: (pageViewController.viewControllers?.first)! ) ?? 0
        segmentedControl.move(to: currentIndex)
        
        
    }
    
}

extension CartViewController: TwicketSegmentedControlDelegate {
    func didSelect(_ segmentIndex: Int) {
        print("Selected index: \(segmentIndex)")
        pageViewController.setViewControllers([subController[segmentIndex]], direction: .forward, animated: true, completion: nil)
        
    }
}
