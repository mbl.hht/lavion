//
//  ShoppingCartViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/11/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

struct ShoppingCartViewModel {
    var id:         String?
    var image:      String?
    var nameProduct: String?
    var cash: String?
    var number: Int?
}

class ShoppingCartViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var TotalPayView: UIView!
    @IBOutlet weak var payButton: UIButton!
    
    //var listProductArray = [ShoppingCartViewModel]()
    
    let realm = try! Realm()
    let productInCartList = (try! Realm()).objects(ProductInCartModel.self)
    var notificationToken: NotificationToken? = nil
    var total:Double = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib.init(nibName: "CartProductTableCell", bundle: nil), forCellReuseIdentifier: "cartProductTableCell")

        TotalPayView.addShadow(with: UIColor.darkGray)
        
        // set up button thanh toans
        payButton.backgroundColor = UIColor.mainColor
        payButton.setShadow(color: UIColor.mainColor!,cornerRadius: 0)
        
        for item in productInCartList {
            total = total + (item.product?.san_pham_gia_ban_thuc_te ?? 0) * Double(item.soLuong)
        }

        moneyLabel.text = "\(total.formatNumber()) VND"
        
        notificationToken = productInCartList.observe({ (object:RealmCollectionChange<Results<ProductInCartModel>>) in
              self.mainTableView.reloadData()
            self.total = 0
            for item in self.productInCartList {
                self.total = self.total + (item.product?.san_pham_gia_ban_thuc_te ?? 0) * Double(item.soLuong)
            }
            
            self.moneyLabel.text = "\(self.total.formatNumber()) VND"
        })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        mainTableView.reloadData()
    }
    
    
    @IBAction func PayAction(_ sender: UIButton) {
        if productInCartList.count <= 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: "Giỏ hàng bạn trống nên không thể thanh toán.", complete: nil)
            return
        }
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "payShoppingCartViewController") as! PayShoppingCartViewController
        vc.totalMonney = total
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
}

extension ShoppingCartViewController: UITableViewDelegate {
    
}

extension ShoppingCartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productInCartList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cartProductTableCell") as? CartProductTableCell else {
            return UITableViewCell()
        }
        cell.item = productInCartList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    
}
