//
//  DetailHistoryPayedViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/23/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class DetailHistoryPayedViewController: UIViewController {
    
    
    @IBOutlet weak var AddressLabel: UILabel!
    @IBOutlet weak var ThanhTienLabel: UILabel!
    
    @IBOutlet weak var phiShipLabel: UILabel!
    @IBOutlet weak var tongTienLabel: UILabel!
    
    @IBOutlet weak var totalPayView: UIView!

    @IBOutlet weak var mainTableView: UITableView!
    
    var listProductArray = [ShoppingCartViewModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavigation(text: "#12 Đã Thanh Toán",hidden: true)
        
        mainTableView.register(UINib.init(nibName: "CartProductTableCell", bundle: nil), forCellReuseIdentifier: "cartProductTableCell")
        mainTableView.delegate      = self
        mainTableView.dataSource    = self
        
        totalPayView.addShadow(with: UIColor.darkGray)
        
        // setup Data
        listProductArray.append(ShoppingCartViewModel.init(id: "1", image: "canci.png", nameProduct: "Siro maxkid calyvit chai 100ml", cash: "100000", number: 1))
        listProductArray.append(ShoppingCartViewModel.init(id: "2", image: "hop_chocolate.png", nameProduct: "Helix Naga Hộp 3 vỉ x 6 viên", cash: "11200000", number: 12))
        listProductArray.append(ShoppingCartViewModel.init(id: "3", image: "hop_vani2.png", nameProduct: "hop_vani2.png", cash: "1100000", number: 2))
        listProductArray.append(ShoppingCartViewModel.init(id: "3", image: "hop_vani2.png", nameProduct: "hop_vani2.png", cash: "1100000", number: 2))
        listProductArray.append(ShoppingCartViewModel.init(id: "3", image: "hop_vani2.png", nameProduct: "hop_vani2.png", cash: "1100000", number: 2))
        listProductArray.append(ShoppingCartViewModel.init(id: "3", image: "hop_vani2.png", nameProduct: "hop_vani2.png", cash: "1100000", number: 2))
        

    }

}

extension DetailHistoryPayedViewController:UITableViewDelegate {
    
}

extension DetailHistoryPayedViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listProductArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cartProductTableCell") as? CartProductTableCell else {
            return UITableViewCell()
        }
        //cell.item = listProductArray[indexPath.row]
        cell.removeButton.isHidden = true
        cell.numberGMS.isEnabled = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    
    
}
