//
//  BoughtViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/11/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

struct HistoryPayModel {
    var id          :Int?
    var status      :String?
    var totalCash   :NSInteger?
    var date        :String?
}

class BoughtViewController: UIViewController {
    
    var arrayList = [HistoryPayModel]()
    
    @IBOutlet weak var mainTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainTableView.delegate      = self
        mainTableView.dataSource    = self
        
        mainTableView.tableFooterView = UIView()
        mainTableView.estimatedRowHeight = 1000
        
        arrayList.append(HistoryPayModel.init(id: 1, status: "Đã thanh toán", totalCash: 100000, date: "12:11:17 12:04:16"))
        arrayList.append(HistoryPayModel.init(id: 1, status: "Đã thanh toán", totalCash: 200000, date: "12:11:17 12:14:16"))
        arrayList.append(HistoryPayModel.init(id: 1, status: "Đã thanh toán", totalCash: 200000, date: "12:11:17 12:14:17"))
    
        
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BoughtViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "detailHistoryPayedViewController") as! DetailHistoryPayedViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
}

extension BoughtViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PayedTableCell") as? PayedTableCell else {
            return UITableViewCell()
        }
        
        cell.item = arrayList[indexPath.row]
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    
}
