//
//  PayShoppingCartViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/17/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class PayShoppingCartViewController: UIViewController {
    
    var paysListArray = ["Địa chỉ của tôi","Địa chỉ của người khác","Văn phòng công ty","Cửa hành đại lý Offline"]
    var set = Set([0])
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var ThanhTienLabel: UILabel!
    
    @IBOutlet weak var phiShipLabel: UILabel!
    @IBOutlet weak var tongTienLabel: UILabel!
    
    @IBOutlet weak var totalPayView: UIView!
    
    @IBOutlet weak var datHangButton: UIButton!
    
    var generalSetting = (try! Realm()).objects(GeneralSettingModel.self).first
    
    var totalMonney: Double = 0
    var diaChiGui: Int = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavigation(text: "Phương thức giao hàng",hidden: true)
        
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib.init(nibName: "PayCartHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "PayCartHeaderView")
        
        mainTableView.tableFooterView = UIView()
        mainTableView.estimatedRowHeight = 1000
        
        
        totalPayView.addShadow(with: UIColor.darkGray)
        
        // set up button thanh toans
        datHangButton.backgroundColor = UIColor.mainColor
        datHangButton.setShadow(color: UIColor.mainColor!,cornerRadius: 0)
        
        
        //set Value
        ThanhTienLabel.text = totalMonney.formatNumber() + " VND"
        let phiship: Double = (generalSetting?.phi_chuyen_vnd ?? 0) * totalMonney
        phiShipLabel.text = "\(phiship) VND"
        

        tongTienLabel.text = (phiship + totalMonney).formatNumber() + " VND"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @objc func choosePay(sender: UIButton) {
        set.popFirst();
        set.insert(sender.tag)
        self.mainTableView.reloadData()

        

    }
    
    @IBAction func thanhToanAction(_ sender: Any) {
    }
    

}


extension PayShoppingCartViewController: UITableViewDelegate {
    
}

extension PayShoppingCartViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "PayCartHeaderView") as? PayCartHeaderView else {
            return UITableViewCell()
        }
        header.nameLabel.text = paysListArray[section]
        header.checkButton.tag = section
        if set.contains(section) == true {
            header.checkImage.image = UIImage(named: "RadioCheck_img.png")
        } else {
             header.checkImage.image = UIImage(named: "Uncheck_img.png")
        }
        header.checkButton.addTarget(self, action:  #selector(self.choosePay(sender:)), for: .touchUpInside)
        diaChiGui = section
        return header
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return paysListArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if set.contains(section) == true && section != 2 {
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NamePersonCartTableCell") as? NamePersonCartTableCell else {
                return UITableViewCell()
            }
            cell.nameTextField.placeholder = "Địa chỉ của tôi"
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddressOtherCartTableCell") as? AddressOtherCartTableCell else {
                return UITableViewCell()
            }
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NamePersonCartTableCell") as? NamePersonCartTableCell else {
                return UITableViewCell()
            }
            cell.nameTextField?.placeholder = "Emai người cần gửi"
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 50
        case 1:
            return 150.0
        case 2:
            return 0
        case 3:
            return 50
        default:
            return 0
        }
    }
    
}
