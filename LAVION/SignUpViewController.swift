//
//  SignUpViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/13/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource  {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var nameUserTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var registerNumberTextField: UITextField!
    @IBOutlet weak var introductionNumberTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var CMNDTextFiled: UITextField!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var moveLoginButton: UIButton!
    @IBOutlet weak var aboutTextView: UITextView!
    
    var pickOption = ["An Giang","Bà Rịa - Vũng Tàu","Bắc Giang","Bắc Kạn","Bạc Liêu","Bắc Ninh","Bến Tre","Bình Định","Bình Dương","Bình Phước","Bình Thuận","Cà Mau","Cao Bằng","Đắk Lắk","Đắk Nông","Điện Biên","Đồng Nai","Đồng Tháp","Gia Lai","Hà Giang","Hà Nam","Hà Tĩnh","Hải Dương","Hậu Giang","Hòa Bình","Hưng Yên","Khánh Hòa","Kiên Giang","Kon Tum","Lai Châu","Lâm Đồng","Lạng Sơn","Lào Cai","Long An","Nam Định","Nghệ An","Ninh Bình","Ninh Thuận","Phú Thọ","Quảng Bình","Quảng Nam","Quảng Ngãi","Quảng Ninh","Quảng Trị","Sóc Trăng","Sơn La","Tây Ninh","Thái Bình","Thái Nguyên","Thanh Hóa","Thừa Thiên Huế","Tiền Giang","Trà Vinh","Tuyên Quang","Vĩnh Long","Vĩnh Phúc","Yên Bái","Phú Yên","Cần Thơ","Đà Nẵng","Hải Phòng","Hà Nội","TP HCM"]
    
    override func viewDidLoad() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.topItem?.title = "Đăng ký thành viên"
        //self.navigationController?.navigationItem.title = "Đăng ký thành viên"
        super.viewDidLoad()
        
        
        //Setting TextField
        emailTextField.keyboardType = .emailAddress
        emailTextField.clearButtonMode = .whileEditing
        
        passwordTextField.isSecureTextEntry =  true
        passwordTextField.clearButtonMode = .whileEditing
        
        nameUserTextField.keyboardType = .alphabet
        nameUserTextField.clearButtonMode = .whileEditing
        
        phoneNumberTextField.keyboardType = .phonePad
        phoneNumberTextField.clearButtonMode = .whileEditing
        
        introductionNumberTextField.keyboardType = .numberPad
        introductionNumberTextField.clearButtonMode = .whileEditing
        
        registerNumberTextField.keyboardType = .numberPad
        registerNumberTextField.clearButtonMode = .whileEditing
        
        CMNDTextFiled.keyboardType = .phonePad
        CMNDTextFiled.clearButtonMode = .whileEditing
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        cityTextField.inputView = pickerView
        cityTextField.clearButtonMode = .whileEditing
        
        //Setting Button
        registerButton.layer.cornerRadius = 5
        registerButton.layer.borderWidth = 1
        registerButton.layer.borderColor = UIColor.clear.cgColor
        registerButton.addTarget(self, action: #selector(self.registerAction(sender:)) , for: .touchUpInside)
        moveLoginButton.addTarget(self, action: #selector(self.moveLoginAction(sender:)) , for: .touchUpInside)
        
        //Setting TextView
        let attributedString = NSMutableAttributedString(string: "Khi bạn click vào Đăng ký là bạn đã đồng ý với Điều khoản sử dụng và Chính sách bảo mật trên Lavion.vn")
        attributedString.addAttribute(.link, value: "https://lavion.vn/chinh-sach-bao-mat", range: NSRange(location: 68, length: 19))
        attributedString.addAttribute(.link, value: "https://lavion.vn/", range: NSRange(location: 93, length: 9))
        aboutTextView.backgroundColor = .clear
        aboutTextView.isScrollEnabled = false
        aboutTextView.isEditable = false
        aboutTextView.attributedText = attributedString
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return false;
    }
    
    @objc func registerAction(sender : UIButton) {
        var stringError : String = "";
        
        if emailTextField.text?.count == 0 {
            stringError = "Email"
        } else if passwordTextField.text?.count == 0 {
            stringError = "Mật khẩu"
        } else if nameUserTextField.text?.count == 0 {
            stringError = "Họ và Tên"
        } else if phoneNumberTextField.text?.count == 0 {
            stringError = "Số điện thoại"
        }  else if registerNumberTextField.text?.count == 0 {
            stringError = "Mã đăng kí"
        }  else if introductionNumberTextField.text?.count == 0 {
            stringError = "Mã giới thiệu"
        } else if cityTextField.text?.count == 0 {
            stringError = "Tỉnh thành"
        } else if CMNDTextFiled.text?.count == 0 {
            stringError = "Số CMND"
        }
        
        if (stringError.count > 0) {
            stringError += " không được để trống."
            ErrorHelper.showErrorWithSelf(self, titleString: stringError, errorString:"", complete: nil)
            return ;
        } else {
            if emailTextField.text?.isValidEmail() == false {
                stringError = "Email"
            } else if phoneNumberTextField.text?.isValidPhone() == false {
                stringError = "Số điện thoại"
            }
            
            if stringError.count > 0 {
                stringError += " không hợp lệ."
                 ErrorHelper.showErrorWithSelf(self, titleString: stringError, errorString:"", complete: nil)
                return;
            }
        }
        
        UIHelper.showHUD()
        SyncHttpConnect.postRegister(email: emailTextField.text!, password: passwordTextField.text!, ma_dk: registerNumberTextField.text!, tinh_thanh: cityTextField.text!, cmnd: CMNDTextFiled.text!, ho_va_ten: nameUserTextField.text!, so_dien_thoai: phoneNumberTextField.text!, ma_gt: introductionNumberTextField.text!) { (data:NSDictionary? , sucess:Bool!) in
                UIHelper.dismisHUD()
                if sucess == true {
                    var error = ""
                    if let value:NSDictionary = data?.object(forKey: "errors") as? NSDictionary {
                        for array  in value.allValues {
                            for text in (array as! NSArray) {
                                let tam = text as! String
                                if tam.count > 0 {
                                    error += tam + "\n"
                                }
                                
                            }
                        }
                    }
                    if error.count > 0 {
                         ErrorHelper.showErrorWithSelf(self, titleString: "Lỗi Đăng kí", errorString:error, complete: nil)
                    }
                    
                    if data?.object(forKey: "message") as? Int == 200 {
                        ErrorHelper.showErrorWithSelf(self, titleString: "Đăng kí thành công", errorString:"Vào Email để kích hoạt tài khoản.", complete: nil)
                    }
                    
                } else {
                    ErrorHelper.showDisconnectInternet(viewController: self)
                }
            

        }
        
    }
    
    
    @objc func moveLoginAction(sender : UIButton) {
      self.navigationController?.popViewController(animated: true)
    }
    
    

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cityTextField.text = pickOption[row]
    }
}
