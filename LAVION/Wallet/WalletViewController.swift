//
//  WalletViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/3/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RKNotificationHub
import RealmSwift

class WalletViewController: UIViewController {
    @IBOutlet weak var backgroundMilkView: UIView!
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var backgroundVNDView: UIView!
    
    @IBOutlet weak var titleView: UIView!
    
    @IBOutlet weak var MILKLabel: UILabel!
    
    @IBOutlet weak var VNDLabel: UILabel!
    
    var hub:RKNotificationHub!
    let statistic = (try! Realm()).objects(StatisticModel.self).first
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleView.backgroundColor = UIColor.mainColor
        
        backgroundVNDView.layer.cornerRadius = 5
        backgroundVNDView.layer.borderWidth = 1
        backgroundVNDView.layer.borderColor = UIColor.clear.cgColor
        let layer = CAGradientLayer()
        var widthTypeIphone:CGFloat = 0.0
        switch UIDevice.current.screenType {
        case .iPhone_XSMax,.iPhone_XR,.iPhones_X_XS:
            widthTypeIphone = 50
            break;
        default :
            widthTypeIphone = 0.0
        }
        layer.frame = CGRect(x: backgroundVNDView.bounds.origin.x, y: backgroundVNDView.bounds.origin.y, width: backgroundVNDView.bounds.width + widthTypeIphone, height: backgroundVNDView.bounds.height)//backgroundVNDView.bounds
        layer.colors = [UIColor("#00c6ff")!.cgColor ,UIColor("#0072ff")!.cgColor]//0072ff
        layer.startPoint = CGPoint(x: 1, y: 0.5)
        layer.endPoint = CGPoint(x: 0, y: 0.5)
        
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.borderColor = UIColor.clear.cgColor
        backgroundVNDView.layer.insertSublayer(layer, at: 0)
        
        let layer1 = CAGradientLayer()
        layer1.frame = CGRect(x: backgroundMilkView.bounds.origin.x, y: backgroundMilkView.bounds.origin.y, width: backgroundMilkView.bounds.width + widthTypeIphone, height: backgroundMilkView.bounds.height)
        layer1.colors = [UIColor("#a8e063")!.cgColor ,UIColor("#56ab2f")!.cgColor]
        layer1.startPoint = CGPoint(x: 1.0, y: 0.5)
        layer1.endPoint = CGPoint(x: 0, y: 0.5)
        layer1.cornerRadius = 5
        layer1.borderWidth = 1
        layer1.borderColor = UIColor.clear.cgColor
        backgroundMilkView.layer.cornerRadius = 5
        backgroundMilkView.layer.borderWidth = 1
        backgroundMilkView.layer.borderColor = UIColor.clear.cgColor
        backgroundMilkView.layer.insertSublayer(layer1, at: 0)
        
        backgroundVNDView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: backgroundVNDView.layer.cornerRadius, opacity: 0.35)
        backgroundMilkView.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: backgroundMilkView.layer.cornerRadius, opacity: 0.35)
        
        hub = RKNotificationHub.init(view: notificationImageView)
        hub.moveCircleBy(x: -3, y: 3)
        hub.scaleCircleSize(by: 0.6)
        

    }
    

    @IBAction func makeMoneyHistoryAction(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc:HistoryWithdrawalsViewController = sb.instantiateViewController(withIdentifier: "historyWithdrawalsViewController") as! HistoryWithdrawalsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
        MILKLabel.text = "\(statistic?.the_milk.formatNumber() ?? "0") MILK"
        VNDLabel.text  = "\(statistic?.tong_tien_vnd.formatNumber() ?? "0") VND"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func openNotificationAction(_ sender: Any) {
        hub.increment()
    }
    
    
    @IBAction func historyTakeCashAction(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc:HistoryTakeCashViewController = sb.instantiateViewController(withIdentifier: "historyTakeCashViewController") as! HistoryTakeCashViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func historyMilkAction(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc:HistoryMilkViewController = sb.instantiateViewController(withIdentifier: "historyMilkViewController") as! HistoryMilkViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func transferMilkAction(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc:TransferMilkViewController = sb.instantiateViewController(withIdentifier: "transferMilkViewController") as! TransferMilkViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func transferMilkToVNDAction(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc:TransferMilkToVNDViewController = sb.instantiateViewController(withIdentifier: "transferMilkToVNDViewController") as! TransferMilkToVNDViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func napAction(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc:NapMILKViewController = sb.instantiateViewController(withIdentifier: "napMILKViewController") as! NapMILKViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

