//
//  HistoryNapMILKViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/13/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class HistoryNapMILKViewController: UIViewController {

    @IBOutlet weak var mainTable: UITableView!
    let historyNapMILKArray = (try! Realm()).objects(HistoryMILKLoadModel.self)
    var notificationToken: NotificationToken? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        initNavigation(text: "Lịch sử nạp MILK",hidden: true)
        
        // init TableView
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.register(UINib.init(nibName: "DetailHistoryTransferMILKToVNDCellTableViewCell", bundle: nil), forCellReuseIdentifier: "detailHistoryTransferMILKToVNDCellTableViewCell")
        notificationToken = historyNapMILKArray.observe({ (data) in
            self.mainTable.reloadData()
        })
    }
}

extension HistoryNapMILKViewController: UITableViewDelegate {
    
}

extension HistoryNapMILKViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyNapMILKArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailHistoryTransferMILKToVNDCellTableViewCell") as! DetailHistoryTransferMILKToVNDCellTableViewCell
        cell.item = historyNapMILKArray[indexPath.row]
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 121
    }
    
}

