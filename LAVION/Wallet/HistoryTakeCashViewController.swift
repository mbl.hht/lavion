//
//  HistoryTakeCashViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/8/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class HistoryTakeCashViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    let vNDWithdrawalArray = (try! Realm()).objects(VNDWithdrawalModel.self)
    let profile = (try! Realm()).objects(ProfileModel.self).first
    var nameBank: String? = ""
    var notificationToken: NotificationToken? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup TableView
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib.init(nibName: "HistoryTakeCashCell", bundle: nil), forCellReuseIdentifier: "historyTakeCashCell")
        mainTableView.register(UINib.init(nibName: "HistoryTakeCashHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HistoryTakeCashHeaderView")
        
        
        //navigationController
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.topItem?.title = "Lịch sử rút tiền"
        self.navigationController?.navigationBar.barTintColor = UIColor.mainColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 20)!]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        //
        
        nameBank = (try! Realm()).objects(BankModel.self).filter("id = \(profile?.idNganHang ?? 0)").first?.tenNganHang
        notificationToken = vNDWithdrawalArray.observe({ (data) in
            self.mainTableView.reloadData()
        })
        
    }
    
    
    

}

extension HistoryTakeCashViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vNDWithdrawalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "historyTakeCashCell") as? HistoryTakeCashCell else {
            return UITableViewCell()
        }
        let item = vNDWithdrawalArray[indexPath.row]
        
        cell.nameBankLabel.text = item.ngan_hang
        cell.statusLabel.text = item.tinh_trang
        cell.dateLabel.text = item.updated_at
        cell.cashLabel.text = "-\(item.so_tien.formatNumber())"
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HistoryTakeCashHeaderView") as? HistoryTakeCashHeaderView else {
            return UITableViewCell()
        }
        
        cell.nameBankLabel.text = nameBank
        cell.nameAccountLabel.text = profile?.tenChuTaiKhoan
        cell.numberAccountLabel.text = profile?.soTaiKhoan
        cell.addressLabel.text = profile?.ten_chi_nhanh

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 390
    }
    
}
