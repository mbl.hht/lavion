//
//  TransferMilkToVNDViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/11/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class TransferMilkToVNDViewController: UIViewController {
    
    @IBOutlet weak var numberMILKTextField: UITextField!
    
    @IBOutlet weak var numberMILKWantTransferTextField: UITextField!
    
    @IBOutlet weak var transactionFeeLabel: UILabel!
    
    @IBOutlet weak var numberVNDTextField: UITextField!
    
    @IBOutlet weak var transferButton: UIButton!
    
    @IBOutlet weak var historyTranferMILKToVNDButton: UIButton!
    
    
    let statistic = (try! Realm()).objects(StatisticModel.self).first
    let generalSetting = (try! Realm()).objects(GeneralSettingModel.self).first
    var notificationToken: NotificationToken? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        numberMILKWantTransferTextField.delegate = self
        
        //Init Navigation
        
        
        //Transfer Button
        transferButton.initRadius()
        
        numberMILKTextField.text = (statistic?.the_milk.formatNumber() ?? "0") + " MILK"
        notificationToken = statistic?.observe({ (ObjectChange) in
            self.numberMILKTextField.text = (self.statistic?.the_milk.formatNumber() ?? "0") + " MILK"
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initNavigation(text: "Chuyển đổi MILK sang VND",hidden: true)
    }
    
    
    
    @IBAction func transferAction(_ sender: Any) {
        var messageError = ""
        if numberMILKWantTransferTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Số MILK không được để trống"
        }
        
        if messageError.count > 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Thông Báo", errorString: messageError, complete: nil)
            return
        }
        
        UIHelper.showHUD()
        SyncHttpConnect.putTransferMILKToVND(MILK: numberMILKWantTransferTextField.text!, complete: { (data, success) in
            UIHelper.dismisHUD()
            if success {
                if data["status"] as! Int == 200 {
                    
                    
                    autoreleasepool {
                        let realm = try! Realm()
                        try! realm.write {
                            //                            self.profile?.soTaiKhoan       = self.numberAccountTextField.text!
                            //                            self.profile?.ten_chi_nhanh    = self.addressBankTextField.text!
                            //                            self.profile?.idNganHang       = (self.pickOption.firstIndex(of: self.nameBankTextField.text!) ?? -2) + 1
                        }
                        
                    }
                    
                    
                    
                    SyncHttpConnect.getStatistic { (data: NSDictionary?, success: Bool) in
                        if success == true && StatisticAccessor.set(data: data as! Dictionary<String, Any>) == true {
                        } else {
                            
                        }
                        
                    }
                    
                    SyncHttpConnect.getHistoryTransferMILKToVND { (data, success) in
                        if success == true && HistoryTransferMILKToVNDAccessor.set(data: data) == true {
                            //callLoadMenu(addType: .TypeVNDTransactionHistory)
                        } else {
                            //logout(type: .FailAPIWhenLogining)
                        }
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: "Đã thực hiện việc chuyển milk thành vnd thành công!", complete: {
                        //self.navigationController?.popViewController(animated: true)
                    })
                    
                    
                    
                } else if data["status"] as! Int == 400 {
                    messageError = data["errors"] as? String ?? ""
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: messageError, complete: nil)
                    
                }
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
        })
    }
    
    @IBAction func historyTranferMILKToVNDAction(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc:HistoryTransferMILKToVNDViewController = sb.instantiateViewController(withIdentifier: "historyTransferMILKToVNDViewController") as! HistoryTransferMILKToVNDViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension TransferMilkToVNDViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == numberMILKWantTransferTextField {
            
            
            let milkString:String = numberMILKWantTransferTextField.text ?? ""
            let mILKTranc:Double = Double(milkString) ?? 0.0
            if mILKTranc > (statistic?.the_milk)! {
                ErrorHelper.showErrorWithSelf(self, titleString: "Thông  ", errorString: "Số MILK bạn muốn chuyển vượt qua số MILK hiện tại của bạn.", complete: {
                        self.numberMILKWantTransferTextField.becomeFirstResponder()
                })
                return;
            }
            
            numberVNDTextField.text  = (mILKTranc * 1000 - mILKTranc * 1000 * (generalSetting?.phi_chuyen_vnd ?? 0)).formatNumber()  + " VND"
            transactionFeeLabel.text = "Phí chuyển \((ceil(mILKTranc * (generalSetting?.phi_chuyen_milk ?? 0))).formatNumber()) MILK / \((mILKTranc * 1000 * (generalSetting?.phi_chuyen_vnd ?? 0)).formatNumber()) VND"
        }
    }
    
}

