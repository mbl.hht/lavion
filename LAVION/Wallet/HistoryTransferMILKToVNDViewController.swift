//
//  HistoryTransferMILKToVNDViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/11/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

//struct HistoryTranferMILKToVNDModel {
//    var codeGD: String?
//    var status: String?
//    var numberMILK: String?
//    var numberVND: String?
//    var fee: String?
//    var date: String?
//}

class HistoryTransferMILKToVNDViewController: UIViewController {
    @IBOutlet weak var mainTableView: UITableView!
    
    let historyTranferMILKToVNDArray = (try! Realm()).objects(HistoryTransferMILKToVNDModel.self)
    var notificationToken: NotificationToken? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavigation(text: "Lịch sử chuyển đổi",hidden: true)
        
        // init TableView
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib.init(nibName: "DetailHistoryTransferMILKToVNDCellTableViewCell", bundle: nil), forCellReuseIdentifier: "detailHistoryTransferMILKToVNDCellTableViewCell")
        notificationToken = historyTranferMILKToVNDArray.observe({ (data) in
            self.mainTableView.reloadData()
        })
        
    }
    
    



}

extension HistoryTransferMILKToVNDViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyTranferMILKToVNDArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailHistoryTransferMILKToVNDCellTableViewCell") as! DetailHistoryTransferMILKToVNDCellTableViewCell
        let item:HistoryTransferMILKToVNDModel = historyTranferMILKToVNDArray[indexPath.row]
        
        cell.initCell(codeGD: "\(item.id)", status: item.status, numberMilk: "\(item.milk_chuyen)", numberVND: item.vnd_nhan_duoc.formatNumber(), fee: "\(item.phi_chuyen_milk)", date: item.created_at)
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 121
    }
    
    
}
