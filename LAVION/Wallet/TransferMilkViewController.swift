//
//  TransferMilkViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/11/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class TransferMilkViewController: UIViewController {

    
    
    @IBOutlet weak var numberMILKTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var namePersonLabel: UILabel!
    
    @IBOutlet weak var numberMILKTransferTextField: UITextField!
    
    @IBOutlet weak var transactionFeeLabel: UILabel!
    
    @IBOutlet weak var transferMILKButton: UIButton!
    
    @IBOutlet weak var historyTransferMilkButton: UIButton!
    
    
    let statistic = (try! Realm()).objects(StatisticModel.self).first
    let generalSetting = (try! Realm()).objects(GeneralSettingModel.self).first
    var notificationToken: NotificationToken? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        numberMILKTransferTextField.delegate = self
     
        //Init button
        transferMILKButton.initRadius()
        notificationToken = statistic?.observe({ (objectChange) in
            self.numberMILKTextField.text = "\(self.statistic?.the_milk.formatNumber() ?? "0") MILK"
        })
        numberMILKTextField.text = "\(statistic?.the_milk.formatNumber() ?? "0") MILK"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Init Navigation
        initNavigation(text: "CHUYỂN MILK nội bộ")
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    @IBAction func transferMILKAction(_ sender: Any) {
        var messageError = ""
        if emailTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Email không được để trống"
        } else if numberMILKTransferTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Số MILK không được để trống"
        }
        
        if messageError.count > 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Thông Báo", errorString: messageError, complete: nil)
            return
        }
        
        UIHelper.showHUD()
        SyncHttpConnect.putTranferMILKToLocal(email: emailTextField.text!, MILK: numberMILKTransferTextField.text!) { (data, success) in
            UIHelper.dismisHUD()
            if success {
                if data["status"] as! Int == 200 {
                    
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: (data["data"] as? String ??  ""), complete: {
                        //self.navigationController?.popViewController(animated: true)
                    })
                    SyncHttpConnect.getStatistic { (data: NSDictionary?, success: Bool) in
                        if success == true && StatisticAccessor.set(data: data as! Dictionary<String, Any>) == true {
                            
                        } else {
                            
                        }
                    }
                    
                    SyncHttpConnect.getHistoryTranferMILKToLocal { (data, success) in
                        if success == true && HistoryTranferMILKToLocalAccessor.set(data: data) == true {
                        } else {
                            
                        }
                    }


                    
                    
                    
                } else if data["status"] as! Int == 400 {
                    messageError = data["errors"] as? String ?? ""
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: messageError, complete: nil)
                    
                }
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
        }
        
    }
    
    @IBAction func historyTransferMILKAction(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc:HistoryTransferMILKLocalViewController = sb.instantiateViewController(withIdentifier: "historyTransferMILKLocalViewController") as! HistoryTransferMILKLocalViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}

extension TransferMilkViewController:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTextField {
            if (emailTextField.text?.count ?? 0) > 0 {
                if (emailTextField.text?.isValidEmail() ?? false) {
                    UIHelper.showHUD()
                    SyncHttpConnect.getCheckEmail(email: emailTextField.text!) { (data, scucess) in
                        UIHelper.dismisHUD()
                        if scucess {
                            DispatchQueue.main.async {
                                if (data["status"] as? Int ?? 0) == 200 {
                                    self.namePersonLabel.text = "Tên người nhận : \((data["data"] as! Dictionary<String,String>)["ho_va_ten"] ?? "")"
                                } else {
                                    self.namePersonLabel.text = "Tên người nhận :"
                                }
                            }
                        }
                    }
                    
                } else {
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: "Không phải định dạng email.", complete: nil)
                }
            }
        }
        
        if textField == numberMILKTransferTextField {
            
            
            let milkString:String = numberMILKTransferTextField.text ?? ""
            let mILKTranc:Double = Double(milkString) ?? 0.0
            if mILKTranc > (statistic?.the_milk)! {
                ErrorHelper.showErrorWithSelf(self, titleString: "Thông  ", errorString: "Số MILK bạn muốn chuyển vượt qua số MILK hiện tại của bạn.", complete: {
                    self.numberMILKTransferTextField.becomeFirstResponder()
                })
                return;
            }
            
            transactionFeeLabel.text = "Phí chuyển \((ceil(mILKTranc * (generalSetting?.phi_chuyen_milk ?? 0))).formatNumber()) MILK"
        }

        
    }
    
    
}
