//
//  HistoryMilkViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/10/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class HistoryMilkViewController: UIViewController {
    @IBOutlet weak var mainTableView: UITableView!
    
    
    let mILKTransactionHistoryArray = (try! Realm()).objects(MILKTransactionHistoryModel.self)
    var notificationToken: NotificationToken? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // init Navigation
        initNavigation(text: "Lịch sử giao dịch MILK")
        
        //init TableView
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib.init(nibName: "DetailHistoryCashCell", bundle: nil), forCellReuseIdentifier: "detailHistoryCashCell")
        
        notificationToken = mILKTransactionHistoryArray.observe({ (ObjectChange) in
            self.mainTableView.reloadData()
        })
        
    }
    
}


extension HistoryMilkViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mILKTransactionHistoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailHistoryCashCell") as! DetailHistoryCashCell
        cell.item = mILKTransactionHistoryArray[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }
    
  
}
