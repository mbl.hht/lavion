//
//  HistoryWithdrawalsViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/4/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class HistoryWithdrawalsViewController: UIViewController {
    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var notificationLabel: UILabel!
    let vNDTransactionHistoryArray = (try! Realm()).objects(VNDTransactionHistoryModel.self)

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib.init(nibName: "DetailHistoryCashCell", bundle: nil), forCellReuseIdentifier: "detailHistoryCashCell")
        
        
        
        //navigationController
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.topItem?.title = "Lịch sử kiếm tiền"
        self.navigationController?.navigationBar.barTintColor = UIColor.mainColor
        //self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 20)!]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        if vNDTransactionHistoryArray.count == 0 {
            notificationLabel.isHidden = false
            mainTableView.isHidden = true
        } else {
            notificationLabel.isHidden = true
            mainTableView.isHidden = false
        }
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension HistoryWithdrawalsViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vNDTransactionHistoryArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "detailHistoryCashCell") as? DetailHistoryCashCell else {
            return UITableViewCell()
        }
        
        cell.nameLabel.text     = vNDTransactionHistoryArray[indexPath.row].email
        cell.contentLabel.text  = vNDTransactionHistoryArray[indexPath.row].ghi_chu
        cell.dateLabel.text     = vNDTransactionHistoryArray[indexPath.row].created_at
        cell.cashLabel.text     = "+" + vNDTransactionHistoryArray[indexPath.row].so_tien.formatNumber()
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }
    
    
}
