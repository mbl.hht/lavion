//
//  HistoryTransferMILKLocalViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/11/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift


class HistoryTransferMILKLocalViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    var token: NotificationToken?
    
    var listHistoryArray = (try! Realm()).objects(HistoryTranferMILKToLocalModel.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.topItem?.title = "Lịch sử chuyển MILK nội bộ"
        self.tabBarController?.tabBar.isHidden = true
        // init TableView
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.register(UINib.init(nibName: "DetailHistoryTransferMILKToLocalCell", bundle: nil), forCellReuseIdentifier: "detailHistoryTransferMILKToLocalCell")
        
        token = listHistoryArray.observe({ (data) in
            self.mainTableView.reloadData()
        })
        
    }
    

}


extension HistoryTransferMILKLocalViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listHistoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "detailHistoryTransferMILKToLocalCell") as? DetailHistoryTransferMILKToLocalCell else {
            return UITableViewCell()
        }
        cell.item = listHistoryArray[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176
    }
    
    
}
