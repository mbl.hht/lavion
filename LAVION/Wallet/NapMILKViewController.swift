//
//  NapMILKViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/13/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import UIKit

class NapMILKViewController: UIViewController {

    @IBOutlet weak var maNapMILKTextField: UITextField!
    @IBOutlet weak var napButton: UIButton!
    @IBOutlet weak var numberSeriTextField: UITextField!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavigation(text: "Nạp MILK", hidden: true)
        napButton.initRadius()
    }
    
    @IBAction func napAction(_ sender: UIButton) {
        var messageError = ""
        if maNapMILKTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Mã nạp không được để trống"
        } else if numberSeriTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Số Seri không được để trống"
        }
        
        if messageError.count > 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Thông Báo", errorString: messageError, complete: nil)
            return
        }
        UIHelper.showHUD()
        SyncHttpConnect.postMILKLoad(ma_nap_diem: maNapMILKTextField.text!, seri: numberSeriTextField.text!) { (data, success) in
            UIHelper.dismisHUD()
            if success {
                
                if (data["status"] as? Int ?? 0) == 200 {
                    
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: (data["data"] as? String ??  ""), complete: {
                        //self.navigationController?.popViewController(animated: true)
                    })
//                    SyncHttpConnect.getStatistic { (data: NSDictionary?, success: Bool) in
//                        if success == true && StatisticAccessor.set(data: data as! Dictionary<String, Any>) == true {
//                            
//                        } else {
//                            
//                        }
//                    }
//                    
//                    SyncHttpConnect.getHistoryTranferMILKToLocal { (data, success) in
//                        if success == true && HistoryTranferMILKToLocalAccessor.set(data: data) == true {
//                        } else {
//                            
//                        }
//                    }
                    
                    
                    
                    
                    
                } else if data["status"] as! Int == 400 {
                    messageError = data["errors"] as? String ?? ""
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: messageError, complete: nil)
                    
                }
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
        }
    }
    
    @IBAction func openHistoryNapMILKAction(_ sender: UIButton) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc:HistoryNapMILKViewController = sb.instantiateViewController(withIdentifier: "historyNapMILKViewController") as! HistoryNapMILKViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
