//
//  HistoryTransferMILKToVNDAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/7/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift

class HistoryTransferMILKToVNDAccessor: NSObject {
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
            let parseData = (data["data"] as! Dictionary<String, Any>)["data"] as! Array<Dictionary<String, Any>>
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
        
                    for item in parseData {
                        let historyTransferMILKToVNDModel  = HistoryTransferMILKToVNDModel()
                        historyTransferMILKToVNDModel.id = item["id"] as? Int ?? -1
                        historyTransferMILKToVNDModel.email_nguoi_chuyen    = item["email_nguoi_chuyen"] as? String ?? ""
                        historyTransferMILKToVNDModel.ho_ten_nguoi_chuyen   = item["ho_ten_nguoi_chuyen"] as? String ?? ""
                        historyTransferMILKToVNDModel.milk_chuyen           = item["milk_chuyen"] as? Int ?? 0
                        historyTransferMILKToVNDModel.phi_chuyen_milk       = item["phi_chuyen_milk"] as? Double ?? 0.0//1,
                        historyTransferMILKToVNDModel.vnd_nhan_duoc         = item["vnd_nhan_duoc"] as? Double ?? 0.0//900 ,
                        historyTransferMILKToVNDModel.vnd_truoc_khi_chuyen  = item["vnd_truoc_khi_chuyen"] as? Double ?? 0.0//0,
                        historyTransferMILKToVNDModel.vnd_sau_khi_chuyen    = item["vnd_sau_khi_chuyen"] as? Double ?? 0.0//9000,
                        historyTransferMILKToVNDModel.milk_truoc_khi_chuyen = item["milk_truoc_khi_chuyen"] as? Double ?? 0.0//10000,
                        historyTransferMILKToVNDModel.milk_sau_khi_chuyen   = item["milk_sau_khi_chuyen"] as? Double ?? 0.0//9990,
                        historyTransferMILKToVNDModel.status                = item["status"] as? String ?? ""//null,
                        historyTransferMILKToVNDModel.message               =  item["message"] as? String ?? ""//null,
                        historyTransferMILKToVNDModel.hash1                 =  item["hash"] as? String ?? ""//"58df0a3a-c6ea-4713-a293-5895a0956779",
                        historyTransferMILKToVNDModel.created_at            =  item["created_at"] as? String ?? ""//"2019-01-02 18:28:20",
                        historyTransferMILKToVNDModel.updated_at            =  item["updated_at"] as? String ?? ""//"2019-01-02 18:28:20"
                        
                        try! realm.write {
                            realm.add(historyTransferMILKToVNDModel, update: true)
                        }
                        
                    }
                    
                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
}
