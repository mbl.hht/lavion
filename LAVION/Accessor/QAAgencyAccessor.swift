//
//  QAAgencyAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/14/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class QAAgencyAccessor: NSObject {
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
            let parseData = (data["data"] as! Dictionary<String, Any>)["data"] as! Array<Dictionary<String, Any>>
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
                    for item in parseData {
                        let model = QAAgencyModel()
                        model.id                        = item["id"] as? Int ?? 0
                        model.ho_va_ten                 = item["ho_va_ten"] as? String ?? ""
                        model.email                     = item["email"] as? String ?? ""
                        model.noi_dung_cau_hoi          = item["noi_dung_cau_hoi"] as? String ?? ""
                        model.created_at                = item["created_at"] as? String ?? ""
                        model.so_luong_cau_tra_loi      = item["so_luong_cau_tra_loi"] as? Int ?? 0
                        
                        try! realm.write {
                            realm.add(model, update: true)
                        }
                    }
                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
}
