//
//  HistoryMILKLoadAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/14/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift

class HistoryMILKLoadAccessor: Object {
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
            let parseData = (data["data"] as! Dictionary<String, Any>)["data"] as! Array<Dictionary<String, Any>>
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
                    for item in parseData {
                        let model = HistoryMILKLoadModel()
                        model.id            = item["id"] as? Int ?? 0
                        model.so_diem       = item["so_diem"] as? Double ?? 0.0
                        model.trang_thai    = item["trang_thai"] as? String ?? ""
                        model.updated_at    = item["updated_at"] as? String ?? ""
                        
                        try! realm.write {
                            realm.add(model, update: true)
                        }
                    }
                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
}
