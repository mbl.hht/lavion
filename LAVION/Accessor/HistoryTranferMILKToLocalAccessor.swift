//
//  HistoryTranferMILKToLocalAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/12/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class HistoryTranferMILKToLocalAccessor: NSObject {
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
            let parseData = (data["data"] as! Dictionary<String, Any>)["data"] as! Array<Dictionary<String, Any>>
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
                    for item in parseData {
                        let model  = HistoryTranferMILKToLocalModel()
                        model.id = item["id"] as? Int ?? 0
                        model.ho_ten_nguoi_chuyen = item["ho_ten_nguoi_chuyen"] as? String ?? ""
                        model.email_nguoi_chuyen = item["email_nguoi_chuyen"] as? String ?? ""
                        model.email_nguoi_nhan = item["email_nguoi_nhan"] as? String ?? ""
                        model.ho_ten_nguoi_nhan = item["ho_ten_nguoi_nhan"] as? String ?? ""
                        model.so_milk_chuyen = item["so_milk_chuyen"] as? Double ?? 0.0
                        model.phi_chuyen_milk = item["phi_chuyen_milk"] as? Double ?? 0.0
                        model.hash1 = item["hash"] as? String ?? ""
                        model.trang_thai = item["trang_thai"] as? String ?? ""
                        model.created_at = item["created_at"] as? String ?? ""
                        model.updated_at = item["updated_at"] as? String ?? ""
                        
                        try! realm.write {
                            realm.add(model, update: true)
                        }
                    }
                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
    
}
