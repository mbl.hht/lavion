//
//  ProductAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/16/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//
import Foundation
import RealmSwift
class ProductAccessor: NSObject {
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
            let parseData = (data["data"] as! Dictionary<String, Any>)["data"] as! Array<Dictionary<String, Any>>
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
                    for item in parseData {
                        let model = ProductModel()
                        model.id                                        = item["id"] as? Int ?? 0
                        model.san_pham_ma                               = item["san_pham_ma"] as? String ?? ""
                        model.san_pham_ten                              = item["san_pham_ten"] as? String ?? ""
                        model.san_pham_url                              = item["san_pham_url"] as? String ?? ""
                        model.san_pham_mo_ta                            = item["san_pham_mo_ta"] as? String ?? ""
                        model.san_pham_so_luong                         = item["san_pham_so_luong"] as? Double ?? 0.0
                        model.san_pham_hinh_dai_dien                    = address + (item["san_pham_hinh_dai_dien"] as? String ?? "")
                        model.san_pham_hinh_anh_slide                   = item["san_pham_hinh_anh_slide"] as? String ?? ""
                        model.san_pham_noi_dung_tab_1                   = item["san_pham_noi_dung_tab_1"] as? String ?? ""
                        model.san_pham_noi_dung_tab_2                   = item["san_pham_noi_dung_tab_2"] as? String ?? ""
                        model.san_pham_noi_dung_tab_3                   = item["san_pham_noi_dung_tab_3"] as? String ?? ""
                        model.san_pham_noi_dung_tab_4                   = item["san_pham_noi_dung_tab_4"] as? String ?? ""
                        model.san_pham_noi_dung_tab_5                   = item["san_pham_noi_dung_tab_5"] as? String ?? ""
                        model.san_pham_keyword                          = item["san_pham_keyword"] as? String ?? ""
                        model.san_pham_tags                             = item["san_pham_tags"] as? String ?? ""
                        model.san_pham_gia_ban_thuc_te                  = item["san_pham_gia_ban_thuc_te"] as? Double ?? 0.0
                        model.created_at                                = item["created_at"] as? String ?? ""
                        model.updated_at                                = item["updated_at"] as? String ?? ""
                        try! realm.write {
                            realm.add(model, update: true)
                        }
                    }
                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
    
}
