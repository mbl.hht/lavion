//
//  GeneralSettingAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/9/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class GeneralSettingAccessor: NSObject {
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
            let parseData = data["data"] as! Dictionary<String, Any>
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
                    let model  = GeneralSettingModel()

                    model.id = parseData["id"] as? Int ?? 0
                    model.thanh_vien_phi_chuyen_milk_sang_vnd = parseData["thanh_vien_phi_chuyen_milk_sang_vnd"] as? Double ?? 0.0
                    model.thanh_vien_phi_chuyen_vnd_sang_milk = parseData["thanh_vien_phi_chuyen_vnd_sang_milk"] as? Double ?? 0.0
                    model.thanh_vien_phi_chuyen_milk_sang_tai_khoan_khac = parseData["thanh_vien_phi_chuyen_milk_sang_tai_khoan_khac"] as? Double ?? 0.0
                    model.dai_ly_online_phi_chuyen_milk_sang_vnd = parseData["dai_ly_online_phi_chuyen_milk_sang_vnd"] as? Double ?? 0.0
                    model.dai_ly_online_phi_chuyen_vnd_sang_milk = parseData["dai_ly_online_phi_chuyen_vnd_sang_milk"] as? Double ?? 0.0
                    model.dai_ly_online_phi_chuyen_milk_sang_tai_khoan_khac = parseData["dai_ly_online_phi_chuyen_milk_sang_tai_khoan_khac"] as? Double ?? 0.0
                    model.dai_ly_offline_phi_chuyen_milk_sang_vnd = parseData["dai_ly_offline_phi_chuyen_milk_sang_vnd"] as? Double ?? 0.0
                    model.dai_ly_offline_phi_chuyen_vnd_sang_milk = parseData["dai_ly_offline_phi_chuyen_vnd_sang_milk"] as? Double ?? 0.0
                    model.dai_ly_online_phi_chuyen_milk_sang_tai_khoan_khac = parseData["dai_ly_online_phi_chuyen_milk_sang_tai_khoan_khac"] as? Double ?? 0.0
                    
                    model.phi_chuyen_milk   = parseData["phi_chuyen_milk"]  as? Double ?? 0.0
                    model.phi_chuyen_vnd    = parseData["phi_chuyen_vnd"]   as? Double ?? 0.0
                    try! realm.write {
                        realm.add(model, update: true)
                    }
                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
}
