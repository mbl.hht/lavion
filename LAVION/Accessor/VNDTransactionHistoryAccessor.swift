//
//  VNDTransactionHistoryAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/3/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift

class VNDTransactionHistoryAccessor: NSObject {
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
            let parseData = (data["data"] as! Dictionary<String, Any>)["data"] as! Array<Dictionary<String, Any>>
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
                    var countId = 0
                    for item in parseData {
                        let transactionVND = VNDTransactionHistoryModel()
                        transactionVND.email        = item["email"] as? String ?? ""
                        transactionVND.so_tien      = item["so_tien"] as? Double ?? 0.0
                        transactionVND.ghi_chu      = item["ghi_chu"] as? String ?? ""
                        transactionVND.vnd_real     = item["vnd_real"] as? Double ?? 0.0
                        transactionVND.milk_real    = item["milk_real"] as? Double ?? 0.0
                        transactionVND.vnd_pending  = item["vnd_pending"] as? Double ?? 0.0
                        transactionVND.milk_pending = item["milk_pending"] as? Double ?? 0.0
                        transactionVND.created_at   = item["created_at"] as? String ?? ""
                        transactionVND.id           = countId
                        countId += 1
                        
                        try! realm.write {
                            realm.add(transactionVND, update: true)
                        }

                    }
                    
                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
}

