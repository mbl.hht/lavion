//
//  VNDWithdrawalAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/4/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class VNDWithdrawalAccessor: NSObject {
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
            let parseData = (data["data"] as! Dictionary<String, Any>)["data"] as! Array<Dictionary<String, Any>>
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
                    for item in parseData {
                        let withdrawalModel = VNDWithdrawalModel()
                        withdrawalModel.id              = item["id"] as! Int
                        withdrawalModel.email_rut_tien  = item["email_rut_tien"] as! String
                        withdrawalModel.so_tien         = item["so_tien"] as! Double
                        withdrawalModel.tinh_trang      = item["tinh_trang"] as? String ?? ""
                        withdrawalModel.ngan_hang       = item["ngan_hang"] as? String ?? ""
                        withdrawalModel.thong_bao       = item["thong_bao"] as? String ?? ""
                        withdrawalModel.updated_at      = item["updated_at"] as? String ?? ""
                        withdrawalModel.created_at      = item["created_at"] as? String ?? ""
                        try! realm.write {
                            realm.add(withdrawalModel, update: true)
                        }
                        
                    }
                    
                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
}
