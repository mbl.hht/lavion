//
//  MILKTransactionHistoryAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/6/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation

import RealmSwift

class MILKTransactionHistoryAccessor: NSObject {
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
            let parseData = (data["data"] as! Dictionary<String, Any>)["data"] as! Array<Dictionary<String, Any>>
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
                    var countId = 0
                    for item in parseData {
                        let transactionMILK  = MILKTransactionHistoryModel()
                        transactionMILK.email        = item["email"] as? String ?? ""
                        transactionMILK.so_tien      = item["so_tien"] as? Double ?? 1.0
                        transactionMILK.ghi_chu      = item["ghi_chu"] as? String ?? ""
                        transactionMILK.created_at   = item["created_at"] as? String ?? ""
                        transactionMILK.id           = countId
                        countId += 1
                        
                        try! realm.write {
                            realm.add(transactionMILK, update: true)
                        }
                        
                    }
                    
                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
}
