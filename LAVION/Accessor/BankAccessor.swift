//
//  BankAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/27/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift

class BankAccessor: NSObject {
    static func set(_ data:NSArray!) -> Bool  {
        if data.count > 0 {
            for item in data {
                let currentItem = item as! NSDictionary
                autoreleasepool {
                    let realm = try! Realm()
                    let bank = BankModel()
                    bank.id          = currentItem.value(forKey: "id") as! Int
                    bank.tenNganHang = currentItem.value(forKey: "ten_ngan_hang") as! String
                    try! realm.write {
                        realm.add(bank, update: true)
                    }
                }
                
            }
        }
        return true
    }
}
