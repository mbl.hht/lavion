//
//  StatisticAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/1/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift

class StatisticAccessor: NSObject {
    
    static func set(data: Dictionary<String,Any>) -> Bool {
        let statusCode = data["status"] as? Int ?? 0
        if statusCode == TypeStatusCode.success.rawValue {
             let parseData = (data["data"] as! Dictionary<String, Any>)["tienNguoiDung"] as? Dictionary<String, Any> ?? Dictionary<String,Any>.init()
            let parseData2 = data["data"] as? Dictionary<String, Any> ?? Dictionary<String, Any>.init()
            if parseData.count > 0 {
                autoreleasepool {
                    let realm = try! Realm()
                    let statisticModel = StatisticModel()
                    statisticModel.id                        = parseData["id"] as! Int
                    statisticModel.email                     = parseData["email"] as! String
                    statisticModel.tong_tien_vnd             = parseData["tong_tien_vnd"] as! Double
                    statisticModel.nap_api                   = parseData["nap_api"] as! Double
                    statisticModel.the_milk                  = parseData["the_milk"] as! Double
                    statisticModel.thuong_gioi_thieu_ctv     = parseData["thuong_gioi_thieu_ctv"] as! Double
                    statisticModel.thuong_dai_ly             = parseData["thuong_dai_ly"] as! Double
                    statisticModel.thuong_hoa_hong_muc_1     = parseData["thuong_hoa_hong_muc_1"] as! Double
                    statisticModel.thuong_hoa_hong_muc_2     = parseData["thuong_hoa_hong_muc_2"] as! Double
                    statisticModel.created_at                = parseData["created_at"] as! String
                    statisticModel.updated_at                = parseData["updated_at"] as! String
                    statisticModel.vnd_pending               = parseData["vnd_pending"] as! Double
                    statisticModel.vnd_chuyen_tu_milk        = parseData["vnd_chuyen_tu_milk"] as! Double
                    statisticModel.vnd_mua_hang              = parseData["vnd_mua_hang"] as! Double
                    statisticModel.milk_chuyen_ctv_khac      = parseData["milk_chuyen_ctv_khac"] as! Double
                    statisticModel.milk_pending              = parseData["milk_pending"] as! Double
                    statisticModel.thuong_vnd_tu_dang_ky     = parseData["thuong_vnd_tu_dang_ky"] as! Double
                    statisticModel.thuong_milk_tu_dang_ky    = parseData["thuong_milk_tu_dang_ky"] as! Double
                    statisticModel.thuong_milk_gioi_thieu    = parseData["thuong_milk_gioi_thieu"] as! Double
                    statisticModel.milk_chuyen_tu_vnd        = parseData["milk_chuyen_tu_vnd"] as! Double
                    statisticModel.tongTienVNDKiemDuoc       = parseData2["tongTienVNDKiemDuoc"] as? Double ?? 0.0
                    statisticModel.tongMilkKiemDuoc          = parseData2["tongMilkKiemDuoc"] as? Double ?? 0.0
                    statisticModel.soCongTacVienGioiThieu       = parseData2["soCongTacVienGioiThieu"] as? Int ?? 0
                    statisticModel.soCongTacVienGioiThieuDaKichHoat       = parseData2["soCongTacVienGioiThieuDaKichHoat"] as? Int ?? 0
                    statisticModel.soDonHangDaBan       = parseData2["soDonHangDaBan"] as? Int ?? 0
                    statisticModel.soDonHangDaBanThanhCong       = parseData2["soDonHangDaBanThanhCong"] as? Int ?? 0
                     statisticModel.soDonMuaHang       = parseData2["soDonMuaHang"] as? Int ?? 0
                    
                    
                    try! realm.write {
                        realm.add(statisticModel, update: true)
                    }

                }
            }
            return true
        } else if statusCode == TypeStatusCode.failed.rawValue {
            return false
        }
        return false
    }
    
}
