//
//  ProfileAccessor.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/23/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

//        "status": 200,
//        "data": {



//            "tinh_thanh": "An Giang",
//            "li_do_khong_duyet": null,
//            "convert_email": "tranvthanhson@gmail.com1",
//            "so_thanh_vien_da_gioi_thieu": 1,
//            "id_nguoi_dung": null,
//            "id_ngan_hang": 9,
//            "created_at": "2018-08-15 19:56:16",
//            "updated_at": "2018-12-20 14:36:30",
//            "avatar_cmnd": null,
//            "dia_chi_tu_ip_dang_ky": "Ho Chi Minh, VN",
//            "is_thanh_vien": "THANH VIEN",
//            "ly_do_nang_cap": null,
//            "admin_duyet_nang_cap": null,
//            "ngay_duyet_nang_cap": null,
//            "is_mail_checked": "YES",
//            "trang_thai_xet_duyet": "NOT YET",
//            "ly_do_block": null,
//            "chung_ip_dang_ky": 1,
//            "is_change_img_01": "NO",
//            "is_change_img_02": "NO",
//            "is_change_avatar": "NO",
//            "ten_nguoi_gioi_thieu": "Nguyễn Quốc Long",
//            "so_dien_thoai_gioi_thieu": "0905523543"
//        },
//        "message": "",
//        "errors": null

class ProfileAccessor: NSObject {
    
    static func set(_ data:NSDictionary!) -> Bool {
        if  data.count > 0 {
            autoreleasepool {
                let realm = try! Realm()
                let profile = ProfileModel()
                profile.id      = data["id"] as! NSInteger
                profile.email   = data["email"] as? String ?? ""
                profile.soDienThoai = data["so_dien_thoai"] as? String ?? ""
                profile.cmnd = data["cmnd"] as? String ?? ""
                profile.soTaiKhoan = data["so_tai_khoan"] as? String ?? ""
                profile.hoVaTen = data["ho_va_ten"] as? String ?? ""
                profile.gioiTinh = data["gioi_tinh"] as? String ?? ""
                profile.ten_chi_nhanh = data["ten_chi_nhanh"] as? String ?? ""
                profile.diaChiHienTai = data["dia_chi_hien_tai"] as? String ?? ""
                profile.diaChiCmnd = data["dia_chi_cmnd"] as? String ?? ""
                profile.tenChuTaiKhoan = data["ten_chu_tai_khoan"] as? String ?? ""
                profile.emailGioiThieu = data["email_gioi_thieu"] as? String ?? ""
                profile.ngaySinh = data["ngay_sinh"] as? String ?? ""
                profile.isDaiLy = ((data["is_dai_ly"] as? String ?? "NO") == "NO" ? false : true)
                profile.emailDaiLyQuanLy = data["email_dai_ly_quan_ly"] as? String ?? ""
                profile.img_01 = data["img_01"] as? String ?? ""
                profile.img_02 = data["img_02"] as? String ?? ""
                profile.avatar = data["avatar"] as? String ?? ""
                profile.tinhThanh = data["tinh_thanh"] as? String ?? ""
                profile.soThanhVienDaGioiThieu = data["so_thanh_vien_da_gioi_thieu"] as? Int ?? 0
                profile.idNganHang = data["id_ngan_hang"] as? Int ?? 0
                profile.isThanhVien = data["is_thanh_vien"] as? String ?? ""
                profile.tenNguoiGioiThieu = data["ten_nguoi_gioi_thieu"] as? String ?? ""
                profile.soDienThoaiGioiThieu = data["so_dien_thoai_gioi_thieu"] as? String ?? ""
                profile.facebook = data["facebook"] as? String ?? ""
                try! realm.write {
                    realm.add(profile, update: true)
                }
                
            }
            return true
        }
        return false
    }
    
    static func setMaGiamGia(maGiamGia: Int) -> Bool {
        autoreleasepool {
            let realm = try! Realm()
            let profile = realm.objects(ProfileModel.self).first
            try! realm.write {
                profile?.magiamgia = maGiamGia

            }
            
        }
        return true
    }
}
