//
//  PopularSearchTableCell.swift
//  LAVION
//
//  Created by iOS on 11/25/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
struct PopularSearchProductModel {
    var image           : String?
    var nameProduct     : String?
    var numberPeople    : String?
}

class PopularSearchTableCell: UITableViewCell {
    var listProductArray = [PopularSearchProductModel]()
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        mainCollectionView.register(UINib.init(nibName: "PopularSearchCollectionCell", bundle: nil), forCellWithReuseIdentifier: "popularSearchCollectionCell")
        if let flowLayout = mainCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let spacing:CGFloat = 1.5;
            let inset:CGFloat = 0;
            let numberOfColumns:CGFloat = 2;
            flowLayout.minimumInteritemSpacing = spacing
            flowLayout.minimumLineSpacing = spacing
            flowLayout.sectionInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            //set scroll to Vertical
            flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.width - numberOfColumns * spacing +
                2 - inset * numberOfColumns) / numberOfColumns, height: 100)
            flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        }
        
        //init Data
        listProductArray.append(PopularSearchProductModel(image: "canci.png", nameProduct: "Siro maxkid calyvit chai 100ml", numberPeople: "1234"))
        listProductArray.append(PopularSearchProductModel(image: "hop_chocolate.png", nameProduct: "Helix Naga Hộp 3 vỉ x 6 viên", numberPeople: "23"))
        listProductArray.append(PopularSearchProductModel(image: "hop_vani2.png", nameProduct: "Sữa viên ăn liền Mixmilk hương vani", numberPeople: "1235"))
        listProductArray.append(PopularSearchProductModel(image: "maxkid calyvid.png", nameProduct: "Siro Maxkid ribo chai 100 ml", numberPeople: "223"))
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PopularSearchTableCell:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listProductArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "popularSearchCollectionCell", for: indexPath) as! PopularSearchCollectionCell
        cell.item = listProductArray[indexPath.row]
        return cell
        
    }
    
    
}

extension PopularSearchTableCell:UICollectionViewDelegate {
    
}
