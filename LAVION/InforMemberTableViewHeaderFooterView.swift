//
//  InforMemberTableViewHeaderFooterView.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/25/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class InforMemberTableViewHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var avatarImageView: UIImageView!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    @IBOutlet weak var avatarButton: UIButton!
    
    var imagePicker = UIImagePickerController()
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.layer.borderWidth = 1.0
        avatarImageView.layer.masksToBounds = false
        avatarImageView.layer.borderColor = UIColor.white.cgColor
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
        avatarImageView.clipsToBounds = true
        avatarImageView.backgroundColor = .white
        
        //Image Picker
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
    }
//
//    
    func setAvatar(avatarImg: UIImage?) {
        avatarImageView.image = avatarImg
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    @IBAction func uploadImageAction(_ sender: Any) {
        self.parentContainerViewController()?.present(imagePicker, animated: true, completion: nil)
    }
    
    

}

extension InforMemberTableViewHeaderFooterView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                avatarImageView.image = image
            }
        self.parentContainerViewController()?.dismiss(animated: true, completion: nil)
        
        }
}
    

