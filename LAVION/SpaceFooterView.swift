//
//  SpaceFooterView.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/26/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class SpaceFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var heightViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var View: UIView!
    
    override func awakeFromNib() {
        logoutButton.layer.cornerRadius = 3
        logoutButton.layer.borderWidth = 1
        logoutButton.layer.borderColor = UIColor.clear.cgColor
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func LogoutAction(_ sender: Any) {
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        LoginHelper.logout(type: .Button)
        appDelegate?.loadLogin()
    }
    
}
