//
//  QAAgencyModel.swift
//  LAVION
//
//  Created by iOS on 12/2/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class QAAgencyModel: Object {
//    "ho_va_ten": "Nguyễn Thị Hậu",
//    "email": "nguyenthihau124ntto@gmail.com",
//    "id": 249,
//    "noi_dung_cau_hoi": "Em đăng kí xong rồi muốn kiếm tiền thì làm thế nào ạ",
//    "so_luong_cau_tra_loi": 0,
//    "created_at": "2018-12-13 22:44:17"
    @objc dynamic var id                = -1
    @objc dynamic var ho_va_ten         = ""
    @objc dynamic var email             = ""
    @objc dynamic var noi_dung_cau_hoi  = ""
    @objc dynamic var so_luong_cau_tra_loi = 0
    @objc dynamic var created_at        = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
