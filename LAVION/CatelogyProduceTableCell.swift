//
//  CatelogyProduceTableCell.swift
//  LAVION
//
//  Created by iOS on 11/25/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
struct CatelogyProduceModel {
    var image           : String?
    var title           : String?
}
class CatelogyProduceTableCell: UITableViewCell {
    var listProductArray = [CatelogyProduceModel]()
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        mainCollectionView.register(UINib.init(nibName: "CatelogyProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "catelogyProductCollectionCell")
        if let flowLayout = mainCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let spacing:CGFloat = 1.5;
            let inset:CGFloat = 0;
            let numberOfColumns:CGFloat = 3;
            flowLayout.minimumInteritemSpacing = spacing
            flowLayout.minimumLineSpacing = spacing
            flowLayout.sectionInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            //set scroll to Vertical
            flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.width - numberOfColumns * spacing +
                2 - inset * numberOfColumns) / numberOfColumns, height: 180)
            flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        }
        
        //init Data
//        listProductArray.append(ProcuductHotModel(image: "canci.png", detail: "Siro maxkid calyvit chai 100ml", cash: "100000 VND"))
//        listProductArray.append(ProcuductHotModel(image: "hop_chocolate.png", detail: "Helix Naga Hộp 3 vỉ x 6 viên", cash: "100000 VND"))
//        listProductArray.append(ProcuductHotModel(image: "hop_vani2.png", detail: "Sữa viên ăn liền Mixmilk hương vani", cash: "100000 VND"))
//        listProductArray.append(ProcuductHotModel(image: "maxkid calyvid.png", detail: "Siro Maxkid ribo chai 100 ml", cash: "90000 VND"))
        listProductArray.append(CatelogyProduceModel(image: "canci.png", title: "Thực phẩm bổ sung cho mọi độ tuổi"))
        listProductArray.append(CatelogyProduceModel(image: "hop_vani2", title: "Thực phẩm chức năng dành cho người lớn"))
        listProductArray.append(CatelogyProduceModel(image: "canci.png", title: "Mỹ phẩm làm đẹp cho phái đẹp"))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension CatelogyProduceTableCell:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listProductArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "catelogyProductCollectionCell", for: indexPath) as! CatelogyProductCollectionCell
        cell.item = listProductArray[indexPath.row]
        return cell
        
    }
    
    
}

extension CatelogyProduceTableCell:UICollectionViewDelegate {
    
}
