//
//  LoginViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/9/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import SVProgressHUD
class LoginViewController: BaseViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgetPasswordLabel: UILabel!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup textField
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.keyboardType = .emailAddress
        
        passwordTextField.isSecureTextEntry = true
        passwordTextField.clearButtonMode = .whileEditing
        
        
        //Setting button
        loginButton.layer.cornerRadius = 5
        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor.clear.cgColor
        loginButton.addTarget(self, action: #selector(self.loginAction(_:)), for: .touchUpInside)

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @IBAction func ForgetPasswordAction(_ sender: UIButton) {
        let vc = ForgetPasswordViewController.init(nibName: "ForgetPasswordViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func registerAction(_ sender: UIButton) {
        let vc = SignUpViewController.init(nibName: "SignUpViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func loginAction(_ sender: UIButton) {
        if emailTextField.text?.count == 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Email không được để trống.", errorString: "", complete: nil)
            return;
        }
        
        if passwordTextField.text?.count == 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Password không được để trống.", errorString: "",complete: nil)
            
            return;
        }
        
        if emailTextField.text?.isValidEmail() == false {
            ErrorHelper.showErrorWithSelf(self, titleString: "Email không hợp lệ", errorString: "", complete: nil)
            return
        }
        UIHelper.showHUD()
        SyncHttpConnect.postLogin(Email: emailTextField.text! , Password: passwordTextField.text!) { (data:NSDictionary?,success : Bool) in
            UIHelper.dismisHUD()
            if success == true {
                if (data!.object(forKey: "access_token") != nil) {
                    UserDefault.setToken(data!.object(forKey: "access_token") as! String)
                    UserDefault.setEmail(self.emailTextField.text ?? "")
                    UserDefault.setPassword(self.passwordTextField.text ?? "")
                    LoginHelper.typeLogin = .EnterUser
                    DispatchQueue.main.async {
                        let sb = UIStoryboard.init(name: "Main", bundle: nil)
                        let vc = sb.instantiateViewController(withIdentifier: "startAppViewController")
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    LoginHelper.loadData()
                } else {
                    ErrorHelper.showErrorWithSelf(self, titleString: "Lỗi đăng nhập", errorString: "Email hoặc password không đúng", complete: nil)
                }
                
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
