//
//  popularSearchCollectionCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/19/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import AlamofireImage
class PopularSearchCollectionCell: UICollectionViewCell {

    @IBOutlet weak var nameProductLabel: UILabel!
    @IBOutlet weak var numberPeopleSearchLabel: UILabel!
    
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var mainView: UIView!
    
    var item: PopularSearchProductModel? {
        didSet {
            nameProductLabel.text = item?.nameProduct
            numberPeopleSearchLabel.text = (item?.numberPeople ?? "0") + " người tìm kiếm"
            productImage.image = UIImage(named: item?.image ?? "")
    
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.initShadowForCell()
    }

}
