//
//  ForgetPasswordViewController.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/10/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: BaseViewController {
    @IBOutlet weak var recoverPasswordLabel: UILabel!
    @IBOutlet weak var contentRecoverPasswordLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.topItem?.title = "Khôi phục mật khẩu"
        //self.navigationController?.navigationItem.title = "Khôi phục mật khẩu"
        
        //Setting TextField
        emailTextField.clearButtonMode = .whileEditing
        emailTextField.keyboardType = .emailAddress
        
        //Setting Button
        resetButton.layer.cornerRadius = 5
        resetButton.layer.borderWidth = 1
        resetButton.layer.borderColor = UIColor.clear.cgColor
        resetButton.addTarget(self, action: #selector(self.forgetPasswordAction(sender:)), for: .touchUpInside)
    }
    

    
    @objc func forgetPasswordAction(sender : UIButton) {
        if emailTextField.text?.count == 0 {
            ErrorHelper.showErrorWithSelf(self, titleString: "Email không được để trống.", errorString: "", complete: nil)
            return;
        }
        
        if emailTextField.text?.isValidEmail() == false {
            ErrorHelper.showErrorWithSelf(self, titleString: "Email không hợp lệ.", errorString: "", complete: nil)
            return
        }
        UIHelper.showHUD()
        SyncHttpConnect.postForget(email: emailTextField.text!) { (data, success) in
             UIHelper.dismisHUD()
            if success == true {
                let dataMessageReset = data?.object(forKey: "error") as? String ?? ""
                if dataMessageReset.count > 0 {
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông báo", errorString: dataMessageReset, complete: nil)
                    return
                } else {
                    ErrorHelper.showErrorWithSelf(self, titleString: "Thông Báo", errorString:  data?.object(forKey: "message") as! String, complete:{
                        () in
                        if data?.object(forKey: "status") as! Int == 200 {
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                }
            } else {
                ErrorHelper.showDisconnectInternet(viewController: self)
            }
           
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    

}
