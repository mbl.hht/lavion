//
//  AppDelegate.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/6/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import DLRadioButton
import Realm
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        
        
        
        
        let config = Realm.Configuration(shouldCompactOnLaunch: { totalBytes, usedBytes in
            // totalBytes refers to the size of the file on disk in bytes (data + free space)
            // usedBytes refers to the number of bytes used by data in the file

            // Compact if the file is over 100MB in size and less than 50% 'used'
            let oneHundredMB = 100 * 1024 * 1024
            return (totalBytes > oneHundredMB) && (Double(usedBytes) / Double(totalBytes)) < 0.5
        })
        do {
            // Realm is compacted on the first open if the configuration block conditions were met.
            try Realm(configuration: config)

        } catch {
            autoreleasepool {
                // all Realm usage here
            }
            let realmURL = Realm.Configuration.defaultConfiguration.fileURL!
            let realmURLs = [
                realmURL,
                realmURL.appendingPathExtension("lock"),
                realmURL.appendingPathExtension("note"),
                realmURL.appendingPathExtension("management")
            ]
            for URL in realmURLs {
                do {
                    try FileManager.default.removeItem(at: URL)
                } catch {
                    // handle error
                }
            }
        }
        
        
        
        // Override point for customization after application launch.
        
        if UserDefault.getToken().count > 0 {
            SyncHttpConnect.postLogin(Email: UserDefault.getEmail(), Password: UserDefault.getPassword()) { (data:NSDictionary?, success:Bool) in
                if success == true {
                    if (data!.object(forKey: "access_token") != nil) {
                        UserDefault.setToken(data!.object(forKey: "access_token") as! String)
                        autoreleasepool{
                            let realm = try! Realm()
                            try! realm.write {
                                realm.deleteAll()
                                
                            }
                        }
                        LoginHelper.typeLogin = .AutoLogin
                        LoginHelper.loadData()
                        
                    } else {
                        //ErrorHelper.showErrorWithSelf((self.window?.rootViewController)!, titleString: "Lỗi đăng nhập", errorString: "Phiên đăng nhập đã hết hạn. Bạn phải đăng nhập lại", complete: nil)
                        DispatchQueue.main.async {
                                LoginHelper.logout()
                                self.performSelector(onMainThread: #selector(self.loadLogin), with: nil, waitUntilDone: false)
                        }

                    }
                } else {
                    DispatchQueue.main.async {// Mất kết nối
                           self.performSelector(onMainThread: #selector(self.loadMenu), with: nil, waitUntilDone: false)
                    }
                }
            }

        } else {
            LoginHelper.logout()
            loadLogin()
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    @objc func loadLogin() {

        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "LoginView")
        let nav = UINavigationController.init(rootViewController: vc)
        self.window?.rootViewController = nav
        vc.navigationController?.setNavigationBarHidden(true, animated: true)
        self.window?.makeKeyAndVisible()
    }
    
    
    @objc func loadMenu() {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "menuLavionTabBarController")
        for viewController in (vc as! MenuLavionTabBarController).viewControllers! {
            viewController.loadView()
        }
        vc.view.backgroundColor = .white
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
        
        
    }

}

