//
//  ProductHotCollectionCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/17/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import AlamofireImage

class ProductHotCollectionCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var cashLabel: UILabel!
    var detailProduct:ProductModel? {
        didSet {
            detailLabel.text = detailProduct?.san_pham_ten
            cashLabel.text = (detailProduct?.san_pham_gia_ban_thuc_te.formatNumber() ?? "0") + " VND"
            let placeholderImage = UIImage(named: "lavion.jpg")
            if let url = URL(string: (detailProduct?.san_pham_hinh_dai_dien) ?? address) {
                productImageView.af_setImage(withURL: url, placeholderImage: placeholderImage)
            } else {
                productImageView.image = placeholderImage
            }
            
            
            
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.initShadowForCell()
        
    }

}
