//
//  PayedTableCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/23/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class PayedTableCell: UITableViewCell {

    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var statusPaylabel: UILabel!
    
    @IBOutlet weak var totalCashView: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    var item: HistoryPayModel? {
        didSet {
            statusPaylabel.text     = "#\(item?.id ?? 0) - \(item?.status ?? "")"
            totalCashView.text      = "Tổng tiền: \(item?.totalCash ?? 0) Đ"
            dateLabel.text          = item?.date
            
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.initShadowForCellType2()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
