//
//  CartProductTableCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/14/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class CartProductTableCell: UITableViewCell {

    @IBOutlet weak var numberGMS: GMStepper!
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var cashLabel: UILabel!
    @IBOutlet weak var nameProduceLabel: UILabel!
    var item: ProductInCartModel? {
        didSet {
            numberGMS.value = Double(item?.soLuong ?? 0)
            cashLabel.text = (item?.product?.san_pham_gia_ban_thuc_te.formatNumber() ?? "") + " VND"
            nameProduceLabel.text = item?.product?.san_pham_ten
            let placeholderImage = UIImage(named: "lavion.jpg")
            if let url = URL(string: (item?.product?.san_pham_hinh_dai_dien) ?? address) {
                productImageView.af_setImage(withURL: url, placeholderImage: placeholderImage)
            } else {
                productImageView.image = placeholderImage
            }
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        numberGMS.addTarget(self, action: #selector(self.stepperValueChanged), for: .valueChanged)
        removeButton.addTarget(self, action:  #selector(self.removeAction), for: .touchUpInside)
    }
    
    @objc func stepperValueChanged(stepper: GMStepper) {
        print(stepper.value, terminator: "")
        if stepper.value == 0 {
            removeProduct()
            return
        }
        let realm = try! Realm()
        let product:ProductInCartModel? = realm.objects(ProductInCartModel.self).filter("id = \(item?.id ?? -1)").first
        if (product != nil) {
            try! realm.write {
                product!.soLuong = Int(stepper.value)
            }
        }
    }
    
    @objc func removeAction(button: UIButton) {
        print("Remove button")
        removeProduct()
        
    }
    
    func removeProduct() {
        let realm = try! Realm()
        let product:ProductInCartModel? = realm.objects(ProductInCartModel.self).filter("id = \(item?.id ?? -1)").first
        if (product != nil) {
            try! realm.write {
                realm.delete(product!)
            }
            
        }
        
    }
    
}
