//
//  HotProduceCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/17/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift




class HotProduceCell: UITableViewCell {
    
    var listProductArray = (try! Realm()).objects(ProductModel.self)
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        mainCollectionView.register(UINib.init(nibName: "ProductHotCollectionCell", bundle: nil), forCellWithReuseIdentifier: "productHotCollectionCell")
        if let flowLayout = mainCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let spacing:CGFloat = 1.5;
            let inset:CGFloat = 0;
            let numberOfColumns:CGFloat = 3;
            flowLayout.minimumInteritemSpacing = spacing
            flowLayout.minimumLineSpacing = spacing
            flowLayout.sectionInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            //set scroll to Vertical
            flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.width - numberOfColumns * spacing +
                2 - inset * numberOfColumns) / numberOfColumns, height: 180)
            flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}

extension HotProduceCell:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listProductArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productHotCollectionCell", for: indexPath) as! ProductHotCollectionCell
        cell.detailProduct = listProductArray[indexPath.row]
        return cell
        
    }
    
    
}

extension HotProduceCell:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "detailProductViewController") as! DetailProductViewController
        vc.detailProduct = listProductArray[indexPath.row]
        
        self.parentContainerViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
    
}
