//
//  ProductTableCell.swift
//  LAVION
//
//  Created by iOS on 11/25/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class ProductTableCell: UITableViewCell {
    var listProductArray = (try! Realm()).objects(ProductModel.self)
    @IBOutlet weak var mainCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        mainCollectionView.isScrollEnabled = false
        
        mainCollectionView.register(UINib.init(nibName: "ProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "productCollectionCell")
        if let flowLayout = mainCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let inset:CGFloat = 0
            let numberOfColumns:CGFloat = 2
            let spacing:CGFloat = 0.01//1.5 * 0.5
            flowLayout.minimumInteritemSpacing = spacing
            flowLayout.minimumLineSpacing = spacing
            flowLayout.sectionInset = UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
            //set scroll to Vertical
            flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.width - numberOfColumns * 2 * spacing - inset * numberOfColumns) / numberOfColumns, height: 300)
            flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        }
        // init Data

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ProductTableCell:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listProductArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCollectionCell", for: indexPath) as! ProductCollectionCell
        if indexPath.row % 2 == 0 {
            cell.leftContraint.constant = 15
            cell.rightContraint.constant = 7.5
        } else {
            cell.leftContraint.constant = 7.5
            cell.rightContraint.constant = 15
        }
        
        if indexPath.row == 0 || indexPath.row == 1 {
            cell.topConstraint.constant = 15
        } else {
            cell.topConstraint.constant = 7.5
        }
        cell.item = listProductArray[indexPath.row]
        return cell
        
    }
    
    
    
    
}

extension ProductTableCell:UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "detailProductViewController") as! DetailProductViewController
        vc.detailProduct = listProductArray[indexPath.row]
        self.parentContainerViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
}
