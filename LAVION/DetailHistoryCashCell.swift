//
//  DetailHistoryCashCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/4/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class DetailHistoryCashCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var cashLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    var item: MILKTransactionHistoryModel? {
        didSet {
            nameLabel.text      = item?.email
            contentLabel.text   = item?.ghi_chu
            dateLabel.text      = item?.created_at
            cashLabel.text      = "+" + (item?.so_tien.formatNumber() ?? "0") + " MILK"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
