//
//  TypeAccountHeaderFooterView.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/25/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class TypeAccountHeaderFooterView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var upgradeTypeButton: UIButton!
    @IBOutlet weak var typeAccountImageView: UIImageView!
    @IBOutlet weak var typeAccountLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    func intHeader(typeAccount: Int) {
        switch typeAccount {
        case 0:
            typeAccountImageView.image = UIImage(named: "medal.png")
            typeAccountLabel.text = "Đại Lý"
        case 1:
            typeAccountImageView.image = UIImage(named: "medal.png")
            typeAccountLabel.text = "Thành viên"
        case 3:
            typeAccountImageView.image = UIImage(named: "medal_base.png")
            typeAccountLabel.text = "Cộng tác viên"
        default:
            break
        }
    }

}
