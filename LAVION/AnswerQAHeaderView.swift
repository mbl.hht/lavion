//
//  AnswerQAHeaderView.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/8/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class AnswerQAHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameQuestionLabel: UILabel!
    @IBOutlet weak var contentQuestionLabel: UILabel!
    @IBOutlet weak var dateQuestionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.initShadowForCell()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
