//
//  CatelogyProductCollectionCell.swift
//  LAVION
//
//  Created by iOS on 11/25/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class CatelogyProductCollectionCell: UICollectionViewCell {
    @IBOutlet weak var CatelogyProductImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    var item: CatelogyProduceModel? {
        didSet {
            CatelogyProductImage.image = UIImage(named: self.item?.image ?? "")
            titleLabel.text = item?.title
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
