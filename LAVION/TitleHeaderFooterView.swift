//
//  TitleHeaderFooterView.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/25/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
class TitleHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var updateButton: UIButton!
    var typeHeader:Enum.TypeHeaderMember = Enum.TypeHeaderMember.None
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateButton.layer.cornerRadius = 10
        updateButton.layer.borderWidth = 1
        updateButton.layer.borderColor = UIColor.clear.cgColor
        updateButton.addTarget(self, action: #selector(self.updateViewAction(sender:)), for: .touchUpInside)
        //custom logic goes here
    }

    func initHeader(urlImage: String, setTitleLabel: String, isBold: Bool,isShowPoint: Bool,setPointLabel: String?,isShowButton: Bool,setTextButton: String) {
        
        iconImageView.image = UIImage.init(named: urlImage)
        
        titleLabel.text = setTitleLabel
        if isBold == true {
            titleLabel.font = UIFont.boldSystemFont(ofSize: titleLabel.font.pointSize)
        } else {
            titleLabel.font = UIFont.systemFont(ofSize: titleLabel.font.pointSize)
        }
        
        if isShowPoint {
            pointLabel.isHidden = false
            pointLabel.text = setPointLabel ?? ""
        } else {
            pointLabel.isHidden = true
        }
        
        if isShowButton {
            updateButton.isHidden = false
            updateButton.setTitle(setTextButton, for: UIControl.State.normal)
        } else {
            updateButton.isHidden = true
        }
        
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @objc func updateViewAction(sender: UIButton) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        switch typeHeader {
        case .CapNhatCMND:
            let vc = sb.instantiateViewController(withIdentifier: "updateCMNDViewController")
            self.parentContainerViewController()?.navigationController?.pushViewController(vc, animated: true)
        case .TaiKhoanNganHang:
            let vc = sb.instantiateViewController(withIdentifier: "accountBankViewController")
            self.parentContainerViewController()?.navigationController?.pushViewController(vc, animated: true)
        case .ThongTinCaNhan:
            let vc = sb.instantiateViewController(withIdentifier: "infomationMemberViewController")
            self.parentContainerViewController()?.navigationController?.pushViewController(vc, animated: true)
        case .MaGiamGia:
            let items = [URL(string: "https://www.lavion.vn")!]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            self.parentContainerViewController()?.present(ac, animated: true)
            
            
        default:
            break
        }
    }
}
