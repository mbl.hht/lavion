//
//  HistoryTakeCashHeaderView.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/8/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit
import RealmSwift

class HistoryTakeCashHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var moneyVNDTextField: UITextField!
    @IBOutlet weak var numberAccountLabel: UILabel!
    @IBOutlet weak var takeCashButton: UIButton!
    
    @IBOutlet weak var nameBankLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var nameAccountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        //Update Button
        takeCashButton.layer.cornerRadius = 5
        takeCashButton.layer.borderWidth = 1
        takeCashButton.layer.borderColor = UIColor.clear.cgColor
        takeCashButton.initRadius()
        
        
    }
    @IBAction func takeCashAction(_ sender: Any) {
        var messageError = ""
        if moneyVNDTextField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            messageError = "Số tiền không được để trống"
        }
        
        if messageError.count > 0 {
            ErrorHelper.showErrorWithSelf((self.window?.rootViewController)!, titleString: "Thông Báo", errorString: messageError, complete: nil)
            return
        }
        
        UIHelper.showHUD()
        SyncHttpConnect.postWithdrawalVND(moneyVND: moneyVNDTextField.text!) { (data, success) in
            UIHelper.dismisHUD()
            if success {
                if data["status"] as! Int == 200 {
                    
                    SyncHttpConnect.getStatistic { (data: NSDictionary?, success: Bool) in
                        if success == true && StatisticAccessor.set(data: data as! Dictionary<String, Any>) == true {
                        } else {
                            
                        }
                        
                    }
                    
                    SyncHttpConnect.getVNDWithdrawalHistory { (data: NSDictionary?, success: Bool) in
                        if success == true && VNDWithdrawalAccessor.set(data: data as! Dictionary<String, Any>) == true {
                            //callLoadMenu(addType: .TypeVNDWithdrawalHistory)
                        } else {
                            //logout(type: .FailAPIWhenLogining)
                        }
                    }

                    ErrorHelper.showErrorWithSelf((self.window?.rootViewController)!, titleString: "Thông báo", errorString: data["data"] as? String ?? "", complete: {
                        //self.navigationController?.popViewController(animated: true)
                        self.moneyVNDTextField.text = ""
                    })
                    
                    
                    
                } else if data["status"] as! Int == 400 {
                    messageError = data["errors"] as? String ?? ""
                    ErrorHelper.showErrorWithSelf((self.window?.rootViewController)!, titleString: "Thông báo", errorString: messageError, complete: nil)
                    
                }
            } else {
                ErrorHelper.showDisconnectInternet(viewController: (self.window?.rootViewController)!)
            }
        }
        
    }
    
}
