//
//  GeneralSettingModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/9/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class GeneralSettingModel: Object {
    @objc dynamic var id = -1
    @objc dynamic var thanh_vien_phi_chuyen_vnd_sang_milk = 0.0
    @objc dynamic var thanh_vien_phi_chuyen_milk_sang_vnd = 0.0
    @objc dynamic var thanh_vien_phi_chuyen_milk_sang_tai_khoan_khac = 0.0
    
    @objc dynamic var dai_ly_offline_phi_chuyen_vnd_sang_milk = 0.0
    @objc dynamic var dai_ly_offline_phi_chuyen_milk_sang_vnd = 0.0
    @objc dynamic var dai_ly_offline_phi_chuyen_milk_sang_tai_khoan_khac = 0.0
    
    @objc dynamic var dai_ly_online_phi_chuyen_vnd_sang_milk = 0.0
    @objc dynamic var dai_ly_online_phi_chuyen_milk_sang_vnd = 0.0
    @objc dynamic var dai_ly_online_phi_chuyen_milk_sang_tai_khoan_khac = 0.0
    @objc dynamic var phi_chuyen_milk = 0.0
    @objc dynamic var phi_chuyen_vnd = 0.0
    
    
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}
