//
//  BankModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/27/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift

class BankModel: Object {
    @objc dynamic var id:Int = -1
    @objc dynamic var tenNganHang = ""
    override class func primaryKey() -> String? {
        return "id"
    }
}
