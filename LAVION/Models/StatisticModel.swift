//
//  StatisticModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/1/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift

class StatisticModel: Object {
    @objc dynamic var id                                = -1
    @objc dynamic var email                             = ""
    @objc dynamic var tong_tien_vnd: Double             = 0
    @objc dynamic var nap_api: Double                   = 0
    @objc dynamic var the_milk: Double                  = 0
    @objc dynamic var thuong_gioi_thieu_ctv: Double     = 0
    @objc dynamic var thuong_dai_ly: Double             = 0
    @objc dynamic var thuong_hoa_hong_muc_1: Double     = 0
    @objc dynamic var thuong_hoa_hong_muc_2: Double     = 0
    @objc dynamic var created_at                        = ""
    @objc dynamic var updated_at                        = ""
    @objc dynamic var vnd_pending: Double               = 0
    @objc dynamic var vnd_chuyen_tu_milk: Double        = 0
    @objc dynamic var vnd_mua_hang: Double              = 0
    @objc dynamic var milk_chuyen_ctv_khac: Double      = 0
    @objc dynamic var milk_pending: Double              = 0
    @objc dynamic var thuong_vnd_tu_dang_ky: Double     = 0
    @objc dynamic var thuong_milk_tu_dang_ky: Double    = 0
    @objc dynamic var thuong_milk_gioi_thieu: Double    = 0
    @objc dynamic var milk_chuyen_tu_vnd: Double        = 0
    @objc dynamic var tongTienVNDKiemDuoc:Double        = 0.0 // 17101000,
    @objc dynamic var tongMilkKiemDuoc:Double           = 0.0 // 0,
    @objc dynamic var soCongTacVienGioiThieu:Int        = 0 // 4,
    @objc dynamic var soCongTacVienGioiThieuDaKichHoat:Int = 0 // 3,
    @objc dynamic var soDonHangDaBan:Int                = 0 // 2,
    @objc dynamic var soDonHangDaBanThanhCong:Int       = 0 // 2,
    @objc dynamic var soDonMuaHang:Int                  = 0 // 2
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
