//
//  VNDWithdrawalModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/4/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
//"id": 230,
//"email_rut_tien": "tranvthanhson@gmail.com",
//"so_tien": 1000000,
//"tinh_trang": "Success",
//"ngan_hang": "Số tài khoản: 123345123 tại Ngân hàng: Vietcombank - Ngân Hàng TMCP Ngoại Thương Việt Nam , chi nhánh: Tên chi nhánh. Người thụ hưởng: Trần Vũ Thanh Sơn",
//"thong_bao": "Admin đã chuyển khoản: 2018-12-06 16:47:37",
//"created_at": "2018-12-05 20:22:47",
//"updated_at": "2018-12-06 16:47:37"
class VNDWithdrawalModel: Object {
    @objc dynamic var id                       = 0
    @objc dynamic var email_rut_tien           = ""
    @objc dynamic var so_tien: Double          = 0.0
    @objc dynamic var tinh_trang               = ""
    @objc dynamic var ngan_hang: String        = ""
    @objc dynamic var thong_bao: String        = ""
    @objc dynamic var created_at: String       = ""
    @objc dynamic var updated_at: String       = ""

    override class func primaryKey() -> String? {
        return "id"
    }
}
