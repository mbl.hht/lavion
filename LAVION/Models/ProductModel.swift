//
//  ProductModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/16/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class ProductModel: Object {
    @objc dynamic var id = -1
    @objc dynamic var san_pham_ma = ""
    @objc dynamic var san_pham_ten = ""
    @objc dynamic var san_pham_url = ""
    @objc dynamic var san_pham_mo_ta = ""
    @objc dynamic var san_pham_so_luong: Double = 0.0
    @objc dynamic var san_pham_hinh_dai_dien = ""
    @objc dynamic var san_pham_hinh_anh_slide = ""
    @objc dynamic var san_pham_noi_dung_tab_1 = ""
    @objc dynamic var san_pham_noi_dung_tab_2 = ""
    @objc dynamic var san_pham_noi_dung_tab_3 = ""
    
    @objc dynamic var san_pham_noi_dung_tab_4 = ""
    @objc dynamic var san_pham_noi_dung_tab_5 = ""
    @objc dynamic var san_pham_keyword = ""
    @objc dynamic var san_pham_tags = ""
    @objc dynamic var san_pham_id_chuyen_muc = ""
    @objc dynamic var san_pham_gia_ban_thuc_te: Double = 0.0
    @objc dynamic var san_pham_gia_ban_ao: Double = 0.0
    @objc dynamic var san_pham_is_khuyen_mai: Bool = false
    @objc dynamic var san_pham_noi_dung_khuyen_mai = ""
    @objc dynamic var san_pham_is_accept = ""
    @objc dynamic var san_pham_is_delete = ""
    @objc dynamic var san_pham_id_nguoi_duyet = ""
    @objc dynamic var san_pham_nguoi_duyet = ""
    @objc dynamic var san_pham_id_nhom_san_pham = ""
    @objc dynamic var created_at = ""
    @objc dynamic var updated_at = ""
    override class func primaryKey() -> String? {
        return "id"
    }

}
