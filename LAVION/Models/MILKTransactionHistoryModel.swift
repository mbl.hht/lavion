//
//  MILKTransactionHistoryModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/5/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class MILKTransactionHistoryModel: Object {
    @objc dynamic var id                        = 0
    @objc dynamic var email                     = ""// "tranvthanhson@gmail.com",
    @objc dynamic var so_tien:Double            = 0.0//: 1000,
    @objc dynamic var ghi_chu                   = ""//"Thưởng VNĐ do giới thiệu thành viên mới!",
    @objc dynamic var created_at                = ""//  "2018-12-27 18:32:17"
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
