//
//  HistoryTranferMILKToLocalModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/12/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class HistoryTranferMILKToLocalModel: Object {
    @objc dynamic var id = -1//: 387,
    @objc dynamic var email_nguoi_chuyen = "" //"tranvthanhson@gmail.com",
    @objc dynamic var ho_ten_nguoi_chuyen="" //"Trần Vũ Thanh Sơn",
    @objc dynamic var email_nguoi_nhan = "" //: "kuduronoob@gmail.com",
    @objc dynamic var ho_ten_nguoi_nhan = ""//: "Khoa Nguyễn",
    @objc dynamic var so_milk_chuyen: Double = 0.0 // 12,
    @objc dynamic var phi_chuyen_milk: Double = 0.0 // 1,
    @objc dynamic var hash1 = ""// "d228ce56-d196-4ceb-97a8-17318c2037e1",
    @objc dynamic var trang_thai = ""//: "PENDING",
    @objc dynamic var created_at = ""//"": "2019-01-12 23:44:08",
    @objc dynamic var updated_at = ""//: "2019-01-12 23:44:08"
    override class func primaryKey() -> String? {
        return "id"
    }
}
