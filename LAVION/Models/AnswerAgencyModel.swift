//
//  AnswerAgencyModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/8/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import Foundation
struct AnswerAgencyModel {
    var id = 0
    var adminEmail: String?
    var adminHoVaTen: String?
    var congTacVienEmail: String? //tranvthanhson@gmail.com",
    var congTacVienHoVaTen: String? // "Trần Vũ Thanh Sơn",
    var coiDungTraLoi: String?//"yup",
    var createdAt: String? //2018-11-21 01:04:48"
}
