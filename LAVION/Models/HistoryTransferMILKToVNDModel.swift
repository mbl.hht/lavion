//
//  HistoryTransferMILKToVNDModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/7/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class HistoryTransferMILKToVNDModel: Object {
    @objc dynamic var id = -1
    @objc dynamic var email_nguoi_chuyen = "" //tranvthanhson@gmail.com",
    @objc dynamic var ho_ten_nguoi_chuyen = "" //"Trần Vũ Thanh Sơn",
    @objc dynamic var milk_chuyen = 0 //  10,
    @objc dynamic var phi_chuyen_milk =  0.0//1,
    @objc dynamic var vnd_nhan_duoc =  0.0//900 ,
    @objc dynamic var vnd_truoc_khi_chuyen =  0.0//0,
    @objc dynamic var vnd_sau_khi_chuyen =  0.0//9000,
    @objc dynamic var milk_truoc_khi_chuyen =  0.0//10000,
    @objc dynamic var milk_sau_khi_chuyen =  0.0//9990,
    @objc dynamic var status =  ""//null,
    @objc dynamic var message =  ""//null,
    @objc dynamic var hash1 =  ""//"58df0a3a-c6ea-4713-a293-5895a0956779",
    @objc dynamic var created_at =  ""//"2019-01-02 18:28:20",
    @objc dynamic var updated_at =  ""//"2019-01-02 18:28:20"
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
