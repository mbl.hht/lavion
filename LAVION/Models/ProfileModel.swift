//
//  ProfileModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 12/23/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
//        "status": 200,
//        "data": {
//            "id": 12,
//            "email": "tranvthanhson@gmail.com",
//            "so_dien_thoai": "01269736096",
//            "cmnd": "123321123",
//            "so_tai_khoan": "123123123",
//            "ho_va_ten": "Trần Vũ Thanh Sơn",
//            "gioi_tinh": "NỮ",
//            "txid": "47284797-9021-4130-9eb1-4245d717a54c",
//            "facebook": "tranvthanhson2",
//            "ten_chi_nhanh": "Tên chi nhánh",
//            "is_delete": "NO",
//            "dia_chi_hien_tai": "435 Tieu La213",
//            "dia_chi_cmnd": "435 Tieu La2",
//            "ten_chu_tai_khoan": "Trần Vũ Thanh Sơn",
//            "email_gioi_thieu": "quoclongdng@gmail.com",
//            "ngay_sinh": "2018-11-01 00:00:00",
//            "admin_kich_hoat": "YES",
//            "txid_quen_mat_khau": "e5d30cad-82ea-448e-9442-88fd4bdfc937",
//            "trang_thai_quen_mat_khau": "YES",
//            "is_block": "NO",
//            "ngay_quen_mat_khau": "2018-12-15 12:03:29",
//            "is_dai_ly": "NO",
//            "email_dai_ly_quan_ly": null,
//            "ip_dang_ky": "171.255.164.251",
//            "img_01": "/images/xac_thuc/c04f9c46-3d9e-4a12-908e-f8134784ad5a.jpg",
//            "img_02": "/images/xac_thuc/b46e8506-56f6-45f0-8ca6-9bb8f62a109c.jpg",
//            "avatar": "/images/xac_thuc/508dda20-3ef6-49ad-bf41-4712c92f3f68.jpg",
//            "tinh_thanh": "An Giang",
//            "li_do_khong_duyet": null,
//            "convert_email": "tranvthanhson@gmail.com1",
//            "so_thanh_vien_da_gioi_thieu": 1,
//            "id_nguoi_dung": null,
//            "id_ngan_hang": 9,
//            "created_at": "2018-08-15 19:56:16",
//            "updated_at": "2018-12-20 14:36:30",
//            "avatar_cmnd": null,
//            "dia_chi_tu_ip_dang_ky": "Ho Chi Minh, VN",
//            "is_thanh_vien": "THANH VIEN",
//            "ly_do_nang_cap": null,
//            "admin_duyet_nang_cap": null,
//            "ngay_duyet_nang_cap": null,
//            "is_mail_checked": "YES",
//            "trang_thai_xet_duyet": "NOT YET",
//            "ly_do_block": null,
//            "chung_ip_dang_ky": 1,
//            "is_change_img_01": "NO",
//            "is_change_img_02": "NO",
//            "is_change_avatar": "NO",
//            "ten_nguoi_gioi_thieu": "Nguyễn Quốc Long",
//            "so_dien_thoai_gioi_thieu": "0905523543"
//        },
//        "message": "",
//        "errors": null

class ProfileModel: Object {
    @objc dynamic var id: NSInteger                 = -1
    @objc dynamic var email                         = ""
    @objc dynamic var soDienThoai                   = ""
    @objc dynamic var cmnd                          = ""
    @objc dynamic var soTaiKhoan                    = ""
    @objc dynamic var hoVaTen                       = ""
    @objc dynamic var gioiTinh                      = ""
    @objc dynamic var ten_chi_nhanh                 = ""
    @objc dynamic var diaChiHienTai                 = ""
    @objc dynamic var diaChiCmnd                    = ""
    @objc dynamic var tenChuTaiKhoan                = ""
    @objc dynamic var emailGioiThieu                = ""
    @objc dynamic var ngaySinh                      = ""
    @objc dynamic var adminKichHoat: Bool           = false
    @objc dynamic var img_01:String                 = ""
    @objc dynamic var img_02:String                 = ""
    @objc dynamic var isDaiLy: Bool                 = false
    @objc dynamic var avatar                        = ""
    @objc dynamic var tinhThanh                     = ""
    @objc dynamic var idNganHang: Int               = -1
    @objc dynamic var soThanhVienDaGioiThieu: Int   = -1
    @objc dynamic var isThanhVien                   = ""
    @objc dynamic var tenNguoiGioiThieu             = ""
    @objc dynamic var soDienThoaiGioiThieu          = ""
    @objc dynamic var emailDaiLyQuanLy              = ""
    @objc dynamic var magiamgia                     = 0
    @objc dynamic var facebook                      = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
