//
//  ProductInCartModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/20/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift
class ProductInCartModel: Object {
    @objc dynamic var id = -1
    @objc dynamic var product: ProductModel?
    @objc dynamic var soLuong = 0
    @objc dynamic var date: Date = Date()
    override class func primaryKey() -> String? {
        return "id"
    }
}
