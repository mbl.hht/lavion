//
//  HistoryMILKLoadModel.swift
//  LAVION
//
//  Created by MaiBaoLoc on 1/14/19.
//  Copyright © 2019 MaiBaoLoc. All rights reserved.
//

import Foundation
import RealmSwift

class HistoryMILKLoadModel: Object {
    @objc dynamic var id = -1
    @objc dynamic var so_diem:Double = 0.0
    @objc dynamic var trang_thai = ""
    @objc dynamic var updated_at = ""
    override class func primaryKey() -> String? {
        return "id"
    }
    
}
