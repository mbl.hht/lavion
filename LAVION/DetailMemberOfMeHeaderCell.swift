//
//  DetailMemberOfMeHeaderCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 10/31/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class DetailMemberOfMeHeaderCell: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var leftImageLayoutConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setValueCell(urlImage: String,titleText: String) {
        iconImageView.image = UIImage(named: urlImage)
        titleLabel.text = titleText
        
    }

}
