//
//  DetailHistoryTransferMILKToVNDCell.swift
//  LAVION
//
//  Created by MaiBaoLoc on 11/11/18.
//  Copyright © 2018 MaiBaoLoc. All rights reserved.
//

import UIKit

class DetailHistoryTransferMILKToLocalCell: UITableViewCell {
    
    @IBOutlet weak var codeGDlabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var numberMILKLabel: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var item: HistoryTranferMILKToLocalModel? {
        didSet {
            codeGDlabel.text = "Mã GD : \(item?.id ?? 0)"
            statusLabel.text = item?.trang_thai
            emailLabel.text = item?.email_nguoi_nhan
            nameLabel.text = item?.ho_ten_nguoi_nhan
            numberMILKLabel.text = "Số MILK : \(item?.so_milk_chuyen.formatNumber() ?? "0") MILK"
            feeLabel.text = "Phí : \(item?.phi_chuyen_milk.formatNumber() ?? "0") MILK"
            dateLabel.text = item?.updated_at
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cancelButton.initRadius()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
    
    @IBAction func cancelAction(_ sender: Any) {
        ErrorHelper.showErrorWithSelf((self.window?.rootViewController)!, titleString: "Thông báo", errorString: "Chức năng này chưa được xây dựng.", complete: nil)
    }
    
    
}
